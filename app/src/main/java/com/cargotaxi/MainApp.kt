package com.cargotaxi

import android.app.Application
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import com.cargotaxi.Model.Model
import com.cargotaxi.extension.isOreoPlus
import org.jetbrains.anko.notificationManager
import timber.log.Timber

@Suppress("unused")
class MainApp : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        if (isOreoPlus()) {
            notificationManager.createNotificationChannel(
                NotificationChannel("foreground", "Фон", NotificationManager.IMPORTANCE_LOW).also {
                    it.lockscreenVisibility = Notification.VISIBILITY_SECRET
                }
            )
            notificationManager.createNotificationChannel(
                NotificationChannel("default", "По умолчанию", NotificationManager.IMPORTANCE_DEFAULT).also {
                    it.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
                }
            )
        }
        DAO.getInstance().init(applicationContext)
        DAO.getInstance().loadProfil()
        Model.getInstance().init(applicationContext)
    }
}