package com.cargotaxi;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.cargotaxi.Model.Model;
import com.google.android.material.textfield.TextInputEditText;

public class CustomerMenuCommentFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_menu_comment, null);
        TextView tvInfo = view.findViewById(R.id.tvInfo);
        tvInfo.setText(Model.getInstance().getParamCardByType("order_comment").getStringValue());
        TextInputEditText etComment = view.findViewById(R.id.etComment);
        etComment.setText(Model.getInstance().getCurrentOrder().getComment());
        etComment.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                Model.getInstance().getCurrentOrder().setComment(s.toString());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Комментарий");
        return view;
    }
}