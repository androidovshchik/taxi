package com.cargotaxi;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.cargotaxi.Model.Model;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.button.MaterialButton;

import java.util.List;

import timber.log.Timber;

public class CustomerMap2Fragment extends Fragment {
    private TextView tvName;
    private TextView tvInfo;
    private TextView tvEmpty;
    private GoogleMap googleMap;
    private onEventListener eventListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Model.getInstance().setCurrentSetAddress(2);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Куда");
        View view = inflater.inflate(R.layout.fragment_customer_map, null);
        tvEmpty = view.findViewById(R.id.tvEmpty);
        RelativeLayout rlSelectAddress = view.findViewById(R.id.rlSelectAddress);
        rlSelectAddress.setOnClickListener(v -> eventListener.toSetFinishAddressEvent());
        MaterialButton btnNext = view.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(v -> eventListener.toMenuEvent());
        tvName = view.findViewById(R.id.tvName);
        tvName.setText(Model.getInstance().getCurrentOrder().getAddressFinish());
        if (tvName.length() > 5) tvEmpty.setVisibility(View.INVISIBLE);
        tvInfo = view.findViewById(R.id.tvInfo);
        tvInfo.setText(Model.getInstance().getCurrentOrder().getCityFinish());
        final BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_action_time);
        SupportMapFragment mapFragment;
        {
            mapFragment = SupportMapFragment.newInstance();
            mapFragment.getMapAsync(googleMap1 -> {
                googleMap = googleMap1;
                LatLng latLng = new LatLng(0, 0);
                LatLng latLng1 = new LatLng(0, 0);
                try {
                    latLng = new LatLng(Model.getInstance().getCurrentOrder().getLatLngStart().latitude, Model.getInstance().getCurrentOrder().getLatLngStart().longitude);
                    googleMap.addMarker(new MarkerOptions().position(latLng)
                        .title(Model.getInstance().getCurrentOrder().getAddressStart())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker1)));
                } catch (Exception ignored) {
                }
                try {
                    latLng1 = new LatLng(Model.getInstance().getCurrentOrder().getLatLngFinish().latitude, Model.getInstance().getCurrentOrder().getLatLngFinish().longitude);
                    googleMap.addMarker(new MarkerOptions().position(latLng1)
                        .title(Model.getInstance().getCurrentOrder().getAddressFinish())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker2)));
                } catch (Exception ignored) {
                }
                try {
                    if (Model.getInstance().getCurrentOrder().getLatLngFinish() != null)
                        new getObjectByCoord().execute();
                    if (latLng1.latitude > 0)
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng1, 18.0f));
                    else
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18.0f));
                    googleMap.setOnMapClickListener(latLng2 -> {
                        googleMap.clear();
                        Model.getInstance().getCurrentOrder().setLatLngFinish(latLng2);
                        googleMap.addMarker(new MarkerOptions().position(Model.getInstance().getCurrentOrder().getLatLngStart())
                            .title(Model.getInstance().getCurrentOrder().getAddressStart())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker1)));
                        googleMap.addMarker(new MarkerOptions().position(latLng2)
                            .title(Model.getInstance().getCurrentOrder().getAddressFinish())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker2)));
                        new getObjectByCoord().execute();
                    });
                } catch (Exception e) {
                    Timber.e(e);
                }
            });
        }
        getChildFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            eventListener = (onEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    public interface onEventListener {
        void toMenuEvent();

        void toSetFinishAddressEvent();
    }

    class getObjectByCoord extends AsyncTask<Context, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Context... arg0) {
            NetworkController.getInstance().getObjectByCoord(Model.getInstance().getCurrentOrder().getLatLngFinish().latitude, Model.getInstance().getCurrentOrder().getLatLngFinish().longitude, 2);
            NetworkController.getInstance().getRoutes();
            return null;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            tvName.setText(Model.getInstance().getCurrentOrder().getAddressFinish());
            if (tvName.length() > 5) tvEmpty.setVisibility(View.INVISIBLE);
            tvInfo.setText(Model.getInstance().getCurrentOrder().getCityFinish());
            List<LatLng> list = Model.getInstance().decodePoly(Model.getInstance().getPointWay());
            for (int z = 0; z < list.size() - 1; z++) {
                LatLng src = list.get(z);
                LatLng dest = list.get(z + 1);
                googleMap.addPolyline(new PolylineOptions()
                    .add(new LatLng(src.latitude, src.longitude),
                        new LatLng(dest.latitude, dest.longitude))
                    .width(5).color(Color.BLUE).geodesic(true));
            }
        }
    }
}