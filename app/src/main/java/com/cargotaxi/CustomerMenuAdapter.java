package com.cargotaxi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.cargotaxi.Model.CustomerMenuCard;
import com.cargotaxi.Model.Model;

import java.util.List;

public class CustomerMenuAdapter extends RecyclerView.Adapter<CustomerMenuAdapter.CardViewHolder> {
    private static CustomerMenuFragment fragment = null;
    private static List<CustomerMenuCard> cards;

    public CustomerMenuAdapter(CustomerMenuFragment fragment, Context context, List<CustomerMenuCard> cards) {
        CustomerMenuAdapter.fragment = fragment;
        CustomerMenuAdapter.cards = cards;
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_menu_switch, viewGroup, false);
        return new CardViewHolder(v, i);
    }

    @Override
    public void onBindViewHolder(final CardViewHolder personViewHolder, final int i) {
        personViewHolder.tvName.setText(cards.get(i).getName());
        personViewHolder.tvInfo.setText(cards.get(i).getInfo());
        personViewHolder.ivIcon.setImageResource(cards.get(i).getIdIcon());
        personViewHolder.cv.setOnClickListener(view -> {
            Model.getInstance().getCustomerMenuCard().get(i).setSelect(!Model.getInstance().getCustomerMenuCard().get(i).isSelect());
            if (cards.get(i).getType().compareTo("time") == 0) fragment.toTimeEvent();
            if (cards.get(i).getType().compareTo("period") == 0) fragment.toPeriodEvent();
            if (cards.get(i).getType().compareTo("type_car") == 0) fragment.toTypeCarEvent();
            if (cards.get(i).getType().compareTo("porter") == 0) fragment.toPorterEvent();
            if (cards.get(i).getType().compareTo("comment") == 0) fragment.toCommentEvent();
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {
        final CardView cv;
        final ImageView ivIcon;
        final TextView tvName;
        final TextView tvInfo;

        CardViewHolder(View itemView, int item) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            tvName = itemView.findViewById(R.id.tvName);
            tvInfo = itemView.findViewById(R.id.tvInfo);
            ivIcon = itemView.findViewById(R.id.ivIcon);
        }
    }
}
