package com.cargotaxi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.cargotaxi.Model.Model;

public class CustomerMenuPorterAdapter extends RecyclerView.Adapter<CustomerMenuPorterAdapter.CardViewHolder> {
    private static CustomerMenuPorterFragment fragment = null;
    private int curPunkt;

    public CustomerMenuPorterAdapter(CustomerMenuPorterFragment fragment, Context context, int curPunkt) {
        CustomerMenuPorterAdapter.fragment = fragment;
        this.curPunkt = curPunkt;
    }

    @Override
    public int getItemCount() {
        return Model.getInstance().getCustomerMenuCard().get(curPunkt).getNamePunkt().size();
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_menu_porter, viewGroup, false);
        return new CardViewHolder(v, i);
    }

    @Override
    public void onBindViewHolder(final CardViewHolder personViewHolder, final int i) {
        personViewHolder.tvName.setText(Model.getInstance().getCustomerMenuCard().get(curPunkt).getNamePunkt().get(i));
        int price = Math.round(Model.getInstance().getParamCardByType("price_porter").getIntValue() * i);
        personViewHolder.tvInfo.setText(String.valueOf(price));
        personViewHolder.tvCurrency.setText("\u20BD/час");
        if (Model.getInstance().getCurrentOrder().getCountPorter() == i) {
            personViewHolder.ivChecked.setBackgroundResource(R.drawable.check1);
            personViewHolder.tvInfo.setTextColor(ContextCompat.getColor(fragment.getContext(), R.color.colorPrimary));
            personViewHolder.tvCurrency.setTextColor(ContextCompat.getColor(fragment.getContext(), R.color.colorPrimary));
        } else {
            personViewHolder.ivChecked.setBackgroundResource(R.drawable.check0);
            personViewHolder.tvInfo.setTextColor(ContextCompat.getColor(fragment.getContext(), R.color.colorTextDop));
            personViewHolder.tvCurrency.setTextColor(ContextCompat.getColor(fragment.getContext(), R.color.colorTextDop));
        }
        personViewHolder.cv.setOnClickListener(view -> {
            Model.getInstance().getCurrentOrder().setCountPorter(i);
            fragment.eventListener.toMenuEvent();
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {
        final CardView cv;
        final TextView tvName;
        final TextView tvInfo;
        final TextView tvCurrency;
        final ImageView ivChecked;

        CardViewHolder(View itemView, int item) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            tvName = itemView.findViewById(R.id.tvName);
            tvInfo = itemView.findViewById(R.id.tvInfo);
            tvCurrency = itemView.findViewById(R.id.tvСurrency);
            ivChecked = itemView.findViewById(R.id.ivChecked);
        }
    }
}
