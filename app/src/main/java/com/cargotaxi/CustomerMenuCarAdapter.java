package com.cargotaxi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.cargotaxi.Model.Model;

public class CustomerMenuCarAdapter extends RecyclerView.Adapter<CustomerMenuCarAdapter.CardViewHolder> {
    private static CustomerMenuCarFragment fragment = null;

    public CustomerMenuCarAdapter(CustomerMenuCarFragment fragment, Context context) {
        CustomerMenuCarAdapter.fragment = fragment;
    }

    @Override
    public int getItemCount() {
        return Model.getInstance().getCarCard().size();
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_menu_car, viewGroup, false);
        return new CardViewHolder(v, i);
    }

    @Override
    public void onBindViewHolder(final CardViewHolder personViewHolder, final int i) {
        personViewHolder.tvName.setText(Model.getInstance().getCarCard().get(i).getType());
        personViewHolder.tvInfo.setText(Model.getInstance().getCarCard().get(i).getInfo());
        personViewHolder.tvCount.setText(Model.getInstance().getCarCard().get(i).getCount() + " машин свободно");
        personViewHolder.tvPrice.setText(String.valueOf(Model.getInstance().getCarCard().get(i).getMinPrice()));
        personViewHolder.tvCurrency.setText("\u20BD/час");
        if (Model.getInstance().getCurrentOrder().getIdTypeCar() == Model.getInstance().getCarCard().get(i).getId()) {
            personViewHolder.ivChecked.setBackgroundResource(R.drawable.check1);
            personViewHolder.tvCurrency.setTextColor(ContextCompat.getColor(fragment.getContext(), R.color.colorPrimary));
            personViewHolder.tvPrice.setTextColor(ContextCompat.getColor(fragment.getContext(), R.color.colorPrimary));
        } else {
            personViewHolder.ivChecked.setBackgroundResource(R.drawable.check0);
            personViewHolder.tvCurrency.setTextColor(ContextCompat.getColor(fragment.getContext(), R.color.colorTextDop));
            personViewHolder.tvPrice.setTextColor(ContextCompat.getColor(fragment.getContext(), R.color.colorTextDop));
        }
        personViewHolder.cv.setOnClickListener(view -> {
            Model.getInstance().getCurrentOrder().setIdTypeCar(Model.getInstance().getCarCard().get(i).getId());
            fragment.eventListener.toMenuEvent();
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {
        final CardView cv;
        final TextView tvName;
        final TextView tvInfo;
        final TextView tvCount;
        final TextView tvPrice;
        final TextView tvCurrency;
        final ImageView ivChecked;

        CardViewHolder(View itemView, int item) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            tvName = itemView.findViewById(R.id.tvName);
            tvInfo = itemView.findViewById(R.id.tvInfo);
            tvCount = itemView.findViewById(R.id.tvCount);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvCurrency = itemView.findViewById(R.id.tvСurrency);
            ivChecked = itemView.findViewById(R.id.ivChecked);
        }
    }
}
