package com.cargotaxi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.cargotaxi.Model.Model;
import com.google.android.material.snackbar.Snackbar;

public class SplashActivity extends AppCompatActivity {
    private IntentFilter intentFilter;
    private MyReceiver receiver;
    private Snackbar snackbar;
    private RelativeLayout rlMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        DAO.getInstance().loadCurOrder();
        NetworkController.getInstance().getInfo();
        rlMain = findViewById(R.id.rlMain);
        new Handler().postDelayed(() -> {
            intentFilter = new IntentFilter();
            intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            receiver = new MyReceiver();
            registerReceiver(receiver, intentFilter);
        }, 1000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            unregisterReceiver(receiver);
        } catch (Exception ignored) {
        }
    }

    private void getInfo() {
        Preferences preferences = new Preferences(getApplicationContext());
        if (Model.getInstance().getTypeUser() == 1) {
            Intent intent = new Intent(getApplicationContext(), CustomerActivity.class);
            startActivity(intent);
            finish();
        } else if (Model.getInstance().getTypeUser() == 2) {
            if (Model.getInstance().isRegDriver() == 0) {
                Intent intent = new Intent(getApplicationContext(), DriverRegActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(getApplicationContext(), DriverActivity.class);
                startActivity(intent);
                finish();
            }
        } else if (preferences.getViewSliders()) {
            Intent intent = new Intent(getApplicationContext(), RegActivity.class);
            startActivity(intent);
            finish();
        } else if (preferences.getReadTerms()) {
            Intent intent = new Intent(getApplicationContext(), StartActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(getApplicationContext(), TermsActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String actionOfIntent = intent.getAction();
            boolean isConnected = Model.getInstance().checkForInternet();
            if (actionOfIntent.equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                if (isConnected) {
                    if (snackbar != null && snackbar.isShown())
                        snackbar.dismiss();
                    getInfo();
                } else {
                    snackbar = Snackbar.make(rlMain, "Проверьте наличие Интернета", Snackbar.LENGTH_INDEFINITE);
                    snackbar.show();
                }
            }
        }
    }
}