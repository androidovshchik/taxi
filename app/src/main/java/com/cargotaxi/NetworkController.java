package com.cargotaxi;

import android.location.Address;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cargotaxi.Model.CarCard;
import com.cargotaxi.Model.Model;
import com.cargotaxi.Model.OrderCard;
import com.cargotaxi.Model.ParamCard;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

public class NetworkController {
    private static NetworkController instance;

    public static NetworkController getInstance() {
        if (instance == null) {
            instance = new NetworkController();
        }
        return instance;
    }

    public void saveCoordinate() {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/save_coordinate.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idUser", String.valueOf(Model.getInstance().getIdUser()));
                params.put("lat", String.valueOf(Model.getInstance().getCurrentLatLng().latitude));
                params.put("lng", String.valueOf(Model.getInstance().getCurrentLatLng().longitude));
                params.put("city", String.valueOf(Model.getInstance().getCurrentCity()));
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void getObjectByQuery(String str1, int numb) {
        try {
            String str = loadInfo("https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=" + str1 + "&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry&key=AIzaSyDY9dneEVpBzxlKVDGOEbHFdG2-Z_on30M");
            JSONObject jObject = new JSONObject(str);
            JSONArray jArray = new JSONArray(jObject.getString("candidates"));
            JSONObject jObject1 = new JSONObject(jArray.get(0).toString());
            str = jObject1.getString("formatted_address");
            if (numb == 1)
                Model.getInstance().getCurrentOrder().setAddressStart(str);
            else
                Model.getInstance().getCurrentOrder().setAddressFinish(str);
            jObject1 = new JSONObject(jObject1.getString("geometry"));
            jObject1 = new JSONObject(jObject1.getString("location"));
            LatLng latLng = new LatLng(Double.parseDouble(jObject1.getString("lat")), Double.parseDouble(jObject1.getString("lng")));
            if (numb == 1)
                Model.getInstance().getCurrentOrder().setLatLngStart(latLng);
            else
                Model.getInstance().getCurrentOrder().setLatLngFinish(latLng);
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    public void getObjectByCoord(double latitude, double longitude, int numb) {
        try {
            List<Address> addresses = Model.getInstance().getGeoCoder().getFromLocation(latitude, longitude, 1);
            if (numb == 1) {
                Model.getInstance().getCurrentOrder().setAddressStart(addresses.get(0).getThoroughfare() + ", " + addresses.get(0).getSubThoroughfare());
                Model.getInstance().getCurrentOrder().setCityStart(addresses.get(0).getLocality());
            } else {
                Model.getInstance().getCurrentOrder().setAddressFinish(addresses.get(0).getThoroughfare() + ", " + addresses.get(0).getSubThoroughfare());
                Model.getInstance().getCurrentOrder().setCityFinish(addresses.get(0).getLocality());
            }
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    public void getRoutes() {
        try {
            OrderCard currendOrder = Model.getInstance().getCurrentOrder();
            String str = loadInfo("https://maps.googleapis.com/maps/api/directions/json?origin=" + currendOrder.getLatLngStart().latitude + "," + currendOrder.getLatLngStart().longitude + "&destination=" + currendOrder.getLatLngFinish().latitude + "," + currendOrder.getLatLngFinish().longitude + "&key=AIzaSyDY9dneEVpBzxlKVDGOEbHFdG2-Z_on30M");
            JSONObject jObject = new JSONObject(str);
            JSONArray routeArray = jObject.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes
                .getJSONObject("overview_polyline");
            String encodedString = overviewPolylines.getString("points");
            Model.getInstance().setPointWay(encodedString);
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    private String loadInfo(String strUrl) {
        String resultString = "";
        try {
            URL url = new URL(strUrl);
            StringBuilder text = new StringBuilder();
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.connect();
            InputStream in = (InputStream) conn.getContent();
            InputStreamReader isr = new InputStreamReader(in, "UTF-8");
            BufferedReader buff = new BufferedReader(isr);
            String line;
            do {
                line = buff.readLine();
                if (line != null)
                    text.append(line);
            } while (line != null);
            resultString = text.toString();
        } catch (Exception e) {
            Timber.e(e);
        }
        return resultString;
    }

    public void getInfo() {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        final String url = Model.getInstance().getUrlHost() + "script/get_info.php";
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
            response -> {
                try {
                    JSONArray jArrayCar = response.getJSONArray("typeCar");
                    Model.getInstance().getCarCard().clear();
                    for (int i = 0; i < jArrayCar.length(); i++) {
                        JSONObject jObjectCar = jArrayCar.getJSONObject(i);
                        Model.getInstance().getCarCard().add(new CarCard(Integer.parseInt(jObjectCar.getString("id")), jObjectCar.getString("name"), jObjectCar.getString("info"), Integer.parseInt(jObjectCar.getString("price")), Integer.parseInt(jObjectCar.getString("koef"))));
                    }
                    Model.getInstance().getParamCard().clear();
                    Model.getInstance().getParamCard().clear();
                    JSONArray jArrayText = response.getJSONArray("text");
                    for (int i = 0; i < jArrayText.length(); i++) {
                        JSONObject jObjectParam = jArrayText.getJSONObject(i);
                        Model.getInstance().getParamCard().add(new ParamCard(jObjectParam.getString("type"), jObjectParam.getString("value")));
                    }
                    JSONArray jArrayParam = response.getJSONArray("param");
                    for (int i = 0; i < jArrayParam.length(); i++) {
                        JSONObject jObjectParam = jArrayParam.getJSONObject(i);
                        Model.getInstance().getParamCard().add(new ParamCard(jObjectParam.getString("type"), jObjectParam.getString("value")));
                    }
                } catch (JSONException e) {
                    Timber.e(e);
                }
            },
            error -> {
            }
        );
        queue.add(getRequest);
    }
}