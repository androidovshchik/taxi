package com.cargotaxi;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cargotaxi.Model.Model;

import java.util.Calendar;
import java.util.TimeZone;

public class CustomerMenuTimeFragment extends Fragment {
    CustomerMap2Fragment.onEventListener eventListener;
    private RecyclerView rv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_menu_time, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Время подачи");
        rv = view.findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        RecyclerView.LayoutManager llm = new GridLayoutManager(getActivity().getApplicationContext(), 1);
        rv.setLayoutManager(llm);
        CardView cvSetDate = view.findViewById(R.id.cvSetDate);
        cvSetDate.setOnClickListener(v -> selectDate());
        reload();
        return view;
    }

    private void reload() {
        CustomerMenuTimeAdapter adapter = new CustomerMenuTimeAdapter(this, getActivity().getApplicationContext(), 0);
        rv.setAdapter(adapter);
    }

    private void selectDate() {
        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        DatePickerDialog.OnDateSetListener datePickerListener = (view, selectedYear, selectedMonth, selectedDay) -> {
            Model.getInstance().getCurrentOrder().setStrDate(selectedYear + "-" + (selectedMonth + 1) + "-" + selectedDay);
            selectTime();
        };
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), datePickerListener, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        datePicker.setCancelable(false);
        datePicker.show();
    }

    private void selectTime() {
        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        TimePickerDialog.OnTimeSetListener timePickerListener = (timePicker, selectedHour, selectedMinute) -> {
            Model.getInstance().getCurrentOrder().setStrDate(Model.getInstance().getCurrentOrder().getStrDate() + " " + selectedHour + ":" + selectedMinute + ":00");
            Model.getInstance().getCurrentOrder().setPosDate(3);
            eventListener.toMenuEvent();
        };
        TimePickerDialog timePicker = new TimePickerDialog(getActivity(), timePickerListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true);
        timePicker.setCancelable(false);
        timePicker.show();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            eventListener = (CustomerMap2Fragment.onEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    public interface onEventListener {
        void toMenuEvent();
    }
}