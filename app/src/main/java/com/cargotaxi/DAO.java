package com.cargotaxi;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.cargotaxi.Model.Model;
import com.cargotaxi.Model.OrderCard;

import timber.log.Timber;

public class DAO {
    private static SQLiteDatabase db;
    private static DAO instance;

    private DAO() {
    }

    public static synchronized DAO getInstance() {
        if (instance == null) {
            instance = new DAO();
        }
        return instance;
    }

    public void init(Context context) {
        try {
            DbOpenHelper dbOpenHelper = new DbOpenHelper(context);
            db = dbOpenHelper.getWritableDatabase();
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    public void deleteProfil() {
        db.delete(DbOpenHelper.ProfilTable, null, null);
    }

    public void saveProfil() {
        deleteProfil();
        ContentValues cv = new ContentValues();
        cv.put("idUser", Model.getInstance().getIdUser());
        cv.put("name", Model.getInstance().getNameUser());
        cv.put("surname", Model.getInstance().getSurnameUser());
        cv.put("phone", Model.getInstance().getPhoneUser());
        cv.put("password", Model.getInstance().getPasswordUser());
        cv.put("type", Model.getInstance().getTypeUser());
        cv.put("regDriver", Model.getInstance().isRegDriver());
        cv.put("idTypeCar", Model.getInstance().getIdTypeCar());
        cv.put("modelCar", Model.getInstance().getModelCar());
        cv.put("numberCar", Model.getInstance().getNumberCar());
        cv.put("regionCar", String.valueOf(Model.getInstance().getRegionCar()));
        cv.put("birthday", Model.getInstance().getBirthdayUser());
        cv.put("mail", "");
        cv.put("gender", Model.getInstance().getGenderUser());
        if (Model.getInstance().isToCenterDriver())
            cv.put("toCenter", "1");
        else
            cv.put("toCenter", "0");
        db.insert(DbOpenHelper.ProfilTable, null, cv);
    }

    public boolean loadProfil() {
        try {
            Cursor cursor = db.query(DbOpenHelper.ProfilTable, null, null, null, null, null, null);
            if (cursor.moveToFirst()) {
                do {
                    Model.getInstance().setIdUser(cursor.getInt(1));
                    Model.getInstance().setNameUser(cursor.getString(2));
                    Model.getInstance().setSurnameUser(cursor.getString(3));
                    Model.getInstance().setPhoneUser(cursor.getString(4));
                    Model.getInstance().setPasswordUser(cursor.getString(5));
                    Model.getInstance().setTypeUser(cursor.getInt(6));
                    Model.getInstance().setRegDriver(cursor.getInt(7));
                    Model.getInstance().setIdTypeCar(cursor.getInt(8));
                    Model.getInstance().setModelCar(cursor.getString(9));
                    Model.getInstance().setNumberCar(cursor.getString(10));
                    Model.getInstance().setBirthdayUser(cursor.getString(11));
                    Model.getInstance().setGenderUser(cursor.getInt(13));
                    Model.getInstance().setRegionCar(cursor.getInt(14));
                    if (cursor.getInt(15) == 1)
                        Model.getInstance().setToCenterDriver(true);
                    else
                        Model.getInstance().setToCenterDriver(false);
                    return true;
                } while (cursor.moveToNext());
            }
            if ((cursor != null) && !(cursor.isClosed()))
                cursor.close();
        } catch (Exception ignored) {
        }
        return false;
    }

    public int loadIdUser() {
        try {
            Cursor cursor = db.query(DbOpenHelper.ProfilTable, null, null, null, null, null, null);
            if (cursor.moveToFirst()) {
                do {
                    return cursor.getInt(1);
                } while (cursor.moveToNext());
            }
            if ((cursor != null) && !(cursor.isClosed()))
                cursor.close();
        } catch (Exception ignored) {
        }
        return 0;
    }

    public void deleteCurOrder() {
        db.delete(DbOpenHelper.CurOrderTable, null, null);
    }

    public void saveCurOrder() {
        deleteCurOrder();
        ContentValues cv = new ContentValues();
        cv.put("id", String.valueOf(Model.getInstance().getCurrentOrder().getId()));
        cv.put("typeCar", Model.getInstance().getCurrentOrder().getIdTypeCar());
        cv.put("latStart", Model.getInstance().getCurrentOrder().getLatLngStart().latitude);
        cv.put("lngStart", Model.getInstance().getCurrentOrder().getLatLngStart().longitude);
        cv.put("addressStart", Model.getInstance().getCurrentOrder().getAddressStart());
        cv.put("cityStart", Model.getInstance().getCurrentOrder().getCityStart());
        cv.put("latFinish", Model.getInstance().getCurrentOrder().getLatLngFinish().latitude);
        cv.put("lngFinish", Model.getInstance().getCurrentOrder().getLatLngFinish().longitude);
        cv.put("addressFinish", Model.getInstance().getCurrentOrder().getAddressFinish());
        cv.put("cityFinish", Model.getInstance().getCurrentOrder().getCityFinish());
        cv.put("ts", Model.getInstance().getCurrentOrder().getStrDate());
        cv.put("period", Model.getInstance().getCurrentOrder().getPeriod());
        cv.put("countPorter", Model.getInstance().getCurrentOrder().getCountPorter());
        cv.put("toCenter", Model.getInstance().getCurrentOrder().isToCenter());
        cv.put("toSuburb", Model.getInstance().getCurrentOrder().isToSuburb());
        cv.put("priceCar", Model.getInstance().getCurrentOrder().getPriceCar(0));
        cv.put("pricePorter", Model.getInstance().getCurrentOrder().getPricePorter(0));
        cv.put("priceDopCar", Model.getInstance().getCurrentOrder().getPriceDopCar());
        cv.put("priceDopPorter", Model.getInstance().getCurrentOrder().getPriceDopPorter());
        cv.put("comment", Model.getInstance().getCurrentOrder().getComment());
        db.insert(DbOpenHelper.CurOrderTable, null, cv);
    }

    public void loadCurOrder() {
        try {
            Cursor cursor = db.query(DbOpenHelper.CurOrderTable, null, null, null, null, null, null);
            if (cursor.moveToFirst()) {
                do {
                    Model.getInstance().setCurrentOrder(new OrderCard(cursor.getInt(1), cursor.getString(3), cursor.getString(4), cursor.getString(7), cursor.getString(8), cursor.getString(5),
                        cursor.getString(6), cursor.getString(9), cursor.getString(10), cursor.getInt(12), cursor.getInt(13), cursor.getString(14), cursor.getString(15),
                        cursor.getInt(16), cursor.getInt(17), cursor.getInt(18), cursor.getInt(19), cursor.getString(20), "", "", cursor.getInt(2),
                        0, 0, 0, 0, "", "", "", "", 0, "", "0", "0", "", 0));
                    Model.getInstance().getCurrentOrder().setStatus(2);
                    return;
                } while (cursor.moveToNext());
            }
            if ((cursor != null) && !(cursor.isClosed()))
                cursor.close();
        } catch (Exception bn) {
            bn.printStackTrace();
        }
    }
}