package com.cargotaxi;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.arsy.maps_library.MapRadar;
import com.cargotaxi.Model.Model;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.button.MaterialButton;

public class RadarSearchFragment extends Fragment {
    private GoogleMap googleMap;
    private MapRadar mapRadar;

    void clearMap() {
        try {
            mapRadar.stopRadarAnimation();
            googleMap.clear();
            googleMap = null;
        } catch (Exception ignored) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Model.getInstance().setCurrentSetAddress(1);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Поиск машины...");
        View root = inflater.inflate(R.layout.fragment_radar_search, null);
        MaterialButton btnCancel = root.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(v -> {
            clearMap();
            ((RadarActivity) getActivity()).deleteOrder(Model.getInstance().getCurrentOrder().getId());
        });
        TextView tvNameAddress1 = root.findViewById(R.id.tvNameAddress1);
        tvNameAddress1.setText(Model.getInstance().getCurrentOrder().getAddressStart());
        TextView tvNameAddress2 = root.findViewById(R.id.tvNameAddress2);
        tvNameAddress2.setText(Model.getInstance().getCurrentOrder().getAddressFinish());
        TextView tvInfoAddress1 = root.findViewById(R.id.tvInfoAddress1);
        tvInfoAddress1.setText(Model.getInstance().getCurrentOrder().getCityStart());
        TextView tvInfoAddress2 = root.findViewById(R.id.tvInfoAddress2);
        tvInfoAddress2.setText(Model.getInstance().getCurrentOrder().getCityFinish());
        TextView tvTypeCar = root.findViewById(R.id.tvTypeCar);
        tvTypeCar.setText(String.valueOf(Model.getInstance().getCurrentOrder().getCardNameTypeCar()));
        TextView tvTime = root.findViewById(R.id.tvTime);
        tvTime.setText(String.valueOf(Model.getInstance().getCurrentOrder().getTimeToRadarCustomer()));
        TextView tvCountPorter = root.findViewById(R.id.tvCountPorter);
        tvCountPorter.setText(String.valueOf(Model.getInstance().getCurrentOrder().getCountPorter()));
        TextView tvPeriod = root.findViewById(R.id.tvPeriod);
        tvPeriod.setText(String.valueOf(Model.getInstance().getCurrentOrder().getPeriodToRadar()));
        TextView tvPrice = root.findViewById(R.id.tvPrice);
        tvPrice.setText(Model.getInstance().getStrCurrency(Model.getInstance().getCurrentOrder().getPriceCar(0) + Model.getInstance().getCurrentOrder().getPricePorter(0)));
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        mapFragment.getMapAsync(googleMap1 -> {
            googleMap = googleMap1;
            LatLng latLng = new LatLng(Model.getInstance().getCurrentOrder().getLatLngStart().latitude, Model.getInstance().getCurrentOrder().getLatLngStart().longitude);
            googleMap.addMarker(new MarkerOptions().position(Model.getInstance().getCurrentOrder().getLatLngStart())
                .title(Model.getInstance().getCurrentOrder().getAddressStart())
                .anchor(0.5f, 0.5f)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_radar)));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 9.0f));
            googleMap.setOnMapClickListener(latLng1 -> {
            });
            mapRadar = new MapRadar(googleMap, latLng, getContext());
            mapRadar.withDistance(60000);
            mapRadar.withClockwiseAnticlockwiseDuration(2);
            mapRadar.withOuterCircleStrokeColor(Color.parseColor("#1E88E5"));
            mapRadar.withRadarColors(Color.parseColor("#001E88E5"), Color.parseColor("#ff1E88E5"));
            mapRadar.withOuterCircleTransparency(0.5f);
            mapRadar.withRadarTransparency(0.5f);
            mapRadar.startRadarAnimation();
        });
        getChildFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();
        return root;
    }
}