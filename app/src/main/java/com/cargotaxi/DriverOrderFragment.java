package com.cargotaxi;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cargotaxi.Model.Model;
import com.cargotaxi.Model.OrderCard;
import com.google.android.material.button.MaterialButton;

import java.util.HashMap;
import java.util.Map;

public class DriverOrderFragment extends Fragment {
    RecyclerView rv;
    private MaterialButton btnRadius1;
    private MaterialButton btnRadius2;
    private boolean bigRadius = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_order, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");
        ((DriverActivity) getActivity()).btnSwitchActiveDriver.setVisibility(View.VISIBLE);
        rv = view.findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        RecyclerView.LayoutManager llm = new GridLayoutManager(getActivity().getApplicationContext(), 1);
        rv.setLayoutManager(llm);
        btnRadius1 = view.findViewById(R.id.btnRadius1);
        btnRadius2 = view.findViewById(R.id.btnRadius2);
        btnRadius1.setChecked(true);
        btnRadius1.setOnClickListener(v -> {
            bigRadius = false;
            btnRadius1.setChecked(true);
            btnRadius2.setChecked(false);
            reload();
        });
        btnRadius2.setOnClickListener(v -> {
            bigRadius = true;
            btnRadius2.setChecked(true);
            btnRadius1.setChecked(false);
            reload();
        });
        reload();
        return view;
    }

    void changeNewOrder() {
        reload();
    }

    void reload() {
        rv.removeAllViews();
        Model.getInstance().getCurrentOrderCard().clear();
        for (OrderCard orderCard : Model.getInstance().getOrderCard())
            if (orderCard.getStatus() == 0)
                if (!Model.getInstance().isCancelOrderById(orderCard.getId())) {
                    if ((!bigRadius) && (orderCard.getIntDistance() < 10))
                        Model.getInstance().getCurrentOrderCard().add(orderCard);
                    else if ((bigRadius) && (orderCard.getIntDistance() < 30))
                        Model.getInstance().getCurrentOrderCard().add(orderCard);
                }
        boolean stat = true;
        if ((!Model.getInstance().isWorkDriver()) || (!Model.getInstance().isStatusDriver()))
            stat = false;
        DriverOrderAdapter driverOrderAdapter = new DriverOrderAdapter(this, getActivity().getApplicationContext(), stat);
        rv.setAdapter(driverOrderAdapter);
        if (Model.getInstance().getCurrentOrderCard().size() > 0)
            rv.setVisibility(View.VISIBLE);
        else
            rv.setVisibility(View.INVISIBLE);
    }

    public void takeOrder(int idOrder) {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/take_order.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idOrder", String.valueOf(idOrder));
                params.put("idUserDriver", String.valueOf(Model.getInstance().getIdUser()));
                return params;
            }
        };
        queue.add(postRequest);
    }
}