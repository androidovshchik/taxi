package com.cargotaxi;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.button.MaterialButton;

public class DriverRegStartFragment extends Fragment {
    private MaterialButton btnStart;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_reg_start, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Регистрация");
        btnStart = view.findViewById(R.id.btnStart);
        return view;
    }

    void enableInternet(boolean enable) {
        try {
            btnStart.setEnabled(enable);
            if (enable)
                btnStart.setText("Пройти регистрацию");
            else
                btnStart.setText("Нет интернета");
        } catch (Exception ignored) {
        }
    }
}