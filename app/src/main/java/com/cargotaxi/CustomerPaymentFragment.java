package com.cargotaxi;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cargotaxi.Model.Model;
import com.google.android.material.button.MaterialButton;

import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

public class CustomerPaymentFragment extends Fragment {
    private MaterialButton btnSetCash;
    private MaterialButton btnSetCard;
    private boolean rbCash = true;
    private onEventListener eventListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_payment, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Способ оплаты");
        TextView tvInfo = view.findViewById(R.id.tvInfo);
        tvInfo.setText(Model.getInstance().getParamCardByType("customer_payment").getStringValue());
        btnSetCash = view.findViewById(R.id.btnSetCash);
        btnSetCard = view.findViewById(R.id.btnSetCard);
        btnSetCash.setOnClickListener(v -> {
            btnSetCard.setBackgroundTintList(ContextCompat.getColorStateList(getActivity().getApplicationContext(), R.color.colorButtonEnabled));
            btnSetCard.setTextColor(Color.parseColor("#848484"));
            btnSetCash.setBackgroundTintList(ContextCompat.getColorStateList(getActivity().getApplicationContext(), R.color.colorPrimary));
            btnSetCash.setTextColor(Color.parseColor("#ffffff"));
            rbCash = true;
        });
        btnSetCard.setOnClickListener(v -> {
            btnSetCard.setBackgroundTintList(ContextCompat.getColorStateList(getActivity().getApplicationContext(), R.color.colorPrimary));
            btnSetCard.setTextColor(Color.parseColor("#ffffff"));
            btnSetCash.setBackgroundTintList(ContextCompat.getColorStateList(getActivity().getApplicationContext(), R.color.colorButtonEnabled));
            btnSetCash.setTextColor(Color.parseColor("#848484"));
            rbCash = false;
        });
        btnSetCard.setBackgroundTintList(ContextCompat.getColorStateList(getActivity().getApplicationContext(), R.color.colorButtonEnabled));
        btnSetCard.setTextColor(Color.parseColor("#848484"));
        TextView tvCostDriver = view.findViewById(R.id.tvCostDriver);
        long priceCar = Math.round(Model.getInstance().getCurrentOrder().getCurrentCarCard().getMinPrice() * (1 + Model.getInstance().getCurrentOrder().getPeriod() * Model.getInstance().getCurrentOrder().getCurrentCarCard().getKoef() / 100f));
        long pricePorter = (long) (Model.getInstance().getParamCardByType("price_porter").getIntValue() * Model.getInstance().getCurrentOrder().getCountPorter() * (1 + Model.getInstance().getCurrentOrder().getPeriod() * Model.getInstance().getParamCardByType("percent_porter").getIntValue() / 100f));
        tvCostDriver.setText(String.valueOf(priceCar));
        TextView tvCostPorter = view.findViewById(R.id.tvCostPorter);
        tvCostPorter.setText(String.valueOf(pricePorter));
        TextView tvCostAll = view.findViewById(R.id.tvCostAll);
        tvCostAll.setText(String.valueOf(priceCar + pricePorter));
        MaterialButton btnPayment = view.findViewById(R.id.btnPayment);
        btnPayment.setOnClickListener(view1 -> {
            if (Model.getInstance().isActiveOrder()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder
                    .setMessage("У вас есть незавершенный заказ")
                    .setCancelable(false)
                    .setNegativeButton("ОК",
                        (dialog, id) -> dialog.cancel());
                AlertDialog alert = builder.create();
                alert.show();
            } else {
                RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
                String url = Model.getInstance().getUrlHost() + "script/add_order.php";
                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                    response -> {
                        try {
                            JSONObject jObject = new JSONObject(response);
                            if (jObject.getBoolean("status")) {
                                Model.getInstance().getCurrentOrder().setId(jObject.getInt("idOrder"));
                                DAO.getInstance().saveCurOrder();
                                eventListener.toRadarEvent();
                            }
                        } catch (Exception e) {
                            Timber.e(e);
                        }
                    },
                    error -> {
                    }
                ) {
                    @Override
                    protected Map<String, String> getParams() {
                        if (Model.getInstance().getCurrentOrder().getPosDate() < 3) {
                            Calendar cal = Calendar.getInstance();
                            cal.add(Calendar.HOUR_OF_DAY, Model.getInstance().getCurrentOrder().getPosDate() + 1);
                            Model.getInstance().getCurrentOrder().setStrDate(new Timestamp(cal.getTimeInMillis()).toString());
                        }
                        Map<String, String> params = new HashMap<>();
                        params.put("typeCar", String.valueOf(Model.getInstance().getCurrentOrder().getIdTypeCar()));
                        params.put("latStart", String.valueOf(Model.getInstance().getCurrentOrder().getLatLngStart().latitude));
                        params.put("lngStart", String.valueOf(Model.getInstance().getCurrentOrder().getLatLngStart().longitude));
                        params.put("addressStart", String.valueOf(Model.getInstance().getCurrentOrder().getAddressStart()));
                        params.put("cityStart", String.valueOf(Model.getInstance().getCurrentOrder().getCityStart()));
                        params.put("latFinish", String.valueOf(Model.getInstance().getCurrentOrder().getLatLngFinish().latitude));
                        params.put("lngFinish", String.valueOf(Model.getInstance().getCurrentOrder().getLatLngFinish().longitude));
                        params.put("addressFinish", String.valueOf(Model.getInstance().getCurrentOrder().getAddressFinish()));
                        params.put("cityFinish", String.valueOf(Model.getInstance().getCurrentOrder().getCityFinish()));
                        params.put("idPlaceStart", "-");
                        params.put("idPlaceFinish", "-");
                        params.put("ts", Model.getInstance().getCurrentOrder().getStrDate());
                        params.put("period", String.valueOf(Model.getInstance().getCurrentOrder().getPeriod()));
                        params.put("countPorter", String.valueOf(Model.getInstance().getCurrentOrder().getCountPorter()));
                        params.put("toCenter", String.valueOf(Model.getInstance().getCurrentOrder().isToCenter()));
                        params.put("toSuburb", String.valueOf(Model.getInstance().getCurrentOrder().isToSuburb()));
                        params.put("priceCar", String.valueOf(Model.getInstance().getCurrentOrder().getPriceCar(0)));
                        params.put("pricePorter", String.valueOf(Model.getInstance().getCurrentOrder().getPricePorter(0)));
                        params.put("priceDopCar", String.valueOf(Model.getInstance().getCurrentOrder().getPriceDopCar()));
                        params.put("priceDopPorter", String.valueOf(Model.getInstance().getCurrentOrder().getPriceDopPorter()));
                        params.put("comment", Model.getInstance().getCurrentOrder().getComment());
                        params.put("idCustomer", String.valueOf(Model.getInstance().getIdUser()));
                        if (rbCash)
                            params.put("typePayment", "1");
                        else
                            params.put("typePayment", "2");
                        Model.getInstance().getCurrentOrder().setStatus(1);
                        return params;
                    }
                };
                queue.add(postRequest);
            }
        });
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            eventListener = (onEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    public interface onEventListener {
        void toRadarEvent();
    }
}