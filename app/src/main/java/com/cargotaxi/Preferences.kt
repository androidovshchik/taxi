package com.cargotaxi

import android.content.Context
import com.chibatching.kotpref.KotprefModel

class Preferences(context: Context) : KotprefModel(context) {

    override val kotprefName = "${context.packageName}_preferences"

    var readTerms by booleanPref(false, "read_terms")

    var viewSliders by booleanPref(false, "view_sliders")
}