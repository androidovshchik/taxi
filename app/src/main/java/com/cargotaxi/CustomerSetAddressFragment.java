package com.cargotaxi;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.cargotaxi.Model.Model;
import com.google.android.gms.common.api.ApiException;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomerSetAddressFragment extends Fragment {
    private TextInputEditText etAddress;
    private FloatingActionButton fabSearch;
    private PlacesClient placesClient;
    private String str2 = "";
    private ArrayList<HashMap<String, String>> arrayList;
    private LinearLayout llAddressList;
    private onEventListener eventListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Ввод адреса");
        View view = inflater.inflate(R.layout.fragment_customer_set_address, null);
        llAddressList = view.findViewById(R.id.llAddressList);
        etAddress = view.findViewById(R.id.etAddress);
        etAddress.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                reloadDriverList();
            }
        });
        etAddress.setOnKeyListener((view1, i, keyEvent) -> {
            if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                switch (i) {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                    case KeyEvent.KEYCODE_ENTER:
                        str2 = etAddress.getText().toString() + ", " + str2;
                        new getObjectByQuery().execute();
                        return false;
                    default:
                        break;
                }
            }
            return false;
        });
        Places.initialize(getActivity().getApplicationContext(), getResources().getString(R.string.key_places));
        placesClient = Places.createClient(getActivity().getApplicationContext());
        fabSearch = view.findViewById(R.id.fabSearch);
        fabSearch.setOnClickListener(v -> {
            str2 = etAddress.getText().toString() + ", " + str2;
            new getObjectByQuery().execute();
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(fabSearch.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        });
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            eventListener = (onEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    private void reloadDriverList() {
        llAddressList.removeAllViews();
        AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();
        FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
            .setCountry("ru")
            .setTypeFilter(TypeFilter.ADDRESS)
            .setSessionToken(token)
            .setQuery(etAddress.getText().toString())
            .build();
        placesClient.findAutocompletePredictions(request).addOnSuccessListener((response) -> {
            try {
                arrayList = new ArrayList<>();
                for (AutocompletePrediction prediction : response.getAutocompletePredictions()) {
                    LayoutInflater inflater = LayoutInflater.from(getContext());
                    RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.card_address_list, null, false);
                    CardView cv = layout.findViewById(R.id.cv);
                    cv.setOnClickListener(v -> {
                        etAddress.setText(prediction.getPrimaryText(null).toString() + " ");
                        etAddress.setSelection(etAddress.getText().length());
                        str2 = prediction.getSecondaryText(null).toString();
                        reloadDriverList();
                    });
                    TextView tvName = layout.findViewById(R.id.tvName);
                    tvName.setText(prediction.getPrimaryText(null).toString());
                    TextView tvInfo = layout.findViewById(R.id.tvInfo);
                    tvInfo.setText(prediction.getSecondaryText(null).toString());
                    llAddressList.addView(layout);
                }
                SimpleAdapter adapter = new SimpleAdapter(getActivity(), arrayList, android.R.layout.simple_list_item_2,
                    new String[]{"name1", "name2"},
                    new int[]{android.R.id.text1, android.R.id.text2});
                adapter.notifyDataSetChanged();
            } catch (Exception ignored) {
            }
        }).addOnFailureListener((exception) -> {
            exception.printStackTrace();
            if (exception instanceof ApiException) {
                ApiException apiException = (ApiException) exception;
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        etAddress.setText("");
    }

    public interface onEventListener {
        void toMap1Event();

        void toMap2Event();
    }

    class getObjectByQuery extends AsyncTask<Context, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Context... arg0) {
            NetworkController.getInstance().getObjectByQuery(str2, Model.getInstance().getCurrentSetAddress());
            return null;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (Model.getInstance().getCurrentSetAddress() == 1)
                eventListener.toMap1Event();
            else
                eventListener.toMap2Event();
        }
    }
}