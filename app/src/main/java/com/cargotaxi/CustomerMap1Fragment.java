package com.cargotaxi;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.cargotaxi.Model.Model;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.button.MaterialButton;

import timber.log.Timber;

public class CustomerMap1Fragment extends Fragment {
    private TextView tvName;
    private TextView tvInfo;
    private TextView tvEmpty;
    private GoogleMap googleMap;
    private onEventListener eventListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Model.getInstance().setCurrentSetAddress(1);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Откуда");
        View view = inflater.inflate(R.layout.fragment_customer_map, null);
        tvEmpty = view.findViewById(R.id.tvEmpty);
        RelativeLayout rlSelectAddress = view.findViewById(R.id.rlSelectAddress);
        rlSelectAddress.setOnClickListener(v -> eventListener.toSetStartAddressEvent());
        MaterialButton btnNext = view.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(v -> eventListener.toMap2Event());
        tvName = view.findViewById(R.id.tvName);
        tvName.setText(Model.getInstance().getCurrentOrder().getAddressStart());
        if (tvName.length() > 5) tvEmpty.setVisibility(View.INVISIBLE);
        tvInfo = view.findViewById(R.id.tvInfo);
        tvInfo.setText(Model.getInstance().getCurrentOrder().getCityStart());
        SupportMapFragment mapFragment;
        {
            mapFragment = SupportMapFragment.newInstance();
            mapFragment.getMapAsync(googleMap1 -> {
                googleMap = googleMap1;
                setMapMarker();
            });
        }
        getChildFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            eventListener = (onEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    public void setMapMarker() {
        try {
            if (Model.getInstance().getCurrentOrder().getLatLngStart() != null) {
                LatLng latLng = new LatLng(Model.getInstance().getCurrentOrder().getLatLngStart().latitude, Model.getInstance().getCurrentOrder().getLatLngStart().longitude);
                googleMap.addMarker(new MarkerOptions().position(latLng)
                    .title(Model.getInstance().getCurrentOrder().getAddressStart())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker1)));
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18.0f));
                if (Model.getInstance().getCurrentOrder().getLatLngStart() != null)
                    new getObjectByCoord().execute();
            } else
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(55.751522, 37.617952), 12.0f));
            googleMap.setOnMapClickListener(latLng -> {
                googleMap.clear();
                Model.getInstance().getCurrentOrder().setLatLngStart(latLng);
                googleMap.addMarker(new MarkerOptions().position(latLng)
                    .title(Model.getInstance().getCurrentOrder().getAddressStart())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker1)));
                new getObjectByCoord().execute();
            });
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    public interface onEventListener {
        void toMap2Event();

        void toSetStartAddressEvent();
    }

    class getObjectByCoord extends AsyncTask<Context, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Context... arg0) {
            NetworkController.getInstance().getObjectByCoord(Model.getInstance().getCurrentOrder().getLatLngStart().latitude, Model.getInstance().getCurrentOrder().getLatLngStart().longitude, 1);
            return null;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            tvName.setText(Model.getInstance().getCurrentOrder().getAddressStart());
            tvInfo.setText(Model.getInstance().getCurrentOrder().getCityStart());
            if (tvName.length() > 5) tvEmpty.setVisibility(View.INVISIBLE);
        }
    }
}