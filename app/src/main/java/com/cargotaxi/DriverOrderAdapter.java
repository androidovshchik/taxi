package com.cargotaxi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.cargotaxi.Model.Model;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;

public class DriverOrderAdapter extends RecyclerView.Adapter<DriverOrderAdapter.CardViewHolder> {
    private static DriverOrderFragment fragment = null;
    private final boolean enable;

    public DriverOrderAdapter(DriverOrderFragment fragment, Context context, boolean enable) {
        DriverOrderAdapter.fragment = fragment;
        this.enable = enable;
    }

    @Override
    public int getItemCount() {
        return Model.getInstance().getCurrentOrderCard().size();
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_order, viewGroup, false);
        return new CardViewHolder(v, i);
    }

    @Override
    public void onBindViewHolder(final CardViewHolder personViewHolder, final int i) {
        personViewHolder.tvNameAddress1.setText(Model.getInstance().getCurrentOrderCard().get(i).getAddressStart());
        personViewHolder.tvInfoAddress1.setText(Model.getInstance().getCurrentOrderCard().get(i).getCityStart());
        personViewHolder.tvNameAddress2.setText(Model.getInstance().getCurrentOrderCard().get(i).getAddressFinish());
        personViewHolder.tvInfoAddress2.setText(Model.getInstance().getCurrentOrderCard().get(i).getCityFinish());
        personViewHolder.tvPriceCar.setText(Model.getInstance().getStrCurrency(Model.getInstance().getCurrentOrderCard().get(i).getPriceCar(0)));
        personViewHolder.tvPricePorter.setText(Model.getInstance().getStrCurrency(Model.getInstance().getCurrentOrderCard().get(i).getPricePorter(0)));
        personViewHolder.tvDopPriceCar.setText(Model.getInstance().getStrCurrency(Model.getInstance().getCurrentOrderCard().get(i).getPriceDopCar() / 60f));
        personViewHolder.tvDopPricePorter.setText(Model.getInstance().getStrCurrency(Model.getInstance().getCurrentOrderCard().get(i).getPriceDopPorter() / 60f));
        personViewHolder.tvTime.setText("--" + String.valueOf(Model.getInstance().getCurrentOrder().getTimeToRadar()).replace("\n", " "));
        personViewHolder.tvTime.setText(Model.getInstance().getCurrentOrderCard().get(i).getTimeToRadar());
        personViewHolder.tvTypeCar.setText(Model.getInstance().getCurrentOrderCard().get(i).getCardNameTypeCar());
        personViewHolder.tvCountPorter.setText(String.valueOf(Model.getInstance().getCurrentOrderCard().get(i).getCountPorter()));
        personViewHolder.tvPeriod.setText(String.valueOf(Model.getInstance().getCurrentOrderCard().get(i).getPeriodToRadar()));
        personViewHolder.tvToCenter.setText(Model.getInstance().getCurrentOrderCard().get(i).getToCenter());
        personViewHolder.tvToSuburb.setText(Model.getInstance().getCurrentOrderCard().get(i).getToSuburb());
        if (Model.getInstance().getCurrentOrderCard().get(i).getComment().length() == 0)
            personViewHolder.tvComment.setVisibility(View.GONE);
        personViewHolder.tvComment.setText(Model.getInstance().getCurrentOrderCard().get(i).getComment());
        personViewHolder.tvDistance.setText(Model.getInstance().getCurrentOrderCard().get(i).getDistance());
        if (Model.getInstance().getCurrentOrderCard().get(i).getIsTake() == 1) {
            personViewHolder.btnConfirm.setEnabled(false);
            personViewHolder.btnConfirm.setText("Ожидайте");
        } else {
            personViewHolder.btnConfirm.setText("Взять заказ");
        }
        if (!enable) {
            personViewHolder.btnCancel.setVisibility(View.GONE);
            personViewHolder.btnConfirm.setVisibility(View.GONE);
        }
        personViewHolder.btnCancel.setOnClickListener(view -> {
            Model.getInstance().getCancelOrder().add(Model.getInstance().getCurrentOrderCard().get(i).getId());
            Model.getInstance().getOrderCardById(Model.getInstance().getCurrentOrderCard().get(i).getId()).setStatus(2);
            Model.getInstance().getCurrentOrderCard().remove(personViewHolder.getAdapterPosition());
            notifyItemRemoved(personViewHolder.getAdapterPosition());
            notifyItemRangeChanged(personViewHolder.getAdapterPosition(), Model.getInstance().getCurrentOrderCard().size());
            if (Model.getInstance().getCurrentOrderCard().size() == 0)
                fragment.rv.setVisibility(View.GONE);
        });
        personViewHolder.btnConfirm.setOnClickListener(view -> {
            if (Model.getInstance().getBalanceDriver() > 0) {
                fragment.takeOrder(Model.getInstance().getCurrentOrderCard().get(i).getId());
                personViewHolder.btnConfirm.setText("Ожидайте");
                personViewHolder.btnConfirm.setEnabled(false);
                Model.getInstance().getCurrentOrderCard().get(i).setOnSelect(true);
            } else
                Snackbar.make(fragment.rv, "У вас недостаточно средств. Пополните баланс", Snackbar.LENGTH_SHORT)
                    .show();
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {
        final CardView cv;
        final TextView tvInfo;
        final MaterialButton btnCancel;
        final MaterialButton btnConfirm;
        final TextView tvNameAddress1;
        final TextView tvInfoAddress1;
        final TextView tvNameAddress2;
        final TextView tvInfoAddress2;
        final TextView tvPriceCar;
        final TextView tvPricePorter;
        final TextView tvTime;
        final TextView tvTypeCar;
        final TextView tvCountPorter;
        final TextView tvPeriod;
        final TextView tvToCenter;
        final TextView tvToSuburb;
        final TextView tvComment;
        final TextView tvDistance;
        final TextView tvDopPriceCar;
        final TextView tvDopPricePorter;

        CardViewHolder(View itemView, int item) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            tvInfo = itemView.findViewById(R.id.tvInfo);
            btnCancel = itemView.findViewById(R.id.btnCancel);
            btnConfirm = itemView.findViewById(R.id.btnConfirm);
            tvNameAddress1 = itemView.findViewById(R.id.tvNameAddress1);
            tvInfoAddress1 = itemView.findViewById(R.id.tvInfoAddress1);
            tvNameAddress2 = itemView.findViewById(R.id.tvNameAddress2);
            tvInfoAddress2 = itemView.findViewById(R.id.tvInfoAddress2);
            tvPriceCar = itemView.findViewById(R.id.tvPriceCar);
            tvPricePorter = itemView.findViewById(R.id.tvPricePorter);
            tvDopPriceCar = itemView.findViewById(R.id.tvDopPriceCar);
            tvDopPricePorter = itemView.findViewById(R.id.tvDopPricePorter);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvTypeCar = itemView.findViewById(R.id.tvTypeCar);
            tvCountPorter = itemView.findViewById(R.id.tvCountPorter);
            tvPeriod = itemView.findViewById(R.id.tvPeriod);
            tvToCenter = itemView.findViewById(R.id.tvToCenter);
            tvToSuburb = itemView.findViewById(R.id.tvToSuburb);
            tvComment = itemView.findViewById(R.id.tvComment);
            tvDistance = itemView.findViewById(R.id.tvDistance);
        }
    }
}
