package com.cargotaxi;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.cargotaxi.Model.Model;
import com.cargotaxi.Model.OrderCard;

public class DriverHistoryFragment extends Fragment {
    private LinearLayout llList;

    @Override
    public View onCreateView(
        @NonNull LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_customer_history_item, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("История заказов");
        ((DriverActivity) getActivity()).btnSwitchActiveDriver.setVisibility(View.GONE);
        llList = root.findViewById(R.id.llList);
        reloadDriverList();
        return root;
    }

    private void reloadDriverList() {
        llList.removeAllViews();
        for (int i = 0; i < Model.getInstance().getOrderCard().size(); i++) {
            if (Model.getInstance().getOrderCard().get(i).getStatus() == 6) {
                LayoutInflater inflater = LayoutInflater.from(getContext());
                RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.card_driver_history, null, false);
                OrderCard orderCard = Model.getInstance().getOrderCard().get(i);
                CardView cv = layout.findViewById(R.id.cv);
                TextView tvPrice = layout.findViewById(R.id.tvPrice);
                TextView tvNameAddress1 = layout.findViewById(R.id.tvNameAddress1);
                TextView tvInfoAddress1 = layout.findViewById(R.id.tvInfoAddress1);
                TextView tvNameAddress2 = layout.findViewById(R.id.tvNameAddress2);
                TextView tvInfoAddress2 = layout.findViewById(R.id.tvInfoAddress2);
                TextView tvPriceCar = layout.findViewById(R.id.tvPriceCar);
                TextView tvPricePorter = layout.findViewById(R.id.tvPricePorter);
                TextView tvTime = layout.findViewById(R.id.tvTime);
                TextView tvTypeCar = layout.findViewById(R.id.tvTypeCar);
                TextView tvCountPorter = layout.findViewById(R.id.tvCountPorter);
                TextView tvPeriod = layout.findViewById(R.id.tvPeriod);
                TextView tvDopPriceCar = layout.findViewById(R.id.tvDopPriceCar);
                TextView tvDopPricePorter = layout.findViewById(R.id.tvDopPricePorter);
                TextView tvToCenter = layout.findViewById(R.id.tvToCenter);
                TextView tvToSuburb = layout.findViewById(R.id.tvToSuburb);
                TextView tvComment = layout.findViewById(R.id.tvComment);
                TextView tvDistance = layout.findViewById(R.id.tvDistance);
                tvNameAddress1.setText(orderCard.getAddressStart());
                tvInfoAddress1.setText(orderCard.getCityStart());
                tvNameAddress2.setText(orderCard.getAddressFinish());
                tvInfoAddress2.setText(orderCard.getCityFinish());
                tvPriceCar.setText(Model.getInstance().getStrCurrency(orderCard.getPriceCar(0)));
                tvPricePorter.setText(Model.getInstance().getStrCurrency(orderCard.getPricePorter(0)));
                tvPrice.setText(Model.getInstance().getStrCurrency(orderCard.getPriceCar(0) + orderCard.getPricePorter(0)));
                if (orderCard.getId() == Model.getInstance().getCurrentOrder().getId()) {
                    tvPriceCar.setText(Model.getInstance().getStrCurrency(Model.getInstance().getCurrentOrder().getPriceCarFinal()));
                    tvPricePorter.setText(Model.getInstance().getStrCurrency(Model.getInstance().getCurrentOrder().getPricePorterFinal()));
                    tvPrice.setText(Model.getInstance().getStrCurrency(orderCard.getPriceCarFinal() + orderCard.getPricePorterFinal()));
                }
                tvTime.setText(String.valueOf(orderCard.getTimeToRadar()));
                tvTypeCar.setText(orderCard.getCardNameTypeCar());
                tvCountPorter.setText(String.valueOf(orderCard.getCountPorter()));
                tvPeriod.setText(String.valueOf(orderCard.getPeriod()));
                tvDopPriceCar.setText(Model.getInstance().getStrCurrency(orderCard.getPriceDopCar() / 60f));
                tvDopPricePorter.setText(Model.getInstance().getStrCurrency(orderCard.getPriceDopPorter() / 60f));
                tvToCenter.setText(orderCard.getToCenter());
                tvToSuburb.setText(orderCard.getToSuburb());
                tvComment.setText(orderCard.getComment());
                tvDistance.setText(orderCard.getDistance());
                llList.addView(layout);
            }
        }
    }
}