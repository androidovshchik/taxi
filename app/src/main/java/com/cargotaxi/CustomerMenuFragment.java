package com.cargotaxi;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cargotaxi.Model.Model;
import com.google.android.material.button.MaterialButton;

public class CustomerMenuFragment extends Fragment {
    private onEventListener eventListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_menu, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Меню заказа");
        Switch swToSuburb = view.findViewById(R.id.swToSuburb);
        swToSuburb.setOnCheckedChangeListener((buttonView, isChecked) -> Model.getInstance().getCurrentOrder().setToSuburb(isChecked));
        Switch swToCenter = view.findViewById(R.id.swToCenter);
        swToCenter.setOnCheckedChangeListener((buttonView, isChecked) -> Model.getInstance().getCurrentOrder().setToCenter(isChecked));
        TextView tvNameAddress1 = view.findViewById(R.id.tvNameAddress1);
        tvNameAddress1.setText(Model.getInstance().getCurrentOrder().getAddressStart());
        TextView tvNameAddress2 = view.findViewById(R.id.tvNameAddress2);
        tvNameAddress2.setText(Model.getInstance().getCurrentOrder().getAddressFinish());
        TextView tvInfoAddress1 = view.findViewById(R.id.tvInfoAddress1);
        tvInfoAddress1.setText(Model.getInstance().getCurrentOrder().getCityStart());
        TextView tvInfoAddress2 = view.findViewById(R.id.tvInfoAddress2);
        tvInfoAddress2.setText(Model.getInstance().getCurrentOrder().getCityFinish());
        CardView cvAddress1 = view.findViewById(R.id.cvAddress1);
        cvAddress1.setOnClickListener(v -> toMap1Event());
        CardView cvAddress2 = view.findViewById(R.id.cvAddress2);
        cvAddress2.setOnClickListener(v -> toMap2Event());
        RecyclerView rv = view.findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        RecyclerView.LayoutManager llm = new GridLayoutManager(getActivity().getApplicationContext(), 1);
        rv.setLayoutManager(llm);
        rv.setVisibility(View.VISIBLE);
        CustomerMenuAdapter formulaAdapter = new CustomerMenuAdapter(this, getActivity().getApplicationContext(), Model.getInstance().getCustomerMenuCard());
        rv.setAdapter(formulaAdapter);
        MaterialButton btnPayment = view.findViewById(R.id.btnPayment);
        long priceCar = Math.round(Model.getInstance().getCurrentOrder().getCurrentCarCard().getMinPrice() * (1 + Model.getInstance().getCurrentOrder().getPeriod() * Model.getInstance().getCurrentOrder().getCurrentCarCard().getKoef() / 100f));
        Model.getInstance().getCurrentOrder().setPriceCar((int) priceCar);
        Model.getInstance().getCurrentOrder().setPriceDopCar(Model.getInstance().getCurrentOrder().getCurrentCarCard().getMinPrice());
        long pricePorter = (long) (Model.getInstance().getParamCardByType("price_porter").getIntValue() * Model.getInstance().getCurrentOrder().getCountPorter() * (1 + Model.getInstance().getCurrentOrder().getPeriod() * Model.getInstance().getParamCardByType("percent_porter").getIntValue() / 100f));
        Model.getInstance().getCurrentOrder().setPricePorter((int) pricePorter);
        if (Model.getInstance().getCurrentOrder().getCountPorter() == 0)
            Model.getInstance().getCurrentOrder().setPriceDopPorter(0);
        else
            Model.getInstance().getCurrentOrder().setPriceDopPorter(Model.getInstance().getParamCardByType("price_porter").getIntValue());
        btnPayment.setText("Заказать за " + (priceCar + pricePorter) + " руб.");
        btnPayment.setOnClickListener(v -> eventListener.toPaymentEvent());
        return view;
    }

    private void toMap1Event() {
        eventListener.toMap1Event();
    }

    private void toMap2Event() {
        eventListener.toMap2Event();
    }

    void toTypeCarEvent() {
        eventListener.toTypeCarEvent();
    }

    void toTimeEvent() {
        eventListener.toTimeEvent();
    }

    void toPeriodEvent() {
        eventListener.toPeriodEvent();
    }

    void toPorterEvent() {
        eventListener.toPorterEvent();
    }

    void toCommentEvent() {
        eventListener.toCommentEvent();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            eventListener = (onEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    public interface onEventListener {
        void toMap1Event();

        void toMap2Event();

        void toTypeCarEvent();

        void toTimeEvent();

        void toPeriodEvent();

        void toPorterEvent();

        void toCommentEvent();

        void toPaymentEvent();
    }
}