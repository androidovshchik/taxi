package com.cargotaxi;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.cargotaxi.Model.Model;
import com.google.android.material.navigation.NavigationView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.jetbrains.anko.ToastsKt;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

public class DriverRegActivity extends AppCompatActivity implements DriverRegInfoFragment.onEventListener, DriverRegCarFragment.onEventListener {
    private final DriverRegStartFragment driverRegStartFragment = new DriverRegStartFragment();
    private final DriverRegInfoFragment driverRegInfoFragment = new DriverRegInfoFragment();
    private final DriverRegCarFragment driverRegCarFragment = new DriverRegCarFragment();
    private Bitmap bitmap;
    private MyReceiver receiver;
    private FragmentTransaction fTrans;
    private RequestQueue rQueue;

    private static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                    boolean filter) {
        float ratio = Math.min(
            maxImageSize / realImage.getWidth(),
            maxImageSize / realImage.getHeight());
        int width = Math.round(ratio * realImage.getWidth());
        int height = Math.round(ratio * realImage.getHeight());
        return Bitmap.createScaledBitmap(realImage, width,
            height, filter);
    }

    @Override
    public void toInfoEvent() {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, driverRegCarFragment);
        fTrans.commit();
    }

    @Override
    public void toRegEvent() {
        Intent intent = new Intent(this, DriverActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestMultiplePermissions();
        setContentView(R.layout.activity_driver_reg);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        receiver = new MyReceiver();
        registerReceiver(receiver, intentFilter);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(Color.parseColor("#1E1E1E"));
        toolbar.setNavigationOnClickListener(v -> finish());
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, driverRegStartFragment);
        fTrans.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
        }
    }

    public void onBtnStartRegClick(View view) {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, driverRegInfoFragment);
        fTrans.commit();
    }

    public void onBtnAddDriverPhotoClick(View view) {
        try {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, 1);
        } catch (Exception e) {
            Timber.e(e);
            ToastsKt.toast(getApplicationContext(), "Установите приложение для просмотра файлов");
        }
    }

    public void onBtnAddPhotoCarClick(View view) {
        try {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, 2);
        } catch (Exception e) {
            Timber.e(e);
            ToastsKt.toast(getApplicationContext(), "Установите приложение для просмотра файлов");
        }
    }

    public void onBtnAddPhotoDoc1Click(View view) {
        try {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            String[] mimeTypes = {"image/jpeg", "image/png"};
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            startActivityForResult(intent, 3);
        } catch (Exception e) {
            Timber.e(e);
            ToastsKt.toast(getApplicationContext(), "Установите приложение для просмотра файлов");
        }
    }

    public void onBtnAddPhotoDoc2Click(View view) {
        try {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            String[] mimeTypes = {"image/jpeg", "image/png"};
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            startActivityForResult(intent, 4);
        } catch (Exception e) {
            Timber.e(e);
            ToastsKt.toast(getApplicationContext(), "Установите приложение для просмотра файлов");
        }
    }

    public void onBtnAddPhotoDoc3Click(View view) {
        try {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            String[] mimeTypes = {"image/jpeg", "image/png"};
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            startActivityForResult(intent, 5);
        } catch (Exception e) {
            Timber.e(e);
            ToastsKt.toast(getApplicationContext(), "Установите приложение для просмотра файлов");
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (data != null) {
            Uri contentURI = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                Bitmap bitmap2 = scaleDown(bitmap, 500, false);
                switch (requestCode) {
                    case 1:
                        uploadImage(bitmap, "driver_photo");
                        driverRegInfoFragment.btnAddPhoto.setImageBitmap(bitmap2);
                        driverRegInfoFragment.loadUserPhoto = true;
                        break;
                    case 2:
                        uploadImage(bitmap, "car_photo");
                        driverRegCarFragment.btnAddPhotoCar.setImageBitmap(bitmap2);
                        driverRegCarFragment.loadRegCarPhoto = true;
                        break;
                    case 3:
                        uploadImage(bitmap, "regts1_photo");
                        driverRegCarFragment.btnAddRegTS1.setImageBitmap(bitmap2);
                        driverRegCarFragment.loadRegTS1Photo = true;
                        break;
                    case 4:
                        uploadImage(bitmap, "regts2_photo");
                        driverRegCarFragment.btnAddRegTS2.setImageBitmap(bitmap2);
                        driverRegCarFragment.loadRegTS2Photo = true;
                        break;
                    case 5:
                        uploadImage(bitmap, "license_photo");
                        driverRegCarFragment.btnAddLicense.setImageBitmap(bitmap2);
                        driverRegCarFragment.loadRegLicensePhoto = true;
                        break;
                }
            } catch (IOException e) {
                Timber.e(e);
                Toast.makeText(DriverRegActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void uploadImage(final Bitmap bitmaper, String type) {
        String upload_URL;
        upload_URL = Model.getInstance().getUrlHost() + "script/upload_" + type + ".php?";
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, upload_URL,
            response -> {
                rQueue.getCache().clear();
                try {
                    JSONObject jsonObject = new JSONObject(new String(response.data));
                    Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    Timber.e(e);
                }
            },
            error -> Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show()) {
            @Override
            protected Map<String, String> getParams() {
                return new HashMap<>();
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap = scaleDown(bitmap, 1000, false);
                bitmap.compress(Bitmap.CompressFormat.PNG, 50, byteArrayOutputStream);
                params.put("filename", new DataPart(Model.getInstance().getIdUser() + ".png", byteArrayOutputStream.toByteArray()));
                byteArrayOutputStream.reset();
                bitmap.recycle();
                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rQueue = Volley.newRequestQueue(DriverRegActivity.this);
        rQueue.add(volleyMultipartRequest);
    }

    private void requestMultiplePermissions() {
        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE)
            .withListener(new MultiplePermissionsListener() {
                @Override
                public void onPermissionsChecked(MultiplePermissionsReport report) {
                    if (report.areAllPermissionsGranted()) {
                    }
                    if (report.isAnyPermissionPermanentlyDenied()) {
                    }
                }

                @Override
                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                    token.continuePermissionRequest();
                }
            }).
            withErrorListener(error -> Toast.makeText(getApplicationContext(), "Some Error! ", Toast.LENGTH_SHORT).show())
            .onSameThread()
            .check();
    }

    public void onBtnExitClick(View view) {
        Intent intent = new Intent(getApplicationContext(), RegActivity.class);
        startActivity(intent);
        finish();
    }

    private void enableButton(boolean enable) {
        driverRegStartFragment.enableInternet(enable);
        driverRegInfoFragment.enableInternet(enable);
        driverRegCarFragment.enableInternet(enable);
    }

    public void onBtnRequisitesClick(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Model.getInstance().getUrlRequisites()));
        startActivity(browserIntent);
    }

    public void onBtnTermsClick(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Model.getInstance().getUrlTerms()));
        startActivity(browserIntent);
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(receiver);
        super.onDestroy();
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String actionOfIntent = intent.getAction();
            boolean isConnected = Model.getInstance().checkForInternet();
            if (actionOfIntent.equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                enableButton(isConnected);
            }
        }
    }
}
