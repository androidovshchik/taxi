package com.cargotaxi;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cargotaxi.Model.Model;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

public class RegActivity extends AppCompatActivity {
    private TextInputEditText etName;
    private TextInputEditText etPassword;
    private MaterialButton btnCustomer;
    private MaterialButton btnDriver;
    private RelativeLayout rlMain;
    private TextInputLayout tilName;
    private TextInputLayout tilPhone;
    private TextInputLayout tilPassword;
    private TextInputEditText etPhone;
    private int type = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Регистрация");
        tilName = findViewById(R.id.tilName);
        tilPhone = findViewById(R.id.tilPhone);
        tilPassword = findViewById(R.id.tilPassword);
        rlMain = findViewById(R.id.rlMain);
        btnCustomer = findViewById(R.id.btnCustomer);
        btnDriver = findViewById(R.id.btnDriver);
        btnDriver.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.colorButtonEnabled));
        btnDriver.setTextColor(Color.parseColor("#848484"));
        btnCustomer.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.colorPrimary));
        btnCustomer.setTextColor(Color.parseColor("#ffffff"));
        type = 1;
        etPhone = findViewById(R.id.etPhone);
        etName = findViewById(R.id.etName);
        etPassword = findViewById(R.id.etPassword);
        etPhone.setText("+7");
        etPhone.setOnFocusChangeListener((view, hasFocus) -> {
            if (etPhone.getSelectionStart() < 2) etPhone.setSelection(2);
        });
        etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() < 2) {
                    etPhone.setText("+7");
                    etPhone.setSelection(etPhone.getText().length());
                }
                if (etPhone.getSelectionStart() < 2) etPhone.setSelection(2);
            }
        });
    }

    public void onBtnCustomerClick(View view) {
        type = 1;
        btnDriver.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.colorButtonEnabled));
        btnDriver.setTextColor(Color.parseColor("#848484"));
        btnCustomer.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.colorPrimary));
        btnCustomer.setTextColor(Color.parseColor("#ffffff"));
    }

    public void onBtnDriverClick(View view) {
        type = 2;
        btnCustomer.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.colorButtonEnabled));
        btnCustomer.setTextColor(Color.parseColor("#848484"));
        btnDriver.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.colorPrimary));
        btnDriver.setTextColor(Color.parseColor("#ffffff"));
    }

    public void onBtnRegClick(View view) {
        validation();
    }

    private void validation() {
        boolean isValid = true;
        if (tilName.getEditText().getText().toString().isEmpty()) {
            tilName.setError("Введите имя");
            isValid = false;
        } else {
            tilName.setErrorEnabled(false);
        }
        if (tilPhone.getEditText().getText().toString().length() < 5) {
            tilPhone.setError("Введите номер телефона");
            isValid = false;
        } else {
            tilPhone.setErrorEnabled(false);
        }
        if (tilPassword.getEditText().getText().toString().isEmpty()) {
            tilPassword.setError("Введите пароль");
            isValid = false;
        }
        if (isValid) {
            registration(etName.getText().toString(), etPhone.getText().toString(), etPassword.getText().toString(), type);
        }
    }

    public void onBtnToInputClick(View view) {
        Intent intent = new Intent(getApplicationContext(), InputActivity.class);
        startActivity(intent);
        finish();
    }

    public void onBtnSendSMSClick(View view) {
        sendSMS(etPhone.getText().toString());
    }

    private void registration(String strName, String strPhone, String strPassword, int intType) {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/reg_user.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
                try {
                    JSONObject jObject = new JSONObject(response);
                    boolean status = jObject.getBoolean("status");
                    if (!status) {
                        Snackbar.make(rlMain, jObject.getString("message"), Snackbar.LENGTH_SHORT).show();
                    } else {
                        Model.getInstance().setNameUser(jObject.getString("name"));
                        Model.getInstance().setPhoneUser(jObject.getString("phone"));
                        Model.getInstance().setIdUser(jObject.getInt("idUser"));
                        Model.getInstance().setTypeUser(jObject.getInt("typeUser"));
                        Model.getInstance().setRegDriver(jObject.getInt("regDriver"));
                        DAO.getInstance().saveProfil();
                        if (Model.getInstance().getTypeUser() == 1) {
                            Intent intent = new Intent(getApplicationContext(), CustomerActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        if (Model.getInstance().getTypeUser() == 2) {
                            Intent intent = new Intent(getApplicationContext(), DriverRegActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        if (Model.getInstance().getTypeUser() == 3) {
                            Intent intent = new Intent(getApplicationContext(), DriverActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                } catch (Exception e) {
                    Timber.e(e);
                }
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", strName);
                params.put("phone", strPhone);
                params.put("password", strPassword);
                params.put("type", String.valueOf(intType));
                return params;
            }
        };
        queue.add(postRequest);
    }

    private void sendSMS(String strPhone) {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/send_sms.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
                try {
                    JSONObject jObject = new JSONObject(response);
                    boolean status = jObject.getBoolean("status");
                    if (!status) {
                        Snackbar.make(rlMain, "Не удалось отправить. Попробуйте еще раз.", Snackbar.LENGTH_SHORT).show();
                    } else {
                        Snackbar.make(rlMain, "Ожидайте sms с паролем", Snackbar.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Timber.e(e);
                }
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", strPhone);
                return params;
            }
        };
        queue.add(postRequest);
    }
}
