package com.cargotaxi;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cargotaxi.Model.Model;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

public class DriverRegInfoFragment extends Fragment {
    ImageButton btnAddPhoto;
    boolean loadUserPhoto = false;
    boolean genderMan = true;
    private MaterialButton btnNext;
    private TextInputEditText etName;
    private TextInputEditText etSurname;
    private TextInputLayout layName;
    private TextInputLayout laySurname;
    private View view;
    private RelativeLayout rlAddPhoto;
    private onEventListener eventListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_driver_reg_info, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("О себе");
        rlAddPhoto = view.findViewById(R.id.rlAddPhoto);
        btnAddPhoto = view.findViewById(R.id.btnAddPhoto);
        etName = view.findViewById(R.id.etName);
        etSurname = view.findViewById(R.id.etSurname);
        layName = view.findViewById(R.id.layName);
        laySurname = view.findViewById(R.id.laySurname);
        btnNext = view.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(v -> validation());
        return view;
    }

    private void validation() {
        boolean isValid = true;
        if (layName.getEditText().getText().toString().isEmpty()) {
            layName.setError("Введите имя");
            isValid = false;
        } else {
            layName.setErrorEnabled(false);
        }
        if (laySurname.getEditText().getText().toString().isEmpty()) {
            laySurname.setError("Введите фамилию");
            isValid = false;
        } else {
            laySurname.setErrorEnabled(false);
        }
        if (!loadUserPhoto) {
            isValid = false;
            rlAddPhoto.setBackgroundResource(R.color.colorError);
        } else
            rlAddPhoto.setBackgroundColor(Color.TRANSPARENT);
        if (isValid) {
            sendInfoReg();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            eventListener = (onEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    private void sendInfoReg() {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/reg_driver_info.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
                try {
                    JSONObject jObject = new JSONObject(response);
                    if (jObject.getBoolean("status")) {
                        Model.getInstance().setNameUser(etName.getText().toString());
                        Model.getInstance().setSurnameUser(etSurname.getText().toString());
                        DAO.getInstance().saveProfil();
                        eventListener.toInfoEvent();
                    } else
                        Snackbar.make(view, "Данные не отправлены", Snackbar.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Timber.e(e);
                    Snackbar.make(view, "Данные не отправлены", Snackbar.LENGTH_SHORT).show();
                }
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(Model.getInstance().getIdUser()));
                params.put("name", etName.getText().toString());
                params.put("surname", etSurname.getText().toString());
                return params;
            }
        };
        queue.add(postRequest);
    }

    void enableInternet(boolean enable) {
        try {
            btnNext.setEnabled(enable);
            if (enable)
                btnNext.setText("Далее");
            else
                btnNext.setText("Нет интернета");
        } catch (Exception ignored) {
        }
    }

    public interface onEventListener {
        void toInfoEvent();
    }
}