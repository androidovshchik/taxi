package com.cargotaxi;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.cargotaxi.Model.Model;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class DriverPaymentFragment extends Fragment {
    RecyclerView rv;
    TextView tvBalance;
    private TextInputEditText etPayment;
    private TextInputLayout layPayment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_payment, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Оплата");
        try {
            etPayment = view.findViewById(R.id.etPayment);
            etPayment.setText(String.valueOf(2000));
            TextView tvInfo = view.findViewById(R.id.tvInfo);
            tvInfo.setText(Model.getInstance().getParamCardByType("driver_payment").getStringValue());
            tvBalance = view.findViewById(R.id.tvBalance);
            tvBalance.setText(Model.getInstance().getStrCurrency(Model.getInstance().getBalanceDriver()));
            layPayment = view.findViewById(R.id.layPayment);
            TextView tvCommission = view.findViewById(R.id.tvСommission);
            tvCommission.setText("Комиссия сервиса: " + Model.getInstance().getParamCardByType("driver_percent").getStringValue() + "%");
            MaterialButton btnPayment = view.findViewById(R.id.btnPayment);
            btnPayment.setOnClickListener(v -> validation());
            TextView tvAccount = view.findViewById(R.id.tvAccount);
            tvAccount.setText("Лицевой счет: " + Model.getInstance().getPhoneUser().substring(1));
        } catch (Exception ignored) {
        }
        return view;
    }

    private void validation() {
        boolean isValid = true;
        if (Integer.parseInt(layPayment.getEditText().getText().toString()) < 2) {
            layPayment.setError("Минимальная сумма 2000 рублей");
            isValid = false;
        } else {
            layPayment.setErrorEnabled(false);
        }
        if (isValid) {
            Model.getInstance().setCurPaymentDriver(etPayment.getText().toString());
            ((DriverActivity) getActivity()).toPay();
        }
    }

    void changePrice() {
        tvBalance.setText(Model.getInstance().getStrCurrency(Model.getInstance().getBalanceDriver()));
    }
}