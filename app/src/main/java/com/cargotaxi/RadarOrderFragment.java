package com.cargotaxi;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.cargotaxi.Model.DriverCard;
import com.cargotaxi.Model.Model;
import com.cargotaxi.Model.OrderCard;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.button.MaterialButton;

public class RadarOrderFragment extends Fragment {
    private GoogleMap googleMap;
    private ImageView ivBottomSheet;

    private static int convertDpToPixels(float dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Model.getInstance().setCurrentSetAddress(1);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("К вам приедет");
        View root = inflater.inflate(R.layout.fragment_radar_order, null);
        TextView tvInfo = root.findViewById(R.id.tvText);
        tvInfo.setText(Model.getInstance().getParamCardByType("customer_radar").getStringValue());
        ivBottomSheet = root.findViewById(R.id.ivBottomSheet);
        LinearLayout bottom_sheet = root.findViewById(R.id.bottom_sheet);
        BottomSheetBehavior sheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    case BottomSheetBehavior.STATE_SETTLING:
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        ivBottomSheet.setBackgroundResource(R.drawable.bottom_sheet1);
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        ivBottomSheet.setBackgroundResource(R.drawable.bottom_sheet);
                    }
                    break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {
            }
        });
        MaterialButton btnCancel = root.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(v -> {
            googleMap.clear();
            googleMap = null;
            ((RadarActivity) getActivity()).deleteOrder(Model.getInstance().getCurrentOrder().getId());
        });
        DriverCard currentDriverCard = Model.getInstance().getCurrentDriverCard();
        TextView tvNameDriver = root.findViewById(R.id.tvNameDriver);
        tvNameDriver.setText(currentDriverCard.getName());
        TextView tvModelCar = root.findViewById(R.id.tvModelCar);
        tvModelCar.setText(currentDriverCard.getModel() + " " + currentDriverCard.getNumber());
        TextView tvNameAddress1 = root.findViewById(R.id.tvNameAddress1);
        TextView tvInfoAddress1 = root.findViewById(R.id.tvInfoAddress1);
        TextView tvNameAddress2 = root.findViewById(R.id.tvNameAddress2);
        TextView tvInfoAddress2 = root.findViewById(R.id.tvInfoAddress2);
        TextView tvPriceCar = root.findViewById(R.id.tvPriceCar);
        TextView tvPricePorter = root.findViewById(R.id.tvPricePorter);
        TextView tvTime = root.findViewById(R.id.tvTime);
        TextView tvTypeCar = root.findViewById(R.id.tvTypeCar);
        TextView tvCountPorter = root.findViewById(R.id.tvCountPorter);
        TextView tvPeriod = root.findViewById(R.id.tvPeriod);
        TextView tvDopPriceCar = root.findViewById(R.id.tvDopPriceCar);
        TextView tvDopPricePorter = root.findViewById(R.id.tvDopPricePorter);
        TextView tvToCenter = root.findViewById(R.id.tvToCenter);
        TextView tvToSuburb = root.findViewById(R.id.tvToSuburb);
        TextView tvComment = root.findViewById(R.id.tvComment);
        TextView tvDistance = root.findViewById(R.id.tvDistance);
        OrderCard curOrder = Model.getInstance().getCurrentOrder();
        tvNameAddress1.setText(curOrder.getAddressStart());
        tvInfoAddress1.setText(curOrder.getCityStart());
        tvNameAddress2.setText(curOrder.getAddressFinish());
        tvInfoAddress2.setText(curOrder.getCityFinish());
        tvPriceCar.setText(Model.getInstance().getStrCurrency(curOrder.getPriceCar(0)));
        tvPricePorter.setText(Model.getInstance().getStrCurrency(curOrder.getPricePorter(0)));
        tvTime.setText(String.valueOf(curOrder.getTimeToRadarCustomer()).replace("\n", " "));
        tvTypeCar.setText(curOrder.getCardNameTypeCar());
        tvCountPorter.setText(String.valueOf(curOrder.getCountPorter()));
        tvPeriod.setText(String.valueOf(curOrder.getPeriod()));
        tvDopPriceCar.setText(Model.getInstance().getStrCurrency(curOrder.getPriceDopCar() / 60f));
        tvDopPricePorter.setText(Model.getInstance().getStrCurrency(curOrder.getPriceDopPorter() / 60f));
        tvToCenter.setText(curOrder.getToCenter());
        tvToSuburb.setText(curOrder.getToSuburb());
        tvComment.setText(curOrder.getComment());
        tvDistance.setText(curOrder.getDistance());
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        mapFragment.getMapAsync(googleMap1 -> {
            googleMap = googleMap1;
            googleMap.addMarker(new MarkerOptions().position(Model.getInstance().getCurrentOrder().getLatLngStart())
                .title(Model.getInstance().getCurrentOrder().getAddressStart())
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker1)));
            Marker mapMarker = googleMap.addMarker(new MarkerOptions().position(currentDriverCard.getLatLng())
                .title(currentDriverCard.getName()));
            loadMarkerIcon(mapMarker, currentDriverCard);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentDriverCard.getLatLng(), 11.0f));
        });
        getChildFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();
        return root;
    }

    private void loadMarkerIcon(final Marker marker, DriverCard currentDriverCard) {
        int w = convertDpToPixels(50, getContext());
        Glide.with(this).asBitmap().load(Model.getInstance().getUrlHost() + "photo/car_photo/" + currentDriverCard.getId() + ".png")
            .apply(new RequestOptions().override(w, w))
            .apply(RequestOptions.circleCropTransform())
            .into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(resource);
                    marker.setIcon(icon);
                }
            });
    }
}