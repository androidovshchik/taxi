package com.cargotaxi;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cargotaxi.Model.Model;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

public class CustomerProfilFragment extends Fragment {
    private TextInputEditText etPhone;
    private TextInputEditText etPassword;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_profil, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Личный кабинет");
        etPhone = view.findViewById(R.id.etPhone);
        etPhone.setText(Model.getInstance().getPhoneUser());
        etPassword = view.findViewById(R.id.etPassword);
        etPassword.setText(Model.getInstance().getPasswordUser());
        ImageButton btnEditPhone = view.findViewById(R.id.btnEditPhone);
        btnEditPhone.setOnClickListener(v -> editData(etPhone.getText().toString(), etPassword.getText().toString()));
        ImageButton btnEditPassword = view.findViewById(R.id.btnEditPassword);
        btnEditPassword.setOnClickListener(v -> editData(etPhone.getText().toString(), etPassword.getText().toString()));
        return view;
    }

    private void editData(String phone, String password) {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/edit_customer_data.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
                try {
                    JSONObject jObject = new JSONObject(response);
                    if (jObject.getBoolean("status")) {
                        Model.getInstance().setPhoneUser(jObject.getString("phone"));
                        Model.getInstance().setPasswordUser(jObject.getString("password"));
                        DAO.getInstance().saveProfil();
                        Toast.makeText(getActivity().getApplicationContext(), "Данные изменены", Toast.LENGTH_SHORT).show();
                    } else {
                        etPhone.setText(Model.getInstance().getPhoneUser());
                        etPassword.setText(Model.getInstance().getPasswordUser());
                        Toast.makeText(getActivity().getApplicationContext(), "Данные не изменены", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Timber.e(e);
                }
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idUser", String.valueOf(Model.getInstance().getIdUser()));
                params.put("phone", phone);
                params.put("password", password);
                return params;
            }
        };
        queue.add(postRequest);
    }
}