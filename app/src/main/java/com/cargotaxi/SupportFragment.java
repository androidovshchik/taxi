package com.cargotaxi;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cargotaxi.Model.Model;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONObject;

import timber.log.Timber;

public class SupportFragment extends Fragment {
    private View view;
    private TextInputEditText etComment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Техподдержка");
        view = inflater.inflate(R.layout.fragment_support, null);
        MaterialButton btnSend = view.findViewById(R.id.btnSend);
        btnSend.setOnClickListener(v -> {
            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            btnSendSupport();
        });
        etComment = view.findViewById(R.id.etComment);
        return view;
    }

    private void btnSendSupport() {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        final String url = Model.getInstance().getUrlHost() + "script/send_support.php?idUser=" + Model.getInstance().getIdUser() + "&text=" + etComment.getText().toString();
        StringRequest getRequest = new StringRequest(Request.Method.POST, url,
            response -> {
                try {
                    JSONObject jObject = new JSONObject(response);
                    if (jObject.getBoolean("status")) {
                        etComment.setText("");
                        Snackbar.make(view, "Ваше сообщение отправлено", Snackbar.LENGTH_SHORT).show();
                        if (Model.getInstance().getTypeUser() == 1)
                            ((CustomerActivity) getActivity()).toPrevFragment();
                        else
                            ((DriverActivity) getActivity()).toPrevFragment();
                    } else {
                        Snackbar.make(view, "Не удалось отправить сообщение", Snackbar.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Snackbar.make(view, "Не удалось отправить сообщение", Snackbar.LENGTH_SHORT).show();
                    Timber.e(e);
                }
            },
            error -> {
            }
        );
        queue.add(getRequest);
    }
}