package com.cargotaxi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cargotaxi.Model.Model;
import com.cargotaxi.Model.OrderCard;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import timber.log.Timber;

public class DriverRadarFragment extends Fragment {
    Timer mTimer;
    MyTimerTask mMyTimerTask;
    private TextView tvPricePorterTitle;
    private TextView tvPriceCarTitle;
    private TextView tvPriceCar;
    private TextView tvPricePorter;
    private GoogleMap googleMap;
    private MaterialButton btnCancel;
    private MaterialButton btnConfirm;
    private TextView tvTimer;
    private ImageView ivBottomSheet;
    private long timeStart;
    private int status = 3;
    private TextView tvText;
    private onEventListener eventListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_radar, null);
        status = 3;
        ivBottomSheet = view.findViewById(R.id.ivBottomSheet);
        LinearLayout bottom_sheet = view.findViewById(R.id.bottom_sheet);
        BottomSheetBehavior sheetBehavior = BottomSheetBehavior.from(bottom_sheet);
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    case BottomSheetBehavior.STATE_SETTLING:
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        ivBottomSheet.setBackgroundResource(R.drawable.bottom_sheet1);
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        ivBottomSheet.setBackgroundResource(R.drawable.bottom_sheet);
                    }
                    break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {
            }
        });
        TextView tvInfo = view.findViewById(R.id.tvInfo);
        tvText = view.findViewById(R.id.tvText);
        tvText.setText(Model.getInstance().getParamCardByType("driver_order_cell").getStringValue());
        tvTimer = view.findViewById(R.id.tvTimer);
        tvTimer.setVisibility(View.GONE);
        TextView tvNameAddress1 = view.findViewById(R.id.tvNameAddress1);
        TextView tvInfoAddress1 = view.findViewById(R.id.tvInfoAddress1);
        TextView tvNameAddress2 = view.findViewById(R.id.tvNameAddress2);
        TextView tvInfoAddress2 = view.findViewById(R.id.tvInfoAddress2);
        tvPriceCar = view.findViewById(R.id.tvPriceCar);
        tvPricePorter = view.findViewById(R.id.tvPricePorter);
        tvPricePorterTitle = view.findViewById(R.id.tvPricePorterTitle);
        tvPriceCarTitle = view.findViewById(R.id.tvPriceCarTitle);
        TextView tvTime = view.findViewById(R.id.tvTime);
        TextView tvTypeCar = view.findViewById(R.id.tvTypeCar);
        TextView tvCountPorter = view.findViewById(R.id.tvCountPorter);
        TextView tvPeriod = view.findViewById(R.id.tvPeriod);
        TextView tvDopPriceCar = view.findViewById(R.id.tvDopPriceCar);
        TextView tvDopPricePorter = view.findViewById(R.id.tvDopPricePorter);
        TextView tvToCenter = view.findViewById(R.id.tvToCenter);
        TextView tvToSuburb = view.findViewById(R.id.tvToSuburb);
        TextView tvComment = view.findViewById(R.id.tvComment);
        TextView tvDistance = view.findViewById(R.id.tvDistance);
        OrderCard curOrder = Model.getInstance().getCurrentOrder();
        tvNameAddress1.setText(curOrder.getAddressStart());
        tvInfoAddress1.setText(curOrder.getCityStart());
        tvNameAddress2.setText(curOrder.getAddressFinish());
        tvInfoAddress2.setText(curOrder.getCityFinish());
        tvPriceCar.setText(Model.getInstance().getStrCurrency(Model.getInstance().getCurrentOrder().getPriceCar(0)));
        tvPricePorter.setText(Model.getInstance().getStrCurrency(Model.getInstance().getCurrentOrder().getPricePorter(0)));
        tvTime.setText(String.valueOf(curOrder.getTimeToRadar()));
        tvTypeCar.setText(curOrder.getCardNameTypeCar());
        tvCountPorter.setText(String.valueOf(curOrder.getCountPorter()));
        tvPeriod.setText(String.valueOf(curOrder.getPeriod()));
        tvDopPriceCar.setText(Model.getInstance().getStrCurrency(curOrder.getPriceDopCar() / 60f));
        tvDopPricePorter.setText(Model.getInstance().getStrCurrency(curOrder.getPriceDopPorter() / 60f));
        tvToCenter.setText(curOrder.getToCenter());
        tvToSuburb.setText(curOrder.getToSuburb());
        tvComment.setText(curOrder.getComment());
        tvDistance.setText(curOrder.getDistance());
        btnCancel = view.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(v -> cancelOrder());
        btnConfirm = view.findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(v -> {
            if (status == 3)
                eventListener.toChangeStatus(4);
            else if (status == 4)
                eventListener.toChangeStatus(5);
            else if (status == 5)
                eventListener.toChangeStatus(6);
        });
        TextView tvNameCustomer = view.findViewById(R.id.tvName);
        tvNameCustomer.setText(Model.getInstance().getCurrentOrder().getNameCustomer());
        TextView tvPhoneCustomer = view.findViewById(R.id.tvPhone);
        tvPhoneCustomer.setText(Model.getInstance().getCurrentOrder().getPhoneCustomer());
        FloatingActionButton fabCall = view.findViewById(R.id.fabCall);
        fabCall.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", Model.getInstance().getCurrentOrder().getPhoneCustomer(), null));
            startActivity(intent);
        });
        SupportMapFragment mapFragment;
        {
            mapFragment = SupportMapFragment.newInstance();
            mapFragment.getMapAsync(googleMap1 -> {
                googleMap = googleMap1;
                googleMap.addMarker(new MarkerOptions().position(Model.getInstance().getCurrentOrder().getLatLngStart())
                    .title(Model.getInstance().getCurrentOrder().getAddressStart())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker1)));
                googleMap.addMarker(new MarkerOptions().position(Model.getInstance().getCurrentOrder().getLatLngFinish())
                    .title(Model.getInstance().getCurrentOrder().getAddressFinish())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker2)));
                new getObjectByCoord().execute();
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Model.getInstance().getCurrentOrder().getLatLngStart(), 13.0f));
                googleMap.setOnMapClickListener(latLng -> {
                });
            });
        }
        getChildFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();
        changeStatus(Model.getInstance().getCurrentOrder().getStatus());
        return view;
    }

    void changeStatus(int status) {
        if (status == 4) {
            this.status = 4;
            btnConfirm.setText("Приступил к работе");
            tvText.setText(Model.getInstance().getParamCardByType("driver_order_confirm").getStringValue());
        }
        if (status == 5) {
            this.status = 5;
            btnConfirm.setText("Работа завершена");
            tvText.setText(Model.getInstance().getParamCardByType("driver_order_ complete").getStringValue());
            btnCancel.setVisibility(View.GONE);
            tvTimer.setVisibility(View.VISIBLE);
            if (mTimer != null) {
                mTimer.cancel();
            }
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date parsedDate = dateFormat.parse(Model.getInstance().getCurrentOrder().getDate());
                timeStart = parsedDate.getTime();
            } catch (Exception ignored) {
            }
            mTimer = new Timer();
            mMyTimerTask = new MyTimerTask();
            mTimer.schedule(mMyTimerTask, 100, 100);
        }
        if (status == 6) {
            ((DriverActivity) getActivity()).toHistory();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            eventListener = (onEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    private void cancelOrder() {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/delete_order.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
                try {
                    JSONObject jObject = new JSONObject(response);
                    if (jObject.getBoolean("status"))
                        eventListener.toOrderList();
                } catch (Exception e) {
                    Timber.e(e);
                }
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idOrder", String.valueOf(Model.getInstance().getCurrentOrder().getId()));
                params.put("idUserDriver", String.valueOf(Model.getInstance().getIdUser()));
                return params;
            }
        };
        queue.add(postRequest);
    }

    public interface onEventListener {
        void toOrderList();

        void toChangeStatus(int status);
    }

    class MyTimerTask extends TimerTask {
        Long curTime;
        Long curTimeNach;

        @Override
        public void run() {
            curTime = Model.getInstance().getCurrentOrder().getPeriod() * 60 * 60 * 1000 - (System.currentTimeMillis() - timeStart);
            curTimeNach = curTime;
            if (curTime > 0) {
            } else {
                curTime = -curTime;
            }
            long hour, minute, second;
            String strHour, strMinute, strSecond;
            hour = TimeUnit.MILLISECONDS.toHours(curTime);
            minute = TimeUnit.MILLISECONDS.toMinutes(curTime) - hour * 60;
            second = TimeUnit.MILLISECONDS.toSeconds(curTime) - hour * 3600 - minute * 60;
            strHour = String.valueOf(hour);
            strMinute = String.valueOf(minute);
            if (strMinute.length() < 2) strMinute = "0" + strMinute;
            strSecond = String.valueOf(second);
            if (strSecond.length() < 2) strSecond = "0" + strSecond;
            String finalStrMinute = strMinute;
            String finalStrSecond = strSecond;
            try {
                getActivity().runOnUiThread(() -> {
                    if (curTimeNach > 0) {
                        tvTimer.setTextColor(Color.parseColor("#1E88E5"));
                        tvPricePorterTitle.setTextColor(Color.parseColor("#1E88E5"));
                        tvPriceCarTitle.setTextColor(Color.parseColor("#1E88E5"));
                        tvPriceCar.setTextColor(Color.parseColor("#1E88E5"));
                        tvPricePorter.setTextColor(Color.parseColor("#1E88E5"));
                        tvPriceCar.setText(Model.getInstance().getStrCurrency(Model.getInstance().getCurrentOrder().getPriceCar(0)));
                        tvPricePorter.setText(Model.getInstance().getStrCurrency(Model.getInstance().getCurrentOrder().getPricePorter(0)));
                    } else {
                        tvTimer.setTextColor(Color.parseColor("#ff0000"));
                        tvPricePorterTitle.setTextColor(Color.parseColor("#ff0000"));
                        tvPriceCarTitle.setTextColor(Color.parseColor("#ff0000"));
                        tvPriceCar.setTextColor(Color.parseColor("#ff0000"));
                        tvPricePorter.setTextColor(Color.parseColor("#ff0000"));
                        tvPriceCar.setText(Model.getInstance().getStrCurrency(Model.getInstance().getCurrentOrder().getPriceCar(TimeUnit.MILLISECONDS.toMinutes(curTime))));
                        tvPricePorter.setText(Model.getInstance().getStrCurrency(Model.getInstance().getCurrentOrder().getPricePorter(TimeUnit.MILLISECONDS.toMinutes(curTime))));
                    }
                    tvTimer.setText(strHour + ":" + finalStrMinute + ":" + finalStrSecond);
                });
            } catch (Exception ignored) {
            }
        }
    }

    class getObjectByCoord extends AsyncTask<Context, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Context... arg0) {
            NetworkController.getInstance().getRoutes();
            return null;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            List<LatLng> list = Model.getInstance().decodePoly(Model.getInstance().getPointWay());
            for (int z = 0; z < list.size() - 1; z++) {
                LatLng src = list.get(z);
                LatLng dest = list.get(z + 1);
                googleMap.addPolyline(new PolylineOptions()
                    .add(new LatLng(src.latitude, src.longitude),
                        new LatLng(dest.latitude, dest.longitude))
                    .width(5).color(Color.BLUE).geodesic(true));
            }
        }
    }
}