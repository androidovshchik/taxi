package com.cargotaxi;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

class CustomerHistoryPagerAdapter extends FragmentPagerAdapter {
    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.tab_text_1, R.string.tab_text_2};
    private final Context mContext;
    public CustomerHistoryItemFragment Fragment1;
    public CustomerHistoryItemFragment Fragment2;

    public CustomerHistoryPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public CustomerHistoryItemFragment getItem(int position) {
        if (position == 0) {
            Fragment1 = CustomerHistoryItemFragment.newInstance(position + 1);
            return Fragment1;
        }
        if (position == 1) {
            Fragment2 = CustomerHistoryItemFragment.newInstance(position + 1);
            return Fragment2;
        }
        return null;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        return 2;
    }
}