package com.cargotaxi;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Switch;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cargotaxi.Model.Model;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

public class DriverRegCarFragment extends Fragment {
    ImageButton btnAddPhotoCar, btnAddRegTS1, btnAddRegTS2, btnAddLicense;
    boolean loadRegCarPhoto = false;
    boolean loadRegTS1Photo = false;
    boolean loadRegTS2Photo = false;
    boolean loadRegLicensePhoto = false;
    private Switch swToCenter;
    private View view;
    private MaterialButton btnNext;
    private TextInputEditText etModelCar;
    private TextInputEditText etNumberCar;
    private TextInputEditText etTypeCar;
    private TextInputEditText etRegionCar;
    private int curSelectTypeCar = 0;
    private RelativeLayout rlAddAddLicense;
    private RelativeLayout rlAddAddRegTS2;
    private RelativeLayout rlAddAddRegTS1;
    private RelativeLayout rlAddPhotoCar;
    private TextInputLayout layModelCar;
    private TextInputLayout layNumberCar;
    private TextInputLayout layRegionCar;
    private onEventListener eventListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_driver_reg_car, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Об автомобиле");
        etModelCar = view.findViewById(R.id.etModelCar);
        etNumberCar = view.findViewById(R.id.etNumberCar);
        etRegionCar = view.findViewById(R.id.etRegionCar);
        rlAddAddLicense = view.findViewById(R.id.rlAddAddLicense);
        rlAddAddRegTS2 = view.findViewById(R.id.rlAddAddRegTS2);
        rlAddAddRegTS1 = view.findViewById(R.id.rlAddAddRegTS1);
        rlAddPhotoCar = view.findViewById(R.id.rlAddPhotoCar);
        swToCenter = view.findViewById(R.id.swToCenter);
        etTypeCar = view.findViewById(R.id.etTypeCar);
        curSelectTypeCar = 0;
        etTypeCar.setText(Model.getInstance().getCarCard().get(curSelectTypeCar).getType());
        etTypeCar.setKeyListener(null);
        etTypeCar.setOnFocusChangeListener((v, hasFocus) -> {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().getApplicationContext().INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etTypeCar.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
            if (hasFocus) {
                showMenu(etTypeCar);
            }
        });
        etTypeCar.setOnClickListener(v -> showMenu(etTypeCar));
        btnAddPhotoCar = view.findViewById(R.id.btnAddPhotoCar);
        btnAddRegTS1 = view.findViewById(R.id.btnAddRegTS1);
        btnAddRegTS2 = view.findViewById(R.id.btnAddRegTS2);
        btnAddLicense = view.findViewById(R.id.btnAddLicense);
        layModelCar = view.findViewById(R.id.layModelCar);
        layNumberCar = view.findViewById(R.id.layNumberCar);
        layRegionCar = view.findViewById(R.id.layRegionCar);
        btnNext = view.findViewById(R.id.btnNext);
        btnNext.setOnClickListener(v -> validation());
        return view;
    }

    private void sendInfoReg() {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/reg_driver_car.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
                try {
                    JSONObject jObject = new JSONObject(response);
                    if (jObject.getBoolean("status"))
                        if (jObject.getBoolean("status")) {
                            Model.getInstance().setRegDriver(1);
                            Model.getInstance().setModelCar(etModelCar.getText().toString());
                            Model.getInstance().setNumberCar(etNumberCar.getText().toString());
                            if (etRegionCar.getText().toString().length() > 0)
                                Model.getInstance().setRegionCar(Integer.parseInt(etRegionCar.getText().toString()));
                            Model.getInstance().setIdTypeCar(Model.getInstance().getCarCard().get(curSelectTypeCar).getId());
                            Model.getInstance().setToCenterDriver(swToCenter.isChecked());
                            DAO.getInstance().saveProfil();
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder
                                .setMessage("Спасибо за регистрацию. Вы сможете приступить к работе, после проверки ваших данных. Это займет не более 12 часов.")
                                .setCancelable(false)
                                .setNegativeButton("Продолжить",
                                    (dialog, id) -> {
                                        dialog.cancel();
                                        eventListener.toRegEvent();
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                        } else
                            Snackbar.make(view, "Данные не отправлены", Snackbar.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Timber.e(e);
                    Snackbar.make(view, "Данные не отправлены", Snackbar.LENGTH_SHORT).show();
                }
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(Model.getInstance().getIdUser()));
                params.put("model", etModelCar.getText().toString());
                params.put("number", etNumberCar.getText().toString());
                params.put("region", etRegionCar.getText().toString());
                params.put("idtype", String.valueOf(Model.getInstance().getCarCard().get(curSelectTypeCar).getId()));
                params.put("toCenter", String.valueOf(swToCenter.isChecked()));
                params.put("newDriver", String.valueOf(1));
                return params;
            }
        };
        queue.add(postRequest);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            eventListener = (onEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    private void showMenu(View anchor) {
        PopupMenu popup = new PopupMenu(getActivity().getApplicationContext(), anchor);
        popup.getMenuInflater().inflate(R.menu.activity_customer_drawer, popup.getMenu());
        popup.getMenu().clear();
        for (int i = 0; i < Model.getInstance().getCarCard().size(); i++) {
            MenuItem item = popup.getMenu().add(Menu.NONE, i, Menu.NONE, Model.getInstance().getCarCard().get(i).getType());
            item.setOnMenuItemClickListener(item1 -> {
                curSelectTypeCar = item1.getItemId();
                etTypeCar.setText(Model.getInstance().getCarCard().get(item1.getItemId()).getType());
                return true;
            });
        }
        popup.show();
    }

    private void validation() {
        boolean isValid = true;
        if (layModelCar.getEditText().getText().toString().isEmpty()) {
            layModelCar.setError("Введите марку автомобиля");
            isValid = false;
        } else {
            layModelCar.setErrorEnabled(false);
        }
        if (layNumberCar.getEditText().getText().toString().isEmpty()) {
            layNumberCar.setError("Введите номер автомобиля");
            isValid = false;
        } else {
            layNumberCar.setErrorEnabled(false);
        }
        if (layRegionCar.getEditText().getText().toString().isEmpty()) {
            layRegionCar.setError("Введите регион");
            isValid = false;
        } else {
            layRegionCar.setErrorEnabled(false);
        }
        if (!loadRegCarPhoto) {
            isValid = false;
            rlAddPhotoCar.setBackgroundResource(R.color.colorError);
        } else
            rlAddPhotoCar.setBackgroundColor(Color.TRANSPARENT);
        if (!loadRegTS1Photo) {
            isValid = false;
            rlAddAddRegTS1.setBackgroundResource(R.color.colorError);
        } else
            rlAddAddRegTS1.setBackgroundColor(Color.TRANSPARENT);
        if (!loadRegTS2Photo) {
            isValid = false;
            rlAddAddRegTS2.setBackgroundResource(R.color.colorError);
        } else
            rlAddAddRegTS2.setBackgroundColor(Color.TRANSPARENT);
        if (!loadRegLicensePhoto) {
            isValid = false;
            rlAddAddLicense.setBackgroundResource(R.color.colorError);
        } else
            rlAddAddLicense.setBackgroundColor(Color.TRANSPARENT);
        if (isValid) {
            sendInfoReg();
        }
    }

    void enableInternet(boolean enable) {
        try {
            btnNext.setEnabled(enable);
            if (enable)
                btnNext.setText("Зарегистрироваться");
            else
                btnNext.setText("Нет интернета");
        } catch (Exception ignored) {
        }
    }

    public interface onEventListener {
        void toRegEvent();
    }
}