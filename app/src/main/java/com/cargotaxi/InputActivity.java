package com.cargotaxi;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cargotaxi.Model.Model;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

public class InputActivity extends AppCompatActivity {
    private EditText etPhone;
    private EditText etPassword;
    private RelativeLayout rlMain;
    private TextInputLayout tilPhone;
    private TextInputLayout tilPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Вход");
        tilPhone = findViewById(R.id.tilPhone);
        tilPassword = findViewById(R.id.tilPassword);
        rlMain = findViewById(R.id.rlMain);
        etPhone = findViewById(R.id.etPhone);
        etPassword = findViewById(R.id.etPassword);
        etPhone.setText("+7");
        etPhone.setOnFocusChangeListener((view, hasFocus) -> {
            if (etPhone.getSelectionStart() < 2) etPhone.setSelection(2);
        });
        etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() < 2) {
                    etPhone.setText("+7");
                    etPhone.setSelection(etPhone.getText().length());
                }
                if (etPhone.getSelectionStart() < 2) etPhone.setSelection(2);
            }
        });
    }

    public void onBtnInputClick(View view) {
        validation();
    }

    private void validation() {
        boolean isValid = true;
        if (tilPhone.getEditText().getText().toString().length() < 5) {
            tilPhone.setError("Введите номер телефона");
            isValid = false;
        } else {
            tilPhone.setErrorEnabled(false);
        }
        if (tilPassword.getEditText().getText().toString().isEmpty()) {
            tilPassword.setError("Введите пароль");
            isValid = false;
        }
        if (isValid) {
            input(etPhone.getText().toString(), etPassword.getText().toString());
        }
    }

    private void input(String strPhone, String strPassword) {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/input_user.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
                try {
                    JSONObject jObject = new JSONObject(response);
                    boolean status = jObject.getBoolean("status");
                    if (!status) {
                        Snackbar.make(rlMain, jObject.getString("message"), Snackbar.LENGTH_SHORT).show();
                    } else {
                        Model.getInstance().setNameUser(jObject.getString("name"));
                        Model.getInstance().setPhoneUser(jObject.getString("phone"));
                        Model.getInstance().setIdUser(jObject.getInt("idUser"));
                        Model.getInstance().setTypeUser(jObject.getInt("typeUser"));
                        Model.getInstance().setRegDriver(jObject.getInt("regDriver"));
                        Model.getInstance().setPasswordUser(jObject.getString("password"));
                        Model.getInstance().setSurnameUser(jObject.getString("surname"));
                        Model.getInstance().setIdTypeCar(jObject.getInt("idTypeCar"));
                        Model.getInstance().setModelCar(jObject.getString("modelCar"));
                        Model.getInstance().setNumberCar(jObject.getString("numberCar"));
                        Model.getInstance().setRegionCar(jObject.getInt("regionCar"));
                        Model.getInstance().setBirthdayUser(jObject.getString("birthday"));
                        Model.getInstance().setGenderUser(jObject.getInt("gender"));
                        Model.getInstance().setToCenterDriver(jObject.getBoolean("toCenter"));
                        Model.getInstance().setToCenterDriver(true);
                        DAO.getInstance().saveProfil();
                        DAO.getInstance().loadProfil();
                        if (Model.getInstance().getTypeUser() == 1) {
                            Intent intent = new Intent(getApplicationContext(), CustomerActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        if (Model.getInstance().getTypeUser() == 2) {
                            if (Model.getInstance().isRegDriver() == 0) {
                                Intent intent = new Intent(getApplicationContext(), DriverRegActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                Intent intent = new Intent(getApplicationContext(), DriverActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    }
                } catch (Exception e) {
                    Timber.e(e);
                }
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("phone", strPhone);
                params.put("password", strPassword);
                return params;
            }
        };
        queue.add(postRequest);
    }
}
