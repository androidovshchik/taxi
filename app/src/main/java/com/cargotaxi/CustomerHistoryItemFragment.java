package com.cargotaxi;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.cargotaxi.Model.Model;
import com.cargotaxi.Model.OrderCard;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.chip.Chip;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import timber.log.Timber;

public class CustomerHistoryItemFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private LinearLayout llList;
    private TextView[] tvCountdown;
    private TextView[] tvPriceTotal;
    private TextView[] tvPricePorterTitle;
    private TextView[] tvPriceCarTitle;
    private TextView[] tvPriceCar;
    private TextView[] tvPricePorter;
    private int curType = 1;
    private ScrollView svMain;
    private GoogleMap googleMap;

    public static CustomerHistoryItemFragment newInstance(int index) {
        CustomerHistoryItemFragment fragment = new CustomerHistoryItemFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    private static int convertDpToPixels(float dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            curType = getArguments().getInt(ARG_SECTION_NUMBER);
        }
    }

    @Override
    public View onCreateView(
        @NonNull LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_customer_history_item, container, false);
        MaterialButton btnToList = root.findViewById(R.id.btnToList);
        btnToList.setOnClickListener(v -> svMain.setVisibility(View.VISIBLE));
        svMain = root.findViewById(R.id.svMain);
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        mapFragment.getMapAsync(googleMap1 -> googleMap = googleMap1);
        getChildFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();
        llList = root.findViewById(R.id.llList);
        reloadDriverList();
        return root;
    }

    public void reloadTimeDriverList() {
        getActivity().runOnUiThread(() -> {
            for (int i = 0; i < Model.getInstance().getOrderCard().size(); i++)
                if (((curType == 1) && (Model.getInstance().getOrderCard().get(i).getStatus() < 6) && (Model.getInstance().getOrderCard().get(i).getStatus() > 4)) || ((curType == 2) && (Model.getInstance().getOrderCard().get(i).getStatus() == 6))) {
                    try {
                        OrderCard orderCard = Model.getInstance().getOrderCard().get(i);
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date parsedDate = dateFormat.parse(orderCard.getDate());
                        long curTime = Model.getInstance().getOrderCard().get(i).getPeriod() * 60 * 60 * 1000 - (System.currentTimeMillis() - parsedDate.getTime());
                        long curTimeNach = curTime;
                        if (curTime > 0) {
                        } else {
                            curTime = -curTime;
                        }
                        long hour, minute, second;
                        String strHour, strMinute, strSecond;
                        hour = TimeUnit.MILLISECONDS.toHours(curTime);
                        minute = TimeUnit.MILLISECONDS.toMinutes(curTime) - hour * 60;
                        second = TimeUnit.MILLISECONDS.toSeconds(curTime) - hour * 3600 - minute * 60;
                        strHour = String.valueOf(hour);
                        strMinute = String.valueOf(minute);
                        if (strMinute.length() < 2) strMinute = "0" + strMinute;
                        strSecond = String.valueOf(second);
                        if (strSecond.length() < 2) strSecond = "0" + strSecond;
                        String finalStrMinute = strMinute;
                        String finalStrSecond = strSecond;
                        if (curTimeNach > 0) {
                            tvCountdown[i].setTextColor(Color.parseColor("#1E88E5"));
                            tvPriceCar[i].setTextColor(Color.parseColor("#1E88E5"));
                            tvPricePorter[i].setTextColor(Color.parseColor("#1E88E5"));
                            tvPricePorterTitle[i].setTextColor(Color.parseColor("#1E88E5"));
                            tvPriceCarTitle[i].setTextColor(Color.parseColor("#1E88E5"));
                            tvPriceTotal[i].setTextColor(Color.parseColor("#1E88E5"));
                            tvPriceCar[i].setText(Model.getInstance().getStrCurrency(orderCard.getPriceCar(0)));
                            tvPricePorter[i].setText(Model.getInstance().getStrCurrency(orderCard.getPricePorter(0)));
                            tvPriceTotal[i].setText(Model.getInstance().getStrCurrency(orderCard.getPriceCar(0) + orderCard.getPricePorter(0)));
                        } else {
                            tvCountdown[i].setTextColor(Color.parseColor("#ff0000"));
                            tvPricePorterTitle[i].setTextColor(Color.parseColor("#ff0000"));
                            tvPriceCarTitle[i].setTextColor(Color.parseColor("#ff0000"));
                            tvPriceCar[i].setTextColor(Color.parseColor("#ff0000"));
                            tvPricePorter[i].setTextColor(Color.parseColor("#ff0000"));
                            tvPriceTotal[i].setTextColor(Color.parseColor("#ff0000"));
                            tvPriceCar[i].setText(Model.getInstance().getStrCurrency(orderCard.getPriceCar(TimeUnit.MILLISECONDS.toMinutes(curTime))));
                            tvPricePorter[i].setText(Model.getInstance().getStrCurrency(orderCard.getPricePorter(TimeUnit.MILLISECONDS.toMinutes(curTime))));
                            tvPriceTotal[i].setText(Model.getInstance().getStrCurrency(orderCard.getPriceCar(TimeUnit.MILLISECONDS.toMinutes(curTime)) + orderCard.getPricePorter(TimeUnit.MILLISECONDS.toMinutes(curTime))));
                        }
                        tvCountdown[i].setText(strHour + ":" + finalStrMinute + ":" + finalStrSecond);
                    } catch (Exception e) {
                        Timber.e(e);
                    }
                }
        });
    }

    public int reloadDriverList() {
        int count1 = llList.getChildCount();
        llList.removeAllViews();
        Chip[] arrChip = new Chip[Model.getInstance().getOrderCard().size()];
        MaterialButton[] arrBtnShowCar = new MaterialButton[Model.getInstance().getOrderCard().size()];
        MaterialButton[] arrBtnCancel = new MaterialButton[Model.getInstance().getOrderCard().size()];
        tvCountdown = new TextView[Model.getInstance().getOrderCard().size()];
        tvPricePorterTitle = new TextView[Model.getInstance().getOrderCard().size()];
        tvPriceCarTitle = new TextView[Model.getInstance().getOrderCard().size()];
        tvPriceCar = new TextView[Model.getInstance().getOrderCard().size()];
        tvPricePorter = new TextView[Model.getInstance().getOrderCard().size()];
        tvPriceTotal = new TextView[Model.getInstance().getOrderCard().size()];
        int count = 0;
        for (int i = 0; i < Model.getInstance().getOrderCard().size(); i++) {
            if (((curType == 1) && (Model.getInstance().getOrderCard().get(i).getStatus() < 6) && (Model.getInstance().getOrderCard().get(i).getStatus() > 3)) || ((curType == 2) && (Model.getInstance().getOrderCard().get(i).getStatus() == 6))) {
                LayoutInflater inflater = LayoutInflater.from(getContext());
                RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.card_history, null, false);
                OrderCard orderCard = Model.getInstance().getOrderCard().get(i);
                CardView cv = layout.findViewById(R.id.cv);
                TextView tvModelCar = layout.findViewById(R.id.tvModelCar);
                tvModelCar.setText(orderCard.getModelCar());
                TextView tvNameDriver = layout.findViewById(R.id.tvNameDriver);
                tvNameDriver.setText(orderCard.getNameDriver());
                TextView tvPhoneDriver = layout.findViewById(R.id.tvPhoneDriver);
                tvPhoneDriver.setText(orderCard.getPhoneDriver());
                TextView tvNumberCar = layout.findViewById(R.id.tvNumberCar);
                tvNumberCar.setText(orderCard.getNumberCar());
                TextView tvRegionCar = layout.findViewById(R.id.tvRegionCar);
                tvRegionCar.setText(String.valueOf(orderCard.getRegionCar()));
                TextView tvTypeCar = layout.findViewById(R.id.tvTypeCar);
                tvTypeCar.setText(orderCard.getCardNameTypeCar());
                TextView tvPeriod = layout.findViewById(R.id.tvPeriod);
                tvPeriod.setText(String.valueOf(orderCard.getPeriod()));
                TextView tvTime = layout.findViewById(R.id.tvTime);
                tvTime.setText(orderCard.getTime());
                tvCountdown[i] = layout.findViewById(R.id.tvCountdown);
                tvPricePorterTitle[i] = layout.findViewById(R.id.tvPricePorterTitle);
                tvPriceCarTitle[i] = layout.findViewById(R.id.tvPriceCarTitle);
                tvPriceCar[i] = layout.findViewById(R.id.tvPriceCar);
                tvPricePorter[i] = layout.findViewById(R.id.tvPricePorter);
                tvPriceTotal[i] = layout.findViewById(R.id.tvPriceTotal);
                arrChip[i] = layout.findViewById(R.id.chStatus);
                TextView tvNameAddress1 = layout.findViewById(R.id.tvNameAddress1);
                TextView tvInfoAddress1 = layout.findViewById(R.id.tvInfoAddress1);
                TextView tvNameAddress2 = layout.findViewById(R.id.tvNameAddress2);
                TextView tvInfoAddress2 = layout.findViewById(R.id.tvInfoAddress2);
                TextView tvTime1 = layout.findViewById(R.id.tvTime1);
                TextView tvTypeCar1 = layout.findViewById(R.id.tvTypeCar1);
                TextView tvCountPorter = layout.findViewById(R.id.tvCountPorter);
                tvPeriod = layout.findViewById(R.id.tvPeriod);
                TextView tvDopPriceCar = layout.findViewById(R.id.tvDopPriceCar);
                TextView tvDopPricePorter = layout.findViewById(R.id.tvDopPricePorter);
                TextView tvToCenter = layout.findViewById(R.id.tvToCenter);
                TextView tvToSuburb = layout.findViewById(R.id.tvToSuburb);
                TextView tvComment = layout.findViewById(R.id.tvComment);
                TextView tvDistance = layout.findViewById(R.id.tvDistance);
                tvNameAddress1.setText(orderCard.getAddressStart());
                tvInfoAddress1.setText(orderCard.getCityStart());
                tvNameAddress2.setText(orderCard.getAddressFinish());
                tvInfoAddress2.setText(orderCard.getCityFinish());
                tvTime1.setText(String.valueOf(orderCard.getTimeToRadar()));
                tvTypeCar1.setText(orderCard.getCardNameTypeCar());
                tvCountPorter.setText(String.valueOf(orderCard.getCountPorter()));
                tvPeriod.setText(orderCard.getPeriodToRadar());
                tvDopPriceCar.setText(Model.getInstance().getStrCurrency(orderCard.getPriceDopCar() / 60f));
                tvDopPricePorter.setText(Model.getInstance().getStrCurrency(orderCard.getPriceDopPorter() / 60f));
                tvToCenter.setText(orderCard.getToCenter());
                tvToSuburb.setText(orderCard.getToSuburb());
                tvComment.setText(orderCard.getComment());
                tvDistance.setText(orderCard.getDistance());
                tvPriceCar[i].setText(Model.getInstance().getStrCurrency(orderCard.getPriceCar(0)));
                tvPricePorter[i].setText(Model.getInstance().getStrCurrency(orderCard.getPricePorter(0)));
                tvPriceTotal[i].setText(Model.getInstance().getStrCurrency(orderCard.getPriceCar(0) + orderCard.getPricePorter(0)));
                tvCountdown[i].setText("");
                arrBtnShowCar[i] = layout.findViewById(R.id.btnShowCar);
                arrBtnShowCar[i].setOnClickListener(v -> setMap(orderCard));
                arrBtnCancel[i] = layout.findViewById(R.id.btnCancel);
                int finalCount = count;
                arrBtnCancel[i].setOnClickListener(v -> {
                    llList.removeViewAt(finalCount);
                    deleteOrder(orderCard.getId());
                });
                if (Model.getInstance().getOrderCard().get(i).getStatus() == 4) {
                    arrChip[i].setText("Ожидание машины");
                    arrChip[i].setBackgroundColor(Color.parseColor("#E2E6E9"));
                }
                if (Model.getInstance().getOrderCard().get(i).getStatus() == 5) {
                    arrChip[i].setText("Машина в работе");
                    arrChip[i].setBackgroundColor(Color.parseColor("#1EC55C"));
                    arrBtnCancel[i].setVisibility(View.GONE);
                }
                if (Model.getInstance().getOrderCard().get(i).getStatus() == 6) {
                    arrChip[i].setText("Заказ завершен");
                    arrChip[i].setBackgroundColor(Color.parseColor("#1EC55C"));
                    arrBtnCancel[i].setVisibility(View.GONE);
                }
                if (Model.getInstance().getOrderCard().get(i).getStatus() == 6) {
                    tvPriceCar[i].setText(Model.getInstance().getStrCurrency(orderCard.getPriceCar(0)));
                    tvPricePorter[i].setText(Model.getInstance().getStrCurrency(orderCard.getPricePorter(0)));
                    tvPriceTotal[i].setText(Model.getInstance().getStrCurrency(orderCard.getPriceCar(0) + orderCard.getPricePorter(0)));
                    tvCountdown[i].setVisibility(View.INVISIBLE);
                    arrBtnShowCar[i].setText("Оценить перевозку");
                    arrBtnShowCar[i].setOnClickListener(v -> {
                        Model.getInstance().setCurrentOrder(orderCard);
                        ((CustomerActivity) getActivity()).toRatingFragment();
                    });
                }
                llList.addView(layout);
                count++;
            }
        }
        return llList.getChildCount() - count1;
    }

    private void setMap(OrderCard orderCard) {
        svMain.setVisibility(View.INVISIBLE);
        try {
            Marker mapMarker = googleMap.addMarker(new MarkerOptions().position(orderCard.getLatLngDriver())
                .title(orderCard.getNameDriver()));
            loadMarkerIcon(mapMarker, orderCard.getIdDriver());
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(orderCard.getLatLngDriver(), 11.0f));
        } catch (Exception ignored) {
        }
    }

    private void loadMarkerIcon(final Marker marker, int id) {
        int w = convertDpToPixels(50, getContext());
        Glide.with(this).asBitmap().load(Model.getInstance().getUrlHost() + "photo/car_photo/" + id + ".png")
            .apply(new RequestOptions().override(w, w))
            .apply(RequestOptions.circleCropTransform())
            .into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(resource);
                    marker.setIcon(icon);
                }
            });
    }

    private void deleteOrder(int idOrder) {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/delete_order.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idOrder", String.valueOf(idOrder));
                params.put("type", "1");
                return params;
            }
        };
        queue.add(postRequest);
    }
}