package com.cargotaxi;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.cargotaxi.Model.Model;
import com.squareup.picasso.Picasso;

public class CustomerRatingFragment extends Fragment {
    RatingBar ratingBar;

    @Override
    public View onCreateView(
        @NonNull LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_customer_rating, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Оцените перевозку");
        TextView tvAddress = root.findViewById(R.id.tvAddress);
        tvAddress.setText(Model.getInstance().getCurrentOrder().getAddressFinish());
        TextView tvName = root.findViewById(R.id.tvName);
        tvName.setText(Model.getInstance().getCurrentOrder().getNameDriver());
        TextView tvModel = root.findViewById(R.id.tvModel);
        tvModel.setText(Model.getInstance().getCurrentOrder().getModelCar() + ", " + Model.getInstance().getCurrentOrder().getNumberCar());
        ratingBar = root.findViewById(R.id.ratingBar);
        ImageView ivPhoto = root.findViewById(R.id.ivPhoto);
        Picasso.get().load(Model.getInstance().getUrlHost() + "photo/car_photo/" + Model.getInstance().getCurrentOrder().getIdDriver() + ".png").transform(new CircleTransform()).into(ivPhoto);
        return root;
    }
}