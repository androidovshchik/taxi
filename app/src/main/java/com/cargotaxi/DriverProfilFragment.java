package com.cargotaxi;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.cargotaxi.Model.Model;

public class DriverProfilFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_driver_profil, null);
        TextView tvInfo = view.findViewById(R.id.tvInfo);
        tvInfo.setText(Model.getInstance().getParamCardByType("driver_profile").getStringValue());
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Личный кабинет");
        ((DriverActivity) getActivity()).btnSwitchActiveDriver.setVisibility(View.GONE);
        return view;
    }
}