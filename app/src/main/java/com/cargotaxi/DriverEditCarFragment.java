package com.cargotaxi;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.cargotaxi.Model.Model;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

public class DriverEditCarFragment extends Fragment {
    ImageButton btnAddPhotoCar, btnAddRegTS1, btnAddRegTS2, btnAddLicense;
    private Switch swToCenter;
    private View view;
    private TextInputEditText etModelCar;
    private TextInputEditText etNumberCar;
    private TextInputEditText etRegionCar;
    private TextInputEditText etTypeCar;
    private int curSelectTypeCar = 0;
    private onEventListener eventListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_driver_reg_car, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Об автомобиле");
        etModelCar = view.findViewById(R.id.etModelCar);
        etModelCar.setText(Model.getInstance().getModelCar());
        etNumberCar = view.findViewById(R.id.etNumberCar);
        etNumberCar.setText(Model.getInstance().getNumberCar());
        etTypeCar = view.findViewById(R.id.etTypeCar);
        curSelectTypeCar = Model.getInstance().getIdTypeCar();
        etTypeCar.setText(Model.getInstance().getCarCardById(curSelectTypeCar).getType());
        etTypeCar.setKeyListener(null);
        etTypeCar.setOnFocusChangeListener((v, hasFocus) -> {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().getApplicationContext().INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etTypeCar.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
            if (hasFocus) {
                showMenu(etTypeCar);
            }
        });
        etTypeCar.setOnClickListener(v -> showMenu(etTypeCar));
        etRegionCar = view.findViewById(R.id.etRegionCar);
        etRegionCar.setText(String.valueOf(Model.getInstance().getRegionCar()));
        swToCenter = view.findViewById(R.id.swToCenter);
        btnAddPhotoCar = view.findViewById(R.id.btnAddPhotoCar);
        btnAddRegTS1 = view.findViewById(R.id.btnAddRegTS1);
        btnAddRegTS2 = view.findViewById(R.id.btnAddRegTS2);
        btnAddLicense = view.findViewById(R.id.btnAddLicense);
        Glide.with(this)
            .load(Model.getInstance().getUrlHost() + "photo/car_photo/" + Model.getInstance().getIdUser() + ".png")
            .into(btnAddPhotoCar);
        Glide.with(this)
            .load(Model.getInstance().getUrlHost() + "photo/regts1_photo/" + Model.getInstance().getIdUser() + ".png")
            .into(btnAddRegTS1);
        Glide.with(this)
            .load(Model.getInstance().getUrlHost() + "photo/regts2_photo/" + Model.getInstance().getIdUser() + ".png")
            .into(btnAddRegTS2);
        Glide.with(this)
            .load(Model.getInstance().getUrlHost() + "photo/license_photo/" + Model.getInstance().getIdUser() + ".png")
            .into(btnAddLicense);
        MaterialButton btnNext = view.findViewById(R.id.btnNext);
        btnNext.setText("Сохранить");
        btnNext.setOnClickListener(v -> sendInfoReg());
        return view;
    }

    private void showMenu(View anchor) {
        PopupMenu popup = new PopupMenu(getActivity().getApplicationContext(), anchor);
        popup.getMenuInflater().inflate(R.menu.activity_customer_drawer, popup.getMenu());
        popup.getMenu().clear();
        for (int i = 0; i < Model.getInstance().getCarCard().size(); i++) {
            MenuItem item = popup.getMenu().add(Menu.NONE, i, Menu.NONE, Model.getInstance().getCarCard().get(i).getType());
            item.setOnMenuItemClickListener(item1 -> {
                curSelectTypeCar = item1.getItemId();
                etTypeCar.setText(Model.getInstance().getCarCard().get(item1.getItemId()).getType());
                return true;
            });
        }
        popup.show();
    }

    private void sendInfoReg() {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/reg_driver_car.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
                try {
                    JSONObject jObject = new JSONObject(response);
                    if (jObject.getBoolean("status")) {
                        Model.getInstance().setModelCar(etModelCar.getText().toString());
                        Model.getInstance().setNumberCar(etNumberCar.getText().toString());
                        if (etRegionCar.getText().toString().length() > 0)
                            Model.getInstance().setRegionCar(Integer.parseInt(etRegionCar.getText().toString()));
                        Model.getInstance().setIdTypeCar(Model.getInstance().getCarCard().get(curSelectTypeCar).getId());
                        Model.getInstance().setToCenterDriver(swToCenter.isChecked());
                        DAO.getInstance().saveProfil();
                        eventListener.toProfilEvent();
                    } else
                        Snackbar.make(view, "Данные не отправлены", Snackbar.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Timber.e(e);
                    Snackbar.make(view, "Данные не отправлены", Snackbar.LENGTH_SHORT).show();
                }
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(Model.getInstance().getIdUser()));
                params.put("model", etModelCar.getText().toString());
                params.put("number", etNumberCar.getText().toString());
                params.put("region", etRegionCar.getText().toString());
                params.put("idtype", String.valueOf(Model.getInstance().getCarCard().get(curSelectTypeCar).getId()));
                params.put("toCenter", String.valueOf(swToCenter.isChecked()));
                params.put("newDriver", String.valueOf(0));
                return params;
            }
        };
        queue.add(postRequest);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            eventListener = (onEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    public interface onEventListener {
        void toProfilEvent();
    }
}