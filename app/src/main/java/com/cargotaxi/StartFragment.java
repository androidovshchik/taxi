package com.cargotaxi;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class StartFragment extends Fragment {
    private static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";
    private int pageNumber;

    static StartFragment newInstance(int page) {
        StartFragment pageFragment = new StartFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_PAGE_NUMBER, page);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageNumber = getArguments().getInt(ARGUMENT_PAGE_NUMBER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start, null);
        ImageView ivIcon = view.findViewById(R.id.ivIcon);
        TextView tvInfo = view.findViewById(R.id.tvInfo);
        if (pageNumber == 0) {
            ivIcon.setImageResource(R.drawable.prez1);
            tvInfo.setText("Быстрый и простой заказ машины.\r\n\r\nВсегда честная цена за доставку без наценок и скрытых платежей.\r\n\r\nОбщайтесь напрямую с водителем, а не с операторам.");
        }
        if (pageNumber == 1) {
            ivIcon.setImageResource(R.drawable.prez2);
            tvInfo.setText("Водители грузовика забирают заказы напрямую.\r\n\r\nВодители платят минимальную комиссию за заказ.\r\n\r\nНет посредников между водителем и клиентом.");
        }
        return view;
    }
}