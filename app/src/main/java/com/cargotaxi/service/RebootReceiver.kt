package com.cargotaxi.service

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.cargotaxi.Model.Model

class RebootReceiver : BroadcastReceiver() {

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    override fun onReceive(context: Context, intent: Intent) {
        if (Model.getInstance().typeUser in 1..2) {
            ForegroundService.start(context)
        }
    }
}