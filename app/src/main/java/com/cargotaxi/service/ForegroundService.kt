@file:Suppress("DEPRECATION")

package com.cargotaxi.service

import android.annotation.SuppressLint
import android.app.Notification
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.media.RingtoneManager
import android.os.IBinder
import android.os.PowerManager
import androidx.core.app.NotificationCompat
import com.cargotaxi.*
import com.cargotaxi.Model.Model
import com.cargotaxi.R
import com.cargotaxi.extension.*
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import org.jetbrains.anko.*
import org.json.JSONObject
import timber.log.Timber
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

@Suppress("SpellCheckingInspection")
class ForegroundService : Service() {

    private var timer: ScheduledFuture<*>? = null

    private lateinit var wakeLock: PowerManager.WakeLock

    private lateinit var locationClient: FusedLocationProviderClient

    private val locationCallback = object : LocationCallback() {

        override fun onLocationAvailability(availability: LocationAvailability) {
            Timber.i("onLocationAvailability $availability")
        }

        override fun onLocationResult(result: LocationResult?) {
            onLocation(result?.lastLocation ?: return)
        }
    }

    override fun onBind(intent: Intent): IBinder? = null

    @SuppressLint("WakelockTimeout")
    override fun onCreate() {
        super.onCreate()
        startForeground(
            Int.MAX_VALUE, NotificationCompat.Builder(applicationContext, "foreground")
            .setSmallIcon(R.drawable.main_logo)
            .setContentTitle("Фоновой сервис")
            .setContentIntent(pendingActivityFor(intentFor<SplashActivity>().clearTop().newTask()))
            .setOngoing(true)
            .build()
        )
        locationClient = LocationServices.getFusedLocationProviderClient(applicationContext)
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, javaClass.name).also {
            it.acquire()
        }
        val seconds = AtomicInteger(-10)
        val executor = Executors.newSingleThreadScheduledExecutor()
        timer = executor.scheduleAtFixedRate({
            val counter = seconds.addAndGet(10)
            when (Model.getInstance().typeUser) {
                1 -> {
                    if (counter % 60 == 0) {
                        sendNotification()
                    }
                    sendCustomer()
                    sendRadar()
                }
                2 -> {
                    if (counter % 60 == 0) {
                        sendNotification()
                    }
                    sendDriver()
                }
                else -> {
                    stopForeground(true)
                    stopSelf()
                }
            }
        }, 0, 10, TimeUnit.SECONDS)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        intent?.let {
            if (it.hasExtra("location")) {
                if (it.getBooleanExtra("location", false)) {
                    try {
                        val interval = it.getLongExtra("interval", 1000L)
                        locationClient.requestLocationUpdates(
                            LocationRequest.create()
                                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                                .setFastestInterval(interval)
                                .setInterval(interval),
                            locationCallback, null
                        )
                    } catch (e: Throwable) {
                        Timber.e(e)
                    }
                } else {
                    locationClient.removeLocationUpdates(locationCallback)
                }
            }
        }
        return START_STICKY
    }

    private fun onLocation(location: Location) {
        Model.getInstance().apply {
            try {
                val addresses = geoCoder.getFromLocation(location.latitude, location.longitude, 1)
                if (addresses.isNotEmpty()) {
                    Timber.d(addresses[0].locality)
                    currentCity = addresses[0].locality
                }
            } catch (e: Throwable) {
                Timber.e(e)
            }
            Timber.d("${location.longitude};${location.latitude} My Current City is: $currentCity")
            currentLatLng = LatLng(location.latitude, location.longitude)
            if (currentOrder.latLngStart == null) {
                currentOrder.latLngStart = LatLng(location.latitude, location.longitude)
            }
        }
        NetworkController.getInstance().saveCoordinate()
    }

    private fun sendNotification() {
        val json = syncGetJson("${Model.getInstance().urlHost}script/get_notification.php?idUser=${DAO.getInstance().loadIdUser()}")
        try {
            if (json != null) {
                showNotification(json)
            }
        } catch (e: Throwable) {
            Timber.e(e)
        }
    }

    private fun sendRadar() {
        val string = syncPost("${Model.getInstance().urlHost}script/get_info_radar.php", mapOf(
            "idCustomer" to Model.getInstance().idUser.toString(),
            "idOrder" to Model.getInstance().currentOrder.id.toString()
        ))
        try {
            if (string != null) {
                val jsonObject = JSONObject(string)
                RadarActivity.onGetRadarInfo(jsonObject)
                showNotification(jsonObject.getJSONObject("notif"))
                sendBroadcast(Intent("radar_info").putExtra("response", string))
            }
        } catch (e: Throwable) {
            Timber.e(e)
        }
    }

    private fun sendDriver() {
        val string = syncPost("${Model.getInstance().urlHost}script/get_info_driver.php", mapOf(
            "idDriver" to Model.getInstance().idUser.toString(),
            "typeCar" to Model.getInstance().idTypeCar.toString()
        ))
        try {
            if (string != null) {
                val jsonObject = JSONObject(string)
                DriverActivity.onGetDriverInfo(jsonObject)
                showNotification(jsonObject.getJSONObject("notif"))
                sendBroadcast(Intent("driver_info").putExtra("response", string))
            }
        } catch (e: Throwable) {
            Timber.e(e)
        }
    }

    private fun sendCustomer() {
        val string = syncPost("${Model.getInstance().urlHost}script/get_info_customer.php", mapOf(
            "idCustomer" to Model.getInstance().idUser.toString()
        ))
        try {
            if (string != null) {
                val jsonObject = JSONObject(string)
                CustomerActivity.onGetCustomerInfo(jsonObject)
                showNotification(jsonObject.getJSONObject("notif"))
            }
        } catch (e: Throwable) {
            Timber.e(e)
        }
    }

    private fun showNotification(jsonObject: JSONObject) {
        if (jsonObject.getBoolean("status")) {
            val builder = NotificationCompat.Builder(applicationContext, "default")
                .setSmallIcon(R.drawable.ic_notifications_white_24dp)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(jsonObject.getString("text"))
                .setContentIntent(pendingActivityFor(intentFor<SplashActivity>().clearTop().newTask()))
                .setPriority(Notification.PRIORITY_MAX)
                .setVibrate(longArrayOf(1000))
                .setLights(Color.RED, 3000, 3000)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            notificationManager.notify(1, builder.build())
        }
    }

    override fun onDestroy() {
        timer?.cancel(true)
        locationClient.removeLocationUpdates(locationCallback)
        wakeLock.release()
        super.onDestroy()
    }

    companion object {

        @JvmStatic
        fun start(context: Context, vararg params: Pair<String, Any?>): Boolean = context.run {
            return try {
                startForegroundService<ForegroundService>(*params) != null
            } catch (e: Throwable) {
                Timber.e(e)
                false
            }
        }

        @JvmStatic
        fun stop(context: Context): Boolean = context.run {
            if (activityManager.isRunning<ForegroundService>()) {
                return stopService<ForegroundService>()
            }
            return true
        }
    }
}