package com.cargotaxi;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cargotaxi.Model.Model;

public class CustomerMenuPorterFragment extends Fragment {
    CustomerMap2Fragment.onEventListener eventListener;
    private RecyclerView rv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_menu_porter, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Количество грузчиков");
        TextView tvInfo = view.findViewById(R.id.tvInfo);
        tvInfo.setText(Model.getInstance().getParamCardByType("customer_porter").getStringValue());
        rv = view.findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        RecyclerView.LayoutManager llm = new GridLayoutManager(getActivity().getApplicationContext(), 1);
        rv.setLayoutManager(llm);
        reload();
        return view;
    }

    private void reload() {
        CustomerMenuPorterAdapter adapter = new CustomerMenuPorterAdapter(this, getActivity().getApplicationContext(), 3);
        rv.setAdapter(adapter);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            eventListener = (CustomerMap2Fragment.onEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    public interface onEventListener {
        void toMenuEvent();
    }
}