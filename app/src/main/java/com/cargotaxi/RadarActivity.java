package com.cargotaxi;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cargotaxi.Model.CarCard;
import com.cargotaxi.Model.DriverCard;
import com.cargotaxi.Model.Model;
import com.cargotaxi.Model.OrderCard;
import com.cargotaxi.Model.ParamCard;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

public class RadarActivity extends BaseActivity {
    private final RadarSearchFragment radarSearchFragment = new RadarSearchFragment();
    private final RadarSelectFragment radarSelectFragment = new RadarSelectFragment();
    private final RadarOrderFragment radarOrderFragment = new RadarOrderFragment();
    private final ArrayList<DriverCard> driverCardList = new ArrayList<>();
    int typeConnect = 1;
    private FragmentTransaction fTrans;

    public static void onGetRadarInfo(JSONObject jMainObject) {
        try {
            JSONArray jArrayCar = jMainObject.getJSONArray("typeCar");
            Model.getInstance().getCarCard().clear();
            for (int i = 0; i < jArrayCar.length(); i++) {
                JSONObject jObjectCar = jArrayCar.getJSONObject(i);
                Model.getInstance().getCarCard().add(new CarCard(Integer.parseInt(jObjectCar.getString("id")), jObjectCar.getString("name"), jObjectCar.getString("info"), Integer.parseInt(jObjectCar.getString("price")), Integer.parseInt(jObjectCar.getString("koef"))));
            }
            Model.getInstance().getParamCard().clear();
            JSONArray jArrayText = jMainObject.getJSONArray("text");
            for (int i = 0; i < jArrayText.length(); i++) {
                JSONObject jObjectParam = jArrayText.getJSONObject(i);
                Model.getInstance().getParamCard().add(new ParamCard(jObjectParam.getString("type"), jObjectParam.getString("value")));
            }
            JSONArray jArrayParam = jMainObject.getJSONArray("param");
            for (int i = 0; i < jArrayParam.length(); i++) {
                JSONObject jObjectParam = jArrayParam.getJSONObject(i);
                Model.getInstance().getParamCard().add(new ParamCard(jObjectParam.getString("type"), jObjectParam.getString("value")));
            }
            JSONArray jArray = jMainObject.getJSONArray("order");
            Model.getInstance().getOrderCard().clear();
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject jObject = jArray.getJSONObject(i);
                OrderCard orderCard = new OrderCard(jObject.getInt("idorder"), jObject.getString("latStart"), jObject.getString("lngStart"),
                    jObject.getString("latFinish"), jObject.getString("lngFinish"), jObject.getString("addressStart"), jObject.getString("cityStart"), jObject.getString("addressFinish"), jObject.getString("cityFinish"),
                    jObject.getInt("period"), jObject.getInt("countPorter"), jObject.getString("toCenter"),
                    jObject.getString("toSuburb"), jObject.getInt("priceCar"), jObject.getInt("pricePorter"), jObject.getInt("priceDopCar"), jObject.getInt("priceDopPorter"), jObject.getString("comment"),
                    jObject.getString("nameCustomer"), jObject.getString("phoneCustomer"), jObject.getInt("typeCar"), jObject.getInt("status"), jObject.getInt("idCustomer"), jObject.getInt("idDriver"), 0,
                    jObject.getString("nameDriver"), jObject.getString("modelCar"), jObject.getString("numberCar"), jObject.getString("date"),
                    jObject.getInt("regionCar"), jObject.getString("phoneDriver"), jObject.getString("latDriver"), jObject.getString("lngDriver"),
                    jObject.getString("time"), jObject.getInt("rating"));
                Model.getInstance().setLastIdOrder(jObject.getInt("idorder"));
                Model.getInstance().getOrderCard().add(0, orderCard);
                if (((jObject.getInt("status") == 4) || (jObject.getInt("status") == 5)) && (orderCard.getId() == Model.getInstance().getCurrentOrder().getId())) {
                    Model.getInstance().getCurrentOrder().setStatus(4);
                }
            }
        } catch (JSONException e) {
            Timber.e(e);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radar);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(Color.parseColor("#1E1E1E"));
        setSupportActionBar(toolbar);
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.add(R.id.frgmCont, radarSearchFragment);
        fTrans.commitAllowingStateLoss();
        typeConnect = 1;
    }

    void setDriver(int id) {
        Model.getInstance().setCurrentDriverCard(driverCardList.get(id));
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.add(R.id.frgmCont, radarOrderFragment);
        fTrans.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
    }

    private void getOrderDriver() {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/get_order_driver.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
                try {
                    JSONArray jArray = new JSONArray(response);
                    driverCardList.clear();
                    for (int i = 0; i < jArray.length(); i++) {
                        DriverCard driverCard = new DriverCard(jArray.getJSONObject(i).getInt("idUser"), jArray.getJSONObject(i).getString("name"), jArray.getJSONObject(i).getString("modelCar"), jArray.getJSONObject(i).getString("idTypeCar"), jArray.getJSONObject(i).getString("numberCar"), jArray.getJSONObject(i).getString("regionCar"), jArray.getJSONObject(i).getString("phone"), jArray.getJSONObject(i).getDouble("lat"), jArray.getJSONObject(i).getDouble("lng"), jArray.getJSONObject(i).getInt("ratingCount"), jArray.getJSONObject(i).getInt("ratingValue"), jArray.getJSONObject(i).getInt("countOrder"));
                        driverCardList.add(driverCard);
                    }
                    if (driverCardList.size() > 0) {
                        if (typeConnect == 1) {
                            try {
                                radarSearchFragment.clearMap();
                            } catch (Exception rt) {
                                rt.printStackTrace();
                            }
                            fTrans = getSupportFragmentManager().beginTransaction();
                            fTrans.replace(R.id.frgmCont, radarSelectFragment);
                            fTrans.commitAllowingStateLoss();
                            radarSelectFragment.driverCardList = driverCardList;
                            typeConnect = 2;
                        }
                        if (typeConnect == 2) {
                            radarSelectFragment.driverCardList = driverCardList;
                            radarSelectFragment.reloadDriverList();
                        }
                    }
                    if (driverCardList.size() == 0) {
                        if (typeConnect == 2) {
                            fTrans = getSupportFragmentManager().beginTransaction();
                            fTrans.replace(R.id.frgmCont, radarSearchFragment);
                            fTrans.commitAllowingStateLoss();
                            typeConnect = 1;
                        }
                    }
                } catch (Exception e) {
                    Timber.e(e);
                }
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idOrder", String.valueOf(Model.getInstance().getCurrentOrder().getId()));
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void rejectDriverRequest(int idUserDriver) {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/reject_driver_request.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
                try {
                    JSONObject jObject = new JSONObject(response);
                    if (jObject.getBoolean("status")) {
                        getOrderDriver();
                    }
                } catch (Exception e) {
                    Timber.e(e);
                }
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idOrder", String.valueOf(Model.getInstance().getCurrentOrder().getId()));
                params.put("idUserDriver", String.valueOf(idUserDriver));
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void changeStatusOrder(int idUserDriver, int status) {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/change_status_order.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
                try {
                    JSONObject jObject = new JSONObject(response);
                    if (jObject.getBoolean("status")) {
                        getOrderDriver();
                    }
                } catch (Exception e) {
                    Timber.e(e);
                }
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idOrder", String.valueOf(Model.getInstance().getCurrentOrder().getId()));
                params.put("idUserDriver", String.valueOf(idUserDriver));
                params.put("status", String.valueOf(status));
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void deleteOrder(int idOrder) {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/delete_order.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
                try {
                    JSONObject jObject = new JSONObject(response);
                    if (jObject.getBoolean("status")) {
                        Model.getInstance().getCurrentOrder().setStatus(1);
                        DAO.getInstance().deleteCurOrder();
                        Intent intent = new Intent(RadarActivity.this, CustomerActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } catch (Exception e) {
                    Timber.e(e);
                }
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idOrder", String.valueOf(Model.getInstance().getCurrentOrder().getId()));
                params.put("type", "1");
                return params;
            }
        };
        queue.add(postRequest);
    }

    @Override
    public void getRadarInfo(@NotNull String response) {
        try {
            JSONObject jMainObject = new JSONObject(response);
            JSONArray jArray = jMainObject.getJSONArray("order");
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject jObject = jArray.getJSONObject(i);
                OrderCard orderCard = new OrderCard(jObject.getInt("idorder"), jObject.getString("latStart"), jObject.getString("lngStart"),
                    jObject.getString("latFinish"), jObject.getString("lngFinish"), jObject.getString("addressStart"), jObject.getString("cityStart"), jObject.getString("addressFinish"), jObject.getString("cityFinish"),
                    jObject.getInt("period"), jObject.getInt("countPorter"), jObject.getString("toCenter"),
                    jObject.getString("toSuburb"), jObject.getInt("priceCar"), jObject.getInt("pricePorter"), jObject.getInt("priceDopCar"), jObject.getInt("priceDopPorter"), jObject.getString("comment"),
                    jObject.getString("nameCustomer"), jObject.getString("phoneCustomer"), jObject.getInt("typeCar"), jObject.getInt("status"), jObject.getInt("idCustomer"), jObject.getInt("idDriver"), 0,
                    jObject.getString("nameDriver"), jObject.getString("modelCar"), jObject.getString("numberCar"), jObject.getString("date"),
                    jObject.getInt("regionCar"), jObject.getString("phoneDriver"), jObject.getString("latDriver"), jObject.getString("lngDriver"),
                    jObject.getString("time"), jObject.getInt("rating"));
                if ((jObject.getInt("status") == 2) && (orderCard.getId() == Model.getInstance().getCurrentOrder().getId())) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(RadarActivity.this);
                    builder.setTitle("Водитель отменил заказ")
                        .setCancelable(false)
                        .setNegativeButton("Ок",
                            (dialog, id) -> {
                                Model.getInstance().getCurrentOrder().setStatus(1);
                                DAO.getInstance().deleteCurOrder();
                                Intent intent = new Intent(RadarActivity.this, CustomerActivity.class);
                                startActivity(intent);
                                finish();
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
                if (((jObject.getInt("status") == 4) || (jObject.getInt("status") == 5)) && (orderCard.getId() == Model.getInstance().getCurrentOrder().getId())) {
                    Intent intent = new Intent(getApplicationContext(), CustomerActivity.class);
                    Bundle b = new Bundle();
                    b.putString("type", "history");
                    intent.putExtras(b);
                    startActivity(intent);
                    finish();
                }
            }
            jArray = jMainObject.getJSONArray("driver");
            driverCardList.clear();
            for (int i = 0; i < jArray.length(); i++) {
                DriverCard driverCard = new DriverCard(jArray.getJSONObject(i).getInt("idUser"), jArray.getJSONObject(i).getString("name"), jArray.getJSONObject(i).getString("modelCar"), jArray.getJSONObject(i).getString("idTypeCar"), jArray.getJSONObject(i).getString("numberCar"), jArray.getJSONObject(i).getString("regionCar"), jArray.getJSONObject(i).getString("phone"), jArray.getJSONObject(i).getDouble("lat"), jArray.getJSONObject(i).getDouble("lng"), jArray.getJSONObject(i).getInt("ratingCount"), jArray.getJSONObject(i).getInt("ratingValue"), jArray.getJSONObject(i).getInt("countOrder"));
                driverCardList.add(driverCard);
            }
            if (driverCardList.size() > 0) {
                if (typeConnect == 1) {
                    try {
                        radarSearchFragment.clearMap();
                    } catch (Exception rt) {
                        rt.printStackTrace();
                    }
                    try {
                        fTrans = getSupportFragmentManager().beginTransaction();
                        fTrans.replace(R.id.frgmCont, radarSelectFragment);
                        fTrans.commitAllowingStateLoss();
                    } catch (Exception rt) {
                        rt.printStackTrace();
                    }
                    radarSelectFragment.driverCardList = driverCardList;
                    typeConnect = 2;
                } else if (typeConnect == 2) {
                    radarSelectFragment.driverCardList = driverCardList;
                    radarSelectFragment.reloadDriverList();
                }
            }
            if (driverCardList.size() == 0) {
                if (typeConnect == 2) {
                    fTrans = getSupportFragmentManager().beginTransaction();
                    fTrans.replace(R.id.frgmCont, radarSearchFragment);
                    fTrans.commitAllowingStateLoss();
                    typeConnect = 1;
                }
            }
        } catch (JSONException e) {
            Timber.e(e);
        }
    }
}
