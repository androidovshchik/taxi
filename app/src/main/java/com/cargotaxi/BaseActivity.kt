package com.cargotaxi

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

    private val broadcastReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            when (intent.action) {
                "driver_info" -> getDriverInfo(intent.getStringExtra("response") ?: return)
                "radar_info" -> getRadarInfo(intent.getStringExtra("response") ?: return)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter().apply {
            addAction("driver_info")
            addAction("radar_info")
        })
    }

    open fun getDriverInfo(response: String) {}

    open fun getRadarInfo(response: String) {}

    override fun onStop() {
        unregisterReceiver(broadcastReceiver)
        super.onStop()
    }
}