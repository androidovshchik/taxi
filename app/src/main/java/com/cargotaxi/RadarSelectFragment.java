package com.cargotaxi;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.cargotaxi.Model.DriverCard;
import com.cargotaxi.Model.Model;
import com.google.android.material.button.MaterialButton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RadarSelectFragment extends Fragment {
    ArrayList<DriverCard> driverCardList = new ArrayList<>();
    private View root;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Model.getInstance().setCurrentSetAddress(1);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Предложения водителей");
        root = inflater.inflate(R.layout.fragment_radar_select, null);
        reloadDriverList();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        reloadDriverList();
    }

    void reloadDriverList() {
        LinearLayout llDriverList = root.findViewById(R.id.llDriverList1);
        llDriverList.removeAllViews();
        for (int i = 0; i < driverCardList.size(); i++) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.card_driver, null, false);
            TextView tvName = layout.findViewById(R.id.tvName);
            TextView tvModel = layout.findViewById(R.id.tvModel);
            TextView tvType = layout.findViewById(R.id.tvType);
            TextView tvCountOrder = layout.findViewById(R.id.tvCountOrder);
            RatingBar ratingBar = layout.findViewById(R.id.ratingBar);
            tvName.setText(driverCardList.get(i).getName());
            tvModel.setText(driverCardList.get(i).getModel());
            tvType.setText(Model.getInstance().getCarCardById(driverCardList.get(i).getIdTypeCar()).getType());
            tvCountOrder.setText("Заказов - " + driverCardList.get(i).getCountOrder());
            if (driverCardList.get(i).getRatingCount() != 0) {
                ratingBar.setRating((driverCardList.get(i).getRatingValue() / driverCardList.get(i).getRatingCount()));
            } else {
                ratingBar.setRating(0);
            }
            ImageView ivPhoto = layout.findViewById(R.id.ivPhoto);
            Picasso.get().load(Model.getInstance().getUrlHost() + "photo/car_photo/" + driverCardList.get(i).getId() + ".png").transform(new CircleTransform()).into(ivPhoto);
            MaterialButton btnCancel = layout.findViewById(R.id.btnCancel);
            final int finalI = i;
            int finalI1 = i;
            btnCancel.setOnClickListener(v -> ((RadarActivity) getActivity()).rejectDriverRequest(driverCardList.get(finalI1).getId()));
            MaterialButton btnAccess = layout.findViewById(R.id.btnAccess);
            btnAccess.setOnClickListener(v -> {
                ((RadarActivity) getActivity()).changeStatusOrder(driverCardList.get(finalI1).getId(), 3);
                ((RadarActivity) getActivity()).typeConnect = 3;
                ((RadarActivity) getActivity()).setDriver(finalI);
            });
            llDriverList.addView(layout);
        }
    }
}