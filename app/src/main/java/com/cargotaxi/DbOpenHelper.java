package com.cargotaxi;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class DbOpenHelper extends SQLiteOpenHelper {
    public static final String ProfilTable = "ProfilTable";
    public static final String CurOrderTable = "CurOrderTable";
    private static final String DB_NAME = "taxi12";
    private static final int DB_VERSION = 1;
    private static final String CREATE_ProfilTable = "create table ProfilTable ( _id integer primary key autoincrement, " +
        "idUser INTEGER, name TEXT, surname TEXT, phone TEXT, password TEXT, type INTEGER, regDriver INTEGER, idTypeCar INTEGER, modelCar TEXT, numberCar TEXT, birthday TEXT, mail TEXT, gender INTEGER,regionCar TEXT, toCenter INTEGER,curOrder INTEGER,curOrderType INTEGER)";
    private static final String CREATE_CurOrderTable = "create table CurOrderTable ( _id integer primary key autoincrement,id INTEGER,typeCar INTEGER, latStart TEXT, lngStart TEXT, " +
        "addressStart TEXT,cityStart TEXT,latFinish TEXT,lngFinish TEXT,addressFinish TEXT,cityFinish TEXT,ts TEXT,period INTEGER,countPorter INTEGER,toCenter TEXT,toSuburb TEXT,priceCar INTEGER,pricePorter INTEGER,priceDopCar INTEGER," +
        "priceDopPorter INTEGER,comment TEXT)";

    public DbOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_ProfilTable);
        sqLiteDatabase.execSQL(CREATE_CurOrderTable);
    }

    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }
}