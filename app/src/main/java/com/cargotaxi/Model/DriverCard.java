package com.cargotaxi.Model;

import com.google.android.gms.maps.model.LatLng;

public class DriverCard {
    private int id;
    private String name;
    private String model;
    private int idTypeCar;
    private String number;
    private String region;
    private String phone;
    private LatLng latLng;
    private int ratingCount;
    private int ratingValue;
    private int countOrder;

    public DriverCard(int id, String name, String model, String idTypeCar, String number, String region, String phone, Double lat, Double lng, int ratingCount, int ratingValue, int countOrder) {
        this.setId(id);
        this.name = name;
        this.model = model;
        this.setIdTypeCar(Integer.parseInt(idTypeCar));
        this.number = number;
        this.region = region;
        this.setRegion(getRegion());
        this.phone = phone;
        latLng = new LatLng(lat, lng);
        this.ratingCount = ratingCount;
        this.ratingValue = ratingValue;
        this.phone = phone;
        this.countOrder = countOrder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }

    private String getRegion() {
        return region;
    }

    private void setRegion(String region) {
        this.region = region;
    }

    public int getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(int ratingCount) {
        this.ratingCount = ratingCount;
    }

    public int getRatingValue() {
        return ratingValue;
    }

    public void setRatingValue(int ratingValue) {
        this.ratingValue = ratingValue;
    }

    public int getIdTypeCar() {
        return idTypeCar;
    }

    private void setIdTypeCar(int idTypeCar) {
        this.idTypeCar = idTypeCar;
    }

    public int getCountOrder() {
        return countOrder;
    }

    public void setCountOrder(int countOrder) {
        this.countOrder = countOrder;
    }
}
