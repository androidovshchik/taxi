package com.cargotaxi.Model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.cargotaxi.R;
import com.google.android.gms.maps.model.LatLng;

import java.io.ByteArrayOutputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Model {
    private static Model instance;
    private Geocoder geoCoder;
    private DriverCard currentDriverCard;
    private String urlHost = "http://cargogruztaxi.ru/app/";
    private String urlTerms = "http://cargogruztaxi.ru/terms_of_use.html";
    private String urlRequisites = "http://cargogruztaxi.ru/requisites.html";
    private OrderCard currentOrder = new OrderCard();
    private LatLng currentLatLng;
    private String currentCity;
    private int lastIdOrder = 0;
    private String strDateRegDriver = "";
    private int idUser = 0;
    private String nameUser = "";
    private String surnameUser = "";
    private String birthdayUser = "";
    private String phoneUser = "";
    private int genderUser = 1;
    private String passwordUser = "";
    private int typeUser = 0;
    private int regDriver = 0;
    private double balanceDriver = 0;
    private String curPaymentDriver = "";
    private int idTypeCar = 0;
    private String modelCar = "";
    private String numberCar = "";
    private int regionCar = 0;
    private int currentSetAddress = 0;
    private String pointWay = "";
    private boolean workDriver = true;
    private boolean statusDriver = false;
    private boolean toCenterDriver = false;
    private Typeface typefaceMain;
    private Context context;
    private ArrayList<CustomerMenuCard> customerMenuCard = new ArrayList<>();
    private ArrayList<OrderCard> orderCard = new ArrayList<>();
    private ArrayList<OrderCard> currentOrderCard = new ArrayList<>();
    private ArrayList<ParamCard> paramCard = new ArrayList<>();
    private ArrayList<CarCard> carCard = new ArrayList<>();
    private ArrayList<Integer> cancelOrder = new ArrayList<>();

    public static synchronized Model getInstance() {
        if (instance == null) {
            instance = new Model();
        }
        return instance;
    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
            maxImageSize / realImage.getWidth(),
            maxImageSize / realImage.getHeight());
        int width = Math.round(ratio * realImage.getWidth());
        int height = Math.round(ratio * realImage.getHeight());
        return Bitmap.createScaledBitmap(realImage, width,
            height, filter);
    }

    public void init(Context context1) {
        setContext(context1);
        setGeoCoder(new Geocoder(context, Locale.getDefault()));
        setTypefaceMain(Typeface.createFromAsset(getContext().getAssets(), "font/Roboto-Black.ttf"));
        getCustomerMenuCard().clear();
        String[] arMenu = getContext().getResources().getStringArray(R.array.customer_menu);
        String[] arMenuIcon = getContext().getResources().getStringArray(R.array.customer_menu_icon);
        String[] arMenuType = getContext().getResources().getStringArray(R.array.customer_menu_type);
        for (int i = 0; i < arMenu.length; i++) {
            CustomerMenuCard customerMenuCard = new CustomerMenuCard();
            customerMenuCard.setName(arMenu[i]);
            customerMenuCard.setType(arMenuType[i]);
            customerMenuCard.setIdIcon(getContext().getResources().getIdentifier(arMenuIcon[i], "drawable", getContext().getPackageName()));
            if (i == 0) {
                String[] arr = getContext().getResources().getStringArray(R.array.customer_menu_time);
                String[] arr1 = getContext().getResources().getStringArray(R.array.customer_menu_time);
                customerMenuCard.setPunkt(arr, arr1);
            }
            if (i == 2) {
                String[] arr = getContext().getResources().getStringArray(R.array.customer_menu_period);
                String[] arr1 = getContext().getResources().getStringArray(R.array.customer_menu_period);
                customerMenuCard.setPunkt(arr, arr1);
            }
            if (i == 3) {
                String[] arr = getContext().getResources().getStringArray(R.array.customer_menu_porter);
                String[] arr1 = getContext().getResources().getStringArray(R.array.customer_menu_porter);
                customerMenuCard.setPunkt(arr, arr1);
            }
            getCustomerMenuCard().add(customerMenuCard);
        }
    }

    public ArrayList<CustomerMenuCard> getCustomerMenuCard() {
        return customerMenuCard;
    }

    public void setCustomerMenuCard(ArrayList<CustomerMenuCard> customerMenuCard) {
        this.customerMenuCard = customerMenuCard;
    }

    public Context getContext() {
        return context;
    }

    private void setContext(Context context) {
        this.context = context;
    }

    public String getPointWay() {
        return pointWay;
    }

    public void setPointWay(String pointWay) {
        this.pointWay = pointWay;
    }

    public int getCurrentSetAddress() {
        return currentSetAddress;
    }

    public void setCurrentSetAddress(int currentSetAddress) {
        this.currentSetAddress = currentSetAddress;
    }

    public ArrayList<CarCard> getCarCard() {
        return carCard;
    }

    public void setCarCard(ArrayList<CarCard> carCard) {
        this.carCard = carCard;
    }

    public CarCard getCarCardById(int id) {
        for (CarCard carCard : getCarCard())
            if (carCard.getId() == id)
                return carCard;
        return null;
    }

    public ArrayList<OrderCard> getOrderCard() {
        return orderCard;
    }

    public void setOrderCard(ArrayList<OrderCard> orderCard) {
        this.orderCard = orderCard;
    }

    public ArrayList<OrderCard> getCurrentOrderCard() {
        return currentOrderCard;
    }

    public void setCurrentOrderCard(ArrayList<OrderCard> currentOrderCard) {
        this.currentOrderCard = currentOrderCard;
    }

    public OrderCard getOrderCardById(int id) {
        for (OrderCard orderCard : getOrderCard())
            if (orderCard.getId() == id)
                return orderCard;
        return null;
    }

    public boolean isWorkDriver() {
        return workDriver;
    }

    public void setWorkDriver(boolean workDriver) {
        this.workDriver = workDriver;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getPhoneUser() {
        return phoneUser;
    }

    public void setPhoneUser(String phoneUser) {
        this.phoneUser = phoneUser;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(int typeUser) {
        this.typeUser = typeUser;
    }

    public String getArrIdTypeCar() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < Model.getInstance().getCarCard().size(); i++)
            if (Model.getInstance().getCarCard().get(i).isSelect()) {
                if (str.length() == 0)
                    str.append(Model.getInstance().getCarCard().get(i).getId());
                else
                    str.append(",").append(Model.getInstance().getCarCard().get(i).getId());
            }
        return str.toString();
    }

    public CarCard getTypeCarById(int id) {
        for (CarCard carCard : getCarCard())
            if (carCard.getId() == id)
                return carCard;
        return null;
    }

    public String getStrDateRegDriver() {
        return strDateRegDriver;
    }

    public void setStrDateRegDriver(String strDateRegDriver) {
        this.strDateRegDriver = strDateRegDriver;
    }

    public int getLastIdOrder() {
        return lastIdOrder;
    }

    public void setLastIdOrder(int lastIdOrder) {
        this.lastIdOrder = lastIdOrder;
    }

    public Typeface getTypefaceMain() {
        return typefaceMain;
    }

    private void setTypefaceMain(Typeface typefaceMain) {
        this.typefaceMain = typefaceMain;
    }

    public LatLng getCurrentLatLng() {
        return currentLatLng;
    }

    public void setCurrentLatLng(LatLng currentLatLng) {
        this.currentLatLng = currentLatLng;
    }

    public OrderCard getCurrentOrder() {
        return currentOrder;
    }

    public void setCurrentOrder(OrderCard currentOrder) {
        this.currentOrder = currentOrder;
    }

    public int isRegDriver() {
        return regDriver;
    }

    public void setRegDriver(int regDriver) {
        this.regDriver = regDriver;
    }

    public String getPasswordUser() {
        return passwordUser;
    }

    public void setPasswordUser(String passwordUser) {
        this.passwordUser = passwordUser;
    }

    public List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;
        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;
            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;
            LatLng p = new LatLng((((double) lat / 1E5)),
                (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }

    public String getSurnameUser() {
        return surnameUser;
    }

    public void setSurnameUser(String surnameUser) {
        this.surnameUser = surnameUser;
    }

    public String getBirthdayUser() {
        return birthdayUser;
    }

    public void setBirthdayUser(String birthdayUser) {
        this.birthdayUser = birthdayUser;
    }

    public int getIdTypeCar() {
        return idTypeCar;
    }

    public void setIdTypeCar(int idTypeCar) {
        this.idTypeCar = idTypeCar;
    }

    public String getModelCar() {
        return modelCar;
    }

    public void setModelCar(String modelCar) {
        this.modelCar = modelCar;
    }

    public String getNumberCar() {
        return numberCar;
    }

    public void setNumberCar(String numberCar) {
        this.numberCar = numberCar;
    }

    public int getGenderUser() {
        return genderUser;
    }

    public void setGenderUser(int genderUser) {
        this.genderUser = genderUser;
    }

    public boolean checkForInternet() {
        ConnectivityManager cm =
            (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
            activeNetwork.isConnectedOrConnecting();
    }

    public ArrayList<ParamCard> getParamCard() {
        return paramCard;
    }

    public void setParamCard(ArrayList<ParamCard> paramCard) {
        this.paramCard = paramCard;
    }

    public ParamCard getParamCardByType(String type) {
        for (ParamCard paramCard : getParamCard())
            if (paramCard.getType().compareTo(type) == 0)
                return paramCard;
        return null;
    }

    public String getUrlHost() {
        return urlHost;
    }

    public void setUrlHost(String urlHost) {
        this.urlHost = urlHost;
    }

    public boolean isStatusDriver() {
        return statusDriver;
    }

    public void setStatusDriver(boolean statusDriver) {
        this.statusDriver = statusDriver;
    }

    public double getBalanceDriver() {
        return balanceDriver;
    }

    public void setBalanceDriver(double balanceDriver) {
        this.balanceDriver = balanceDriver;
    }

    public Geocoder getGeoCoder() {
        return geoCoder;
    }

    private void setGeoCoder(Geocoder geoCoder) {
        this.geoCoder = geoCoder;
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap = scaleDown(bitmap, 1000, false);
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        bitmap.recycle();
        return byteArrayOutputStream.toByteArray();
    }

    public int getRegionCar() {
        return regionCar;
    }

    public void setRegionCar(int regionCar) {
        this.regionCar = regionCar;
    }

    public boolean isToCenterDriver() {
        return toCenterDriver;
    }

    public void setToCenterDriver(boolean toCenterDriver) {
        this.toCenterDriver = toCenterDriver;
    }

    public String getCurrentCity() {
        return currentCity;
    }

    public void setCurrentCity(String currentCity) {
        this.currentCity = currentCity;
    }

    public DriverCard getCurrentDriverCard() {
        return currentDriverCard;
    }

    public void setCurrentDriverCard(DriverCard currentDriverCard) {
        this.currentDriverCard = currentDriverCard;
    }

    public ArrayList<Integer> getCancelOrder() {
        return cancelOrder;
    }

    public void setCancelOrder(ArrayList<Integer> cancelOrder) {
        this.cancelOrder = cancelOrder;
    }

    public Boolean isCancelOrderById(int id) {
        for (Integer idOrder : getCancelOrder())
            if (idOrder == id) {
                return true;
            }
        return false;
    }

    public String getStrCurrency(double val) {
        NumberFormat cf1 = NumberFormat.getCurrencyInstance(new Locale("ru", "RU"));
        return cf1.format(val);
    }

    public boolean isActiveOrder() {
        for (OrderCard orderCard : getOrderCard())
            if ((orderCard.getStatus() > 2) && (orderCard.getStatus() < 6))
                return true;
        return false;
    }

    public String getUrlTerms() {
        return urlTerms;
    }

    public void setUrlTerms(String urlTerms) {
        this.urlTerms = urlTerms;
    }

    public String getUrlRequisites() {
        return urlRequisites;
    }

    public void setUrlRequisites(String urlRequisites) {
        this.urlRequisites = urlRequisites;
    }

    public String getCurPaymentDriver() {
        return curPaymentDriver;
    }

    public void setCurPaymentDriver(String curPaymentDriver) {
        this.curPaymentDriver = curPaymentDriver;
    }
}