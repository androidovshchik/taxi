package com.cargotaxi.Model;

import com.google.android.gms.maps.model.LatLng;

public class OrderCard {
    private int id = 0;
    private int idTypeCar = 101;
    private LatLng latLngStart;
    private LatLng latLngFinish;
    private LatLng latLngDriver;
    private String addressStart = "";
    private String cityStart = "";
    private String addressFinish = "";
    private String cityFinish = "";
    private boolean onSelect = false;
    private String strDate = "";
    private int posDate = 0;
    private int period = 1;
    private int countPorter = 0;
    private boolean toCenter = false;
    private boolean toSuburb = false;
    private int priceCar = 0;
    private int pricePorter = 0;
    private int priceCarReal = 0;
    private int pricePorterReal = 0;
    private int priceCarFinal = 0;
    private int pricePorterFinal = 0;
    private int priceDopCar = 0;
    private int priceDopPorter = 0;
    private String comment = "";
    private int status = 0;
    private String nameCustomer = "";
    private String phoneCustomer = "";
    private int idCustomer = 0;
    private int idDriver = 0;
    private String nameDriver = "";
    private String modelCar = "";
    private String numberCar = "";
    private String date = "";
    private int isTake = 0;
    private int regionCar = 0;
    private String phoneDriver = "";
    private String time = "";
    private int rating = 0;

    public OrderCard(int id, String latStart, String lngStart, String latFinish, String lngFinish, String addressStart, String cityStart, String addressFinish, String cityFinish, int period, int countPorter, String toCenter, String toSuburb, int priceCar, int pricePorter, int priceDopCar, int priceDopPorter, String comment, String nameCustomer, String phoneCustomer, int idTypeCar, int status, int idCustomer, int idDriver, int isTake,
                     String nameDriver, String modelCar, String numberCar, String date, int regionCar, String phoneDriver, String latDriver, String lngDriver, String time, int rating) {
        this.id = id;
        latLngStart = new LatLng(Double.parseDouble(latStart), Double.parseDouble(lngStart));
        latLngFinish = new LatLng(Double.parseDouble(latFinish), Double.parseDouble(lngFinish));
        this.setAddressStart(addressStart);
        this.cityStart = cityStart;
        this.setAddressFinish(addressFinish);
        this.cityFinish = cityFinish;
        this.period = period;
        this.countPorter = countPorter;
        if (toCenter.compareTo("1") == 0)
            this.toCenter = true;
        if (toSuburb.compareTo("1") == 0)
            this.toSuburb = true;
        this.setPriceCar(priceCar);
        this.setPricePorter(pricePorter);
        this.setPriceDopCar(priceDopCar);
        this.setPriceDopPorter(priceDopPorter);
        this.comment = comment;
        this.nameCustomer = nameCustomer;
        this.phoneCustomer = phoneCustomer;
        this.idTypeCar = idTypeCar;
        this.status = status;
        this.idCustomer = idCustomer;
        this.idDriver = idDriver;
        this.isTake = isTake;
        this.nameDriver = nameDriver;
        this.modelCar = modelCar;
        this.numberCar = numberCar;
        this.date = date;
        this.regionCar = regionCar;
        this.phoneDriver = phoneDriver;
        latLngDriver = new LatLng(Double.parseDouble(latDriver), Double.parseDouble(lngDriver));
        this.setRegionCar(getRegionCar());
        this.setTime(time);
        this.rating = rating;
    }

    public OrderCard() {
    }

    private static float distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return (float) (earthRadius * c);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDistance() {
        float distance = 0;
        if (Model.getInstance().getCurrentLatLng() != null)
            distance = distFrom(Model.getInstance().getCurrentLatLng().latitude, Model.getInstance().getCurrentLatLng().longitude, getLatLngStart().latitude, getLatLngStart().longitude);
        return Math.round(distance / 10.0) / 100.0 + " км";
    }

    public int getIntDistance() {
        float distance = 0;
        if (Model.getInstance().getCurrentLatLng() != null)
            try {
                distance = distFrom(Model.getInstance().getCurrentLatLng().latitude, Model.getInstance().getCurrentLatLng().longitude, getLatLngStart().latitude, getLatLngStart().longitude);
            } catch (Exception ignored) {
            }
        return (int) (Math.round(distance / 10.0) / 100.0);
    }

    public LatLng getLatLngStart() {
        return latLngStart;
    }

    public void setLatLngStart(LatLng latLngStart) {
        this.latLngStart = latLngStart;
    }

    public LatLng getLatLngFinish() {
        return latLngFinish;
    }

    public void setLatLngFinish(LatLng latLngFinish) {
        this.latLngFinish = latLngFinish;
    }

    public String getAddressStart() {
        return addressStart;
    }

    public void setAddressStart(String addressStart) {
        this.addressStart = addressStart;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public int getCountPorter() {
        return countPorter;
    }

    public void setCountPorter(int countPorter) {
        this.countPorter = countPorter;
    }

    public boolean isToCenter() {
        return toCenter;
    }

    public String getToCenter() {
        if (isToCenter())
            return "да";
        else
            return "нет";
    }

    public void setToCenter(boolean toCenter) {
        this.toCenter = toCenter;
    }

    public boolean isToSuburb() {
        return toSuburb;
    }

    public String getToSuburb() {
        if (isToSuburb())
            return "да";
        else
            return "нет";
    }

    public void setToSuburb(boolean toSuburb) {
        this.toSuburb = toSuburb;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCityStart() {
        return cityStart;
    }

    public void setCityStart(String cityStart) {
        this.cityStart = cityStart;
    }

    public String getCityFinish() {
        return cityFinish;
    }

    public void setCityFinish(String cityFinish) {
        this.cityFinish = cityFinish;
    }

    public int getPosDate() {
        return posDate;
    }

    public void setPosDate(int posDate) {
        this.posDate = posDate;
    }

    public String getNameCustomer() {
        return nameCustomer;
    }

    public void setNameCustomer(String nameCustomer) {
        this.nameCustomer = nameCustomer;
    }

    public String getPhoneCustomer() {
        return phoneCustomer;
    }

    public void setPhoneCustomer(String phoneCustomer) {
        this.phoneCustomer = phoneCustomer;
    }

    public String getTimeToRadar() {
        return date.replaceFirst(" ", "\n");
    }

    public String getTimeToRadarCustomer() {
        if (posDate == 0) return "Через\n1 час";
        if (posDate == 1) return "Через\n2 часа";
        if (posDate == 2) return "Через\n3 часа";
        if (posDate == 3) return getStrDate().replaceFirst(" ", "\n");
        return "";
    }

    public String getPeriodToRadar() {
        if (period == 1) return "1 час";
        if (period == 2) return "2 часа";
        if (period == 3) return "3 часа";
        if (period == 4) return "4 часа";
        if (period == 5) return "5 часов";
        if (period == 6) return "6 часов";
        return "";
    }

    public String getStrDate() {
        return strDate;
    }

    public void setStrDate(String strDate) {
        this.strDate = strDate;
    }

    public boolean isOnSelect() {
        return onSelect;
    }

    public void setOnSelect(boolean onSelect) {
        this.onSelect = onSelect;
    }

    public void setPriceCar(int priceCar) {
        this.priceCar = priceCar;
    }

    public double getPricePorter(long dopMinute) {
        setPricePorterReal((int) (pricePorter + (priceDopPorter / 60f * dopMinute)));
        return (int) (pricePorter + (priceDopPorter / 60f * dopMinute));
    }

    public void setPricePorter(int pricePorter) {
        this.pricePorter = pricePorter;
    }

    public String getAddressFinish() {
        return addressFinish;
    }

    public void setAddressFinish(String addressFinish) {
        this.addressFinish = addressFinish;
    }

    public int getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(int idCustomer) {
        this.idCustomer = idCustomer;
    }

    public int getIdDriver() {
        return idDriver;
    }

    public void setIdDriver(int idDriver) {
        this.idDriver = idDriver;
    }

    public int getIsTake() {
        return isTake;
    }

    public void setIsTake(int isTake) {
        this.isTake = isTake;
    }

    public String getNameDriver() {
        return nameDriver;
    }

    public void setNameDriver(String nameDriver) {
        this.nameDriver = nameDriver;
    }

    public String getModelCar() {
        return modelCar;
    }

    public void setModelCar(String modelCar) {
        this.modelCar = modelCar;
    }

    public String getNumberCar() {
        return numberCar;
    }

    public void setNumberCar(String numberCar) {
        this.numberCar = numberCar;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getRegionCar() {
        return regionCar;
    }

    private void setRegionCar(int regionCar) {
        this.regionCar = regionCar;
    }

    public String getPhoneDriver() {
        return phoneDriver;
    }

    public void setPhoneDriver(String phoneDriver) {
        this.phoneDriver = phoneDriver;
    }

    public LatLng getLatLngDriver() {
        return latLngDriver;
    }

    public void setLatLngDriver(LatLng latLngDriver) {
        this.latLngDriver = latLngDriver;
    }

    public String getTime() {
        return time;
    }

    private void setTime(String time) {
        this.time = time;
    }

    public int getIdTypeCar() {
        return idTypeCar;
    }

    public void setIdTypeCar(int idTypeCar) {
        this.idTypeCar = idTypeCar;
    }

    public CarCard getCurrentCarCard() {
        for (CarCard carCard : Model.getInstance().getCarCard())
            if (carCard.getId() == idTypeCar)
                return carCard;
        return Model.getInstance().getCarCard().get(0);
    }

    public String getNameTypeCar() {
        return Model.getInstance().getCarCardById(getIdTypeCar()).getType();
    }

    public String getCardNameTypeCar() {
        String str = Model.getInstance().getCarCardById(getIdTypeCar()).getType();
        if (str.indexOf("(") > 0)
            str = str.substring(0, str.indexOf("(") - 1) + "\n" + str.substring(str.indexOf("("));
        return str;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getPriceDopCar() {
        return priceDopCar;
    }

    public void setPriceDopCar(int priceDopCar) {
        this.priceDopCar = priceDopCar;
    }

    public int getPriceDopPorter() {
        return priceDopPorter;
    }

    public void setPriceDopPorter(int priceDopPorter) {
        this.priceDopPorter = priceDopPorter;
    }

    public int getPriceCarReal() {
        return priceCarReal;
    }

    private void setPriceCarReal(int priceCarReal) {
        this.priceCarReal = priceCarReal;
    }

    public int getPricePorterReal() {
        return pricePorterReal;
    }

    private void setPricePorterReal(int pricePorterReal) {
        this.pricePorterReal = pricePorterReal;
    }

    public int getPriceCarFinal() {
        return priceCarFinal;
    }

    public void setPriceCarFinal(int priceCarFinal) {
        this.priceCarFinal = priceCarFinal;
    }

    public int getPricePorterFinal() {
        return pricePorterFinal;
    }

    public void setPricePorterFinal(int pricePorterFinal) {
        this.pricePorterFinal = pricePorterFinal;
    }

    public double getPriceCar(long dopMinute) {
        setPriceCarReal((int) (priceCar + (priceDopCar / 60f * dopMinute)));
        return (int) (priceCar + (priceDopCar / 60f * dopMinute));
    }
}
