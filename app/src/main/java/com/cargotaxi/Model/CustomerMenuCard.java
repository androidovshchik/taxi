package com.cargotaxi.Model;

import java.util.ArrayList;

public class CustomerMenuCard {
    private final ArrayList<String> namePunkt = new ArrayList<>();
    private final ArrayList<String> infoPunkt = new ArrayList<>();
    private String name;
    private int idIcon;
    private String type = "";
    private ArrayList<Boolean> selectPunkt = new ArrayList<>();
    private boolean select = false;
    private int id;
    private int idCategory;
    private String val;
    private String math;

    public CustomerMenuCard() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        String str = "";
        if (getType().compareTo("type_car") == 0) {
            str = Model.getInstance().getCurrentOrder().getNameTypeCar();
        } else if (getType().compareTo("comment") == 0) {
            if (Model.getInstance().getCurrentOrder().getComment().length() == 0)
                str = "нет комментария";
            else
                str = Model.getInstance().getCurrentOrder().getComment();
        }
        if (getType().compareTo("period") == 0)
            str = this.namePunkt.get(Model.getInstance().getCurrentOrder().getPeriod() - 1);
        if (getType().compareTo("porter") == 0)
            str = this.namePunkt.get(Model.getInstance().getCurrentOrder().getCountPorter());
        if (getType().compareTo("time") == 0) {
            if (Model.getInstance().getCurrentOrder().getPosDate() < 3)
                str = this.namePunkt.get(Model.getInstance().getCurrentOrder().getPosDate());
            else
                str = Model.getInstance().getCurrentOrder().getStrDate();
        }
        return str;
    }

    public String getInfoCar() {
        String str = Model.getInstance().getCurrentOrder().getNameTypeCar();
        if (str.indexOf("(") > 0)
            str = str.substring(0, str.indexOf("(")) + "\n" + str.substring(str.indexOf("("));
        return str;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMath() {
        return math;
    }

    public void setMath(String math) {
        this.math = math;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public int getIdIcon() {
        return idIcon;
    }

    public void setIdIcon(int idIcon) {
        this.idIcon = idIcon;
    }

    public ArrayList<Boolean> getSelect() {
        return selectPunkt;
    }

    public ArrayList<String> getNamePunkt() {
        return namePunkt;
    }

    public void setPunkt(String[] namePunkt, String[] infoPunkt) {
        this.namePunkt.clear();
        for (int i = 0; i < namePunkt.length; i++) {
            this.namePunkt.add(namePunkt[i]);
            this.infoPunkt.add(infoPunkt[i]);
            this.selectPunkt.add(false);
        }
    }

    public ArrayList<String> getInfoPunkt() {
        return infoPunkt;
    }

    public ArrayList<Boolean> getSelectPunkt() {
        return selectPunkt;
    }

    public void setSelectPunkt(ArrayList<Boolean> selectPunkt) {
        this.selectPunkt = selectPunkt;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(ArrayList<Boolean> select) {
        this.selectPunkt = select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
