package com.cargotaxi.Model;

public class CarCard {
    private int id;
    private String type;
    private String info;
    private int minPrice;
    private int koef;
    private int count = 0;
    private boolean select = false;

    public CarCard(int id, String type, String info, int minPrice, int koef) {
        this.id = id;
        this.type = type;
        this.info = info;
        this.minPrice = minPrice;
        this.koef = koef;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public int getKoef() {
        return koef;
    }

    public void setKoef(int koef) {
        this.koef = koef;
    }
}
