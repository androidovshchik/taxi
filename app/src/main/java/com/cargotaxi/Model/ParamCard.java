package com.cargotaxi.Model;

public class ParamCard {
    private String type = "";
    private String value = "";

    public ParamCard(String type, String value) {
        this.setType(type);
        this.setValue(value);
    }

    public String getType() {
        return type;
    }

    private void setType(String type) {
        this.type = type;
    }

    public String getStringValue() {
        return value;
    }

    public int getIntValue() {
        try {
            return Integer.parseInt(value);
        } catch (Exception ignored) {
        }
        return 0;
    }

    private void setValue(String value) {
        this.value = value;
    }
}
