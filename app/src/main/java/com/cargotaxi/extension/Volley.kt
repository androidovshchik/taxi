package com.cargotaxi.extension

import android.content.Context
import com.android.volley.Request
import com.android.volley.Request.Method
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.RequestFuture
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject
import timber.log.Timber
import java.util.concurrent.TimeUnit

fun Context.syncPost(url: String, params: Map<String, String>? = null): String? {
    val future = RequestFuture.newFuture<String>()
    return syncExecute(object : StringRequest(Method.POST, url, future, future) {

        init {
            setShouldCache(false)
        }

        override fun getParams(): Map<String, String>? {
            return params
        }
    }, future)
}

fun Context.syncGetJson(url: String): JSONObject? {
    val future = RequestFuture.newFuture<JSONObject>()
    return syncExecute(JsonObjectRequest(Method.GET, url, null, future, future), future)
}

fun <T> Context.syncExecute(request: Request<T>?, future: RequestFuture<T>): T? {
    val queue = Volley.newRequestQueue(applicationContext)
    queue.add(request)
    try {
        return future.get(10, TimeUnit.SECONDS)
    } catch (e: Exception) {
        Timber.e(e)
    }
    return null
}