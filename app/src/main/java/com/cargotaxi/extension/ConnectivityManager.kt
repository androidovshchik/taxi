@file:Suppress("unused", "DEPRECATION")

package com.cargotaxi.extension

import android.net.ConnectivityManager

val ConnectivityManager.isConnected: Boolean
    get() = activeNetworkInfo?.isConnected == true