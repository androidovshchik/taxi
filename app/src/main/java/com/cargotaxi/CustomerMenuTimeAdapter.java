package com.cargotaxi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.cargotaxi.Model.Model;

public class CustomerMenuTimeAdapter extends RecyclerView.Adapter<CustomerMenuTimeAdapter.CardViewHolder> {
    private static CustomerMenuTimeFragment fragment = null;
    private int curPunkt;

    public CustomerMenuTimeAdapter(CustomerMenuTimeFragment fragment, Context context, int curPunkt) {
        CustomerMenuTimeAdapter.fragment = fragment;
        this.curPunkt = curPunkt;
    }

    @Override
    public int getItemCount() {
        return Model.getInstance().getCustomerMenuCard().get(curPunkt).getNamePunkt().size();
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_menu_time, viewGroup, false);
        return new CardViewHolder(v, i);
    }

    @Override
    public void onBindViewHolder(final CardViewHolder personViewHolder, final int i) {
        personViewHolder.tvName.setText(Model.getInstance().getCustomerMenuCard().get(curPunkt).getNamePunkt().get(i));
        if (Model.getInstance().getCurrentOrder().getPosDate() == i) {
            personViewHolder.ivChecked.setBackgroundResource(R.drawable.check1);
        } else {
            personViewHolder.ivChecked.setBackgroundResource(R.drawable.check0);
        }
        personViewHolder.cv.setOnClickListener(view -> {
            Model.getInstance().getCurrentOrder().setPosDate(i);
            fragment.eventListener.toMenuEvent();
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {
        final CardView cv;
        final TextView tvName;
        final ImageView ivChecked;

        CardViewHolder(View itemView, int item) {
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            tvName = itemView.findViewById(R.id.tvName);
            ivChecked = itemView.findViewById(R.id.ivChecked);
        }
    }
}
