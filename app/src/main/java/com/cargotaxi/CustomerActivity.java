package com.cargotaxi;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cargotaxi.Model.CarCard;
import com.cargotaxi.Model.Model;
import com.cargotaxi.Model.OrderCard;
import com.cargotaxi.Model.ParamCard;
import com.cargotaxi.service.ForegroundService;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import kotlin.Pair;
import timber.log.Timber;

public class CustomerActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, CustomerMap1Fragment.onEventListener, CustomerMap2Fragment.onEventListener, CustomerMenuFragment.onEventListener, CustomerPaymentFragment.onEventListener, CustomerSetAddressFragment.onEventListener, CustomerMenuTimeFragment.onEventListener, CustomerMenuCarFragment.onEventListener {
    private final CustomerMap1Fragment customerMap1Fragment = new CustomerMap1Fragment();
    private final CustomerMap2Fragment customerMap2Fragment = new CustomerMap2Fragment();
    private final CustomerMenuFragment customerMenuFragment = new CustomerMenuFragment();
    private final CustomerMenuCarFragment customerMenuCarFragment = new CustomerMenuCarFragment();
    private final CustomerMenuTimeFragment customerMenuTimeFragment = new CustomerMenuTimeFragment();
    private final CustomerMenuPeriodFragment customerMenuPeriodFragment = new CustomerMenuPeriodFragment();
    private final CustomerMenuPorterFragment customerMenuPorterFragment = new CustomerMenuPorterFragment();
    private final CustomerMenuCommentFragment customerMenuCommentFragment = new CustomerMenuCommentFragment();
    private final CustomerPaymentFragment customerPaymentFragment = new CustomerPaymentFragment();
    private final CustomerHistoryFragment customerHistoryFragment = new CustomerHistoryFragment();
    private final CustomerProfilFragment customerProfilFragment = new CustomerProfilFragment();
    private final CustomerRatingFragment customerRatingFragment = new CustomerRatingFragment();
    private String currentTypeFragment = "";
    private CustomerSetAddressFragment customerSetAddressFragment = new CustomerSetAddressFragment();
    private Timer timerGetInfo;
    private TimertaskGetInfo timertaskGetInfo;
    private Timer timerHistory;
    private TimertaskHistory timertaskHistory;
    private FragmentTransaction fTrans;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;

    public static void onGetCustomerInfo(JSONObject jMainObject) {
        try {
            JSONArray jArrayCar = jMainObject.getJSONArray("typeCar");
            Model.getInstance().getCarCard().clear();
            for (int i = 0; i < jArrayCar.length(); i++) {
                JSONObject jObjectCar = jArrayCar.getJSONObject(i);
                Model.getInstance().getCarCard().add(new CarCard(Integer.parseInt(jObjectCar.getString("id")), jObjectCar.getString("name"), jObjectCar.getString("info"), Integer.parseInt(jObjectCar.getString("price")), Integer.parseInt(jObjectCar.getString("koef"))));
            }
            Model.getInstance().getParamCard().clear();
            Model.getInstance().getParamCard().clear();
            JSONArray jArrayText = jMainObject.getJSONArray("text");
            for (int i = 0; i < jArrayText.length(); i++) {
                JSONObject jObjectParam = jArrayText.getJSONObject(i);
                Model.getInstance().getParamCard().add(new ParamCard(jObjectParam.getString("type"), jObjectParam.getString("value")));
            }
            JSONArray jArrayParam = jMainObject.getJSONArray("param");
            for (int i = 0; i < jArrayParam.length(); i++) {
                JSONObject jObjectParam = jArrayParam.getJSONObject(i);
                Model.getInstance().getParamCard().add(new ParamCard(jObjectParam.getString("type"), jObjectParam.getString("value")));
            }
            JSONArray jArray = jMainObject.getJSONArray("order");
            Model.getInstance().getOrderCard().clear();
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject jObject = jArray.getJSONObject(i);
                {
                    OrderCard orderCard = new OrderCard(jObject.getInt("idorder"), jObject.getString("latStart"), jObject.getString("lngStart"),
                        jObject.getString("latFinish"), jObject.getString("lngFinish"), jObject.getString("addressStart"), jObject.getString("cityStart"), jObject.getString("addressFinish"), jObject.getString("cityFinish"),
                        jObject.getInt("period"), jObject.getInt("countPorter"), jObject.getString("toCenter"),
                        jObject.getString("toSuburb"), jObject.getInt("priceCar"), jObject.getInt("pricePorter"), jObject.getInt("priceDopCar"), jObject.getInt("priceDopPorter"), jObject.getString("comment"),
                        jObject.getString("nameCustomer"), jObject.getString("phoneCustomer"), jObject.getInt("typeCar"), jObject.getInt("status"), jObject.getInt("idCustomer"), jObject.getInt("idDriver"), 0,
                        jObject.getString("nameDriver"), jObject.getString("modelCar"), jObject.getString("numberCar"), jObject.getString("date"),
                        jObject.getInt("regionCar"), jObject.getString("phoneDriver"), jObject.getString("latDriver"), jObject.getString("lngDriver"),
                        jObject.getString("time"), jObject.getInt("rating"));
                    Model.getInstance().setLastIdOrder(jObject.getInt("idorder"));
                    Model.getInstance().getOrderCard().add(0, orderCard);
                }
            }
        } catch (JSONException e) {
            Timber.e(e);
        }
    }

    @Override
    public void toMap2Event() {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, customerMap2Fragment);
        fTrans.commitAllowingStateLoss();
    }

    @Override
    public void toMenuEvent() {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, customerMenuFragment);
        fTrans.commitAllowingStateLoss();
    }

    @Override
    public void toMap1Event() {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, customerMap1Fragment);
        fTrans.commitAllowingStateLoss();
    }

    @Override
    public void toTypeCarEvent() {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, customerMenuCarFragment);
        fTrans.commitAllowingStateLoss();
        setBackArrow(true);
        currentTypeFragment = "";
    }

    @Override
    public void toRadarEvent() {
        Intent intent = new Intent(this, RadarActivity.class);
        startActivity(intent);
        finish();
        currentTypeFragment = "";
    }

    @Override
    public void toTimeEvent() {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, customerMenuTimeFragment);
        fTrans.commitAllowingStateLoss();
        setBackArrow(true);
        currentTypeFragment = "";
    }

    @Override
    public void toPeriodEvent() {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, customerMenuPeriodFragment);
        fTrans.commitAllowingStateLoss();
        setBackArrow(true);
        currentTypeFragment = "";
    }

    @Override
    public void toPorterEvent() {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, customerMenuPorterFragment);
        fTrans.commitAllowingStateLoss();
        setBackArrow(true);
        currentTypeFragment = "";
    }

    @Override
    public void toSetFinishAddressEvent() {
        fTrans = getSupportFragmentManager().beginTransaction();
        customerSetAddressFragment = new CustomerSetAddressFragment();
        fTrans.replace(R.id.frgmCont, customerSetAddressFragment);
        fTrans.commitAllowingStateLoss();
        setBackArrow(true);
        currentTypeFragment = "map2";
    }

    @Override
    public void toSetStartAddressEvent() {
        fTrans = getSupportFragmentManager().beginTransaction();
        customerSetAddressFragment = new CustomerSetAddressFragment();
        fTrans.replace(R.id.frgmCont, customerSetAddressFragment);
        fTrans.commitAllowingStateLoss();
        setBackArrow(true);
        currentTypeFragment = "map1";
    }

    @Override
    public void toCommentEvent() {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, customerMenuCommentFragment);
        fTrans.commitAllowingStateLoss();
        setBackArrow(true);
        currentTypeFragment = "";
    }

    @Override
    public void toPaymentEvent() {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, customerPaymentFragment);
        fTrans.commitAllowingStateLoss();
        setBackArrow(true);
        currentTypeFragment = "";
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configure_button();
        setContentView(R.layout.activity_customer);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(Color.parseColor("#1E1E1E"));
        toolbar.setNavigationOnClickListener(v -> finish());
        timerGetInfo = new Timer();
        timertaskGetInfo = new TimertaskGetInfo();
        timerGetInfo.schedule(timertaskGetInfo, 100, 10000);
        timerHistory = new Timer();
        timertaskHistory = new TimertaskHistory();
        timerHistory.schedule(timertaskHistory, 100, 990);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);
        Bundle b = getIntent().getExtras();
        String value = "";
        if (b != null) {
            value = b.getString("type");
        }
        if (value.compareTo("history") == 0) {
            fTrans = getSupportFragmentManager().beginTransaction();
            fTrans.add(R.id.frgmCont, customerHistoryFragment);
            fTrans.commitAllowingStateLoss();
        } else if (Model.getInstance().getCurrentOrder().getStatus() == 2) {
            toRadarEvent();
        } else if (Model.getInstance().getCurrentOrder().getStatus() == 0) {
            fTrans = getSupportFragmentManager().beginTransaction();
            fTrans.add(R.id.frgmCont, customerMap1Fragment);
            fTrans.commitAllowingStateLoss();
            drawer.openDrawer(GravityCompat.START);
        } else if (Model.getInstance().getCurrentOrder().getStatus() == 1) {
            fTrans = getSupportFragmentManager().beginTransaction();
            fTrans.add(R.id.frgmCont, customerMenuFragment);
            fTrans.commitAllowingStateLoss();
        } else {
            fTrans = getSupportFragmentManager().beginTransaction();
            fTrans.add(R.id.frgmCont, customerHistoryFragment);
            fTrans.commitAllowingStateLoss();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //noinspection unchecked
        ForegroundService.start(getApplicationContext(), new Pair("location", true), new Pair("interval", 1000L));
    }

    @Override
    protected void onStop() {
        //noinspection unchecked
        ForegroundService.start(getApplicationContext(), new Pair("location", true), new Pair("interval", 5000L));
        super.onStop();
    }

    private void setBackArrow(boolean rt) {
        if (rt) {
            toggle.setDrawerIndicatorEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toggle.setToolbarNavigationClickListener(v -> {
                fTrans = getSupportFragmentManager().beginTransaction();
                InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                if (currentTypeFragment.compareTo("map1") == 0)
                    fTrans.replace(R.id.frgmCont, customerMap1Fragment);
                else if (currentTypeFragment.compareTo("map2") == 0)
                    fTrans.replace(R.id.frgmCont, customerMap2Fragment);
                else
                    fTrans.replace(R.id.frgmCont, customerMenuFragment);
                setBackArrow(false);
                fTrans.commitAllowingStateLoss();
            });
        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            toggle.setDrawerIndicatorEnabled(true);
            toggle.setToolbarNavigationClickListener(v -> drawer.openDrawer(Gravity.LEFT));
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    public void toPrevFragment() {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, customerMenuFragment);
        fTrans.commitAllowingStateLoss();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_order) {
            fTrans = getSupportFragmentManager().beginTransaction();
            fTrans.replace(R.id.frgmCont, customerMenuFragment);
            fTrans.commitAllowingStateLoss();
            setBackArrow(false);
        } else if (id == R.id.nav_history) {
            fTrans = getSupportFragmentManager().beginTransaction();
            fTrans.replace(R.id.frgmCont, customerHistoryFragment);
            fTrans.commitAllowingStateLoss();
            setBackArrow(true);
            currentTypeFragment = "";
        } else if (id == R.id.nav_support) {
            fTrans = getSupportFragmentManager().beginTransaction();
            SupportFragment supportFragment = new SupportFragment();
            fTrans.replace(R.id.frgmCont, supportFragment);
            fTrans.addToBackStack(null);
            fTrans.commitAllowingStateLoss();
            setBackArrow(true);
            currentTypeFragment = "";
        } else if (id == R.id.nav_profile) {
            fTrans = getSupportFragmentManager().beginTransaction();
            fTrans.replace(R.id.frgmCont, customerProfilFragment);
            fTrans.commitAllowingStateLoss();
            setBackArrow(true);
            currentTypeFragment = "";
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng sydney = new LatLng(-34, 151);
        googleMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    public void onBtnExitClick(View view) {
        ForegroundService.stop(getApplicationContext());
        Model.getInstance().setTypeUser(0);
        DAO.getInstance().deleteProfil();
        Intent intent = new Intent(getApplicationContext(), RegActivity.class);
        startActivity(intent);
        finish();
    }

    public void onBtnConfirmTypeCar(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(drawer.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, customerMenuFragment);
        fTrans.commitAllowingStateLoss();
    }

    public void onDestroy() {
        timerGetInfo.cancel();
        timertaskGetInfo.cancel();
        timerHistory.cancel();
        timertaskHistory.cancel();
        super.onDestroy();
    }

    private void configure_button() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}
                    , 10);
            }
        }
    }

    public void toRatingFragment() {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, customerRatingFragment);
        fTrans.commitAllowingStateLoss();
    }

    public void onBtnRatingSendClick(View view) {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/set_rating.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
                try {
                    JSONObject jObject = new JSONObject(response);
                    if (jObject.getBoolean("status")) {
                        Toast.makeText(getApplicationContext(), "Рейтинг добавлен", Toast.LENGTH_SHORT).show();
                        fTrans = getSupportFragmentManager().beginTransaction();
                        fTrans.replace(R.id.frgmCont, customerHistoryFragment);
                        fTrans.commitAllowingStateLoss();
                    } else
                        Toast.makeText(getApplicationContext(), "Ошибка", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), "Ошибка", Toast.LENGTH_SHORT).show();
                    Timber.e(e);
                }
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idOrder", String.valueOf(Model.getInstance().getCurrentOrder().getId()));
                int rating = 5;
                try {
                    rating = (int) customerRatingFragment.ratingBar.getRating();
                } catch (Exception ignored) {
                }
                params.put("rating", String.valueOf(rating));
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void onBtnRatingCancelClick(View view) {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, customerHistoryFragment);
        fTrans.commitAllowingStateLoss();
    }

    public void onBtnRequisitesClick(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Model.getInstance().getUrlRequisites()));
        startActivity(browserIntent);
    }

    public void onBtnTermsClick(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Model.getInstance().getUrlTerms()));
        startActivity(browserIntent);
    }

    class TimertaskGetInfo extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(() -> {
                try {
                    customerMap1Fragment.setMapMarker();
                } catch (Exception ignored) {
                }
            });
        }
    }

    class TimertaskHistory extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(() -> {
                try {
                    int count1 = customerHistoryFragment.sectionsPagerAdapter.Fragment1.reloadDriverList();
                    customerHistoryFragment.sectionsPagerAdapter.Fragment1.reloadTimeDriverList();
                    int count2 = customerHistoryFragment.sectionsPagerAdapter.Fragment2.reloadDriverList();
                    if ((count1 == -1) && (count2 == 1))
                        customerHistoryFragment.viewPager.setCurrentItem(1);
                } catch (Exception ignored) {
                }
            });
        }
    }
}
