package com.cargotaxi;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.cargotaxi.Model.Model;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import timber.log.Timber;

public class DriverEditInfoFragment extends Fragment {
    ImageButton btnAddPhoto;
    private TextInputEditText etName;
    private TextInputEditText etSurname;
    private TextInputEditText etPhone;
    private TextInputLayout layName;
    private TextInputLayout laySurname;
    private TextInputLayout layPhone;
    private View view;
    private onEventListener eventListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_driver_edit_info, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("О себе");
        btnAddPhoto = view.findViewById(R.id.btnAddPhoto);
        etName = view.findViewById(R.id.etName);
        etName.setText(Model.getInstance().getNameUser());
        etSurname = view.findViewById(R.id.etSurname);
        etSurname.setText(Model.getInstance().getSurnameUser());
        etPhone = view.findViewById(R.id.etPhone);
        etPhone.setText(Model.getInstance().getPhoneUser());
        Glide.with(this)
            .load(Model.getInstance().getUrlHost() + "photo/driver_photo/" + Model.getInstance().getIdUser() + ".png")
            .into(btnAddPhoto);
        etPhone.setOnFocusChangeListener((view, hasFocus) -> {
            if (etPhone.getSelectionStart() < 2) etPhone.setSelection(2);
        });
        etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() < 2) {
                    etPhone.setText("+7");
                    etPhone.setSelection(etPhone.getText().length());
                }
                if (etPhone.getSelectionStart() < 2) etPhone.setSelection(2);
            }
        });
        layName = view.findViewById(R.id.layName);
        laySurname = view.findViewById(R.id.laySurname);
        layPhone = view.findViewById(R.id.layPhone);
        MaterialButton btnNext = view.findViewById(R.id.btnNext);
        btnNext.setText("Сохранить");
        btnNext.setOnClickListener(v -> validation());
        return view;
    }

    private void validation() {
        boolean isValid = true;
        if (layName.getEditText().getText().toString().isEmpty()) {
            layName.setError("Введите имя");
            isValid = false;
        } else {
            layName.setErrorEnabled(false);
        }
        if (laySurname.getEditText().getText().toString().isEmpty()) {
            laySurname.setError("Введите фамилию");
            isValid = false;
        } else {
            laySurname.setErrorEnabled(false);
        }
        if (layPhone.getEditText().getText().toString().length() < 5) {
            layPhone.setError("Введите номер телефона");
            isValid = false;
        } else {
            layPhone.setErrorEnabled(false);
        }
        if (isValid) {
            sendInfoReg();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            eventListener = (onEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    private void sendInfoReg() {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/reg_driver_info.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
                try {
                    JSONObject jObject = new JSONObject(response);
                    if (jObject.getBoolean("status")) {
                        Model.getInstance().setNameUser(etName.getText().toString());
                        Model.getInstance().setSurnameUser(etSurname.getText().toString());
                        Model.getInstance().setPhoneUser(etPhone.getText().toString());
                        DAO.getInstance().saveProfil();
                        eventListener.toProfilEvent();
                    } else
                        Snackbar.make(view, "Данные не отправлены", Snackbar.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Timber.e(e);
                    Snackbar.make(view, "Данные не отправлены", Snackbar.LENGTH_SHORT).show();
                }
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id", String.valueOf(Model.getInstance().getIdUser()));
                params.put("name", etName.getText().toString());
                params.put("surname", etSurname.getText().toString());
                params.put("birthday", Model.getInstance().getStrDateRegDriver());
                params.put("mail", "");
                return params;
            }
        };
        queue.add(postRequest);
    }

    public interface onEventListener {
        void toProfilEvent();
    }
}