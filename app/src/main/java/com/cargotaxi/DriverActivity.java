package com.cargotaxi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cargotaxi.Model.CarCard;
import com.cargotaxi.Model.Model;
import com.cargotaxi.Model.OrderCard;
import com.cargotaxi.Model.ParamCard;
import com.cargotaxi.service.ForegroundService;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import kotlin.Pair;
import ru.yandex.money.android.sdk.Checkout;
import ru.yandex.money.android.sdk.TokenizationResult;
import timber.log.Timber;

public class DriverActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, DriverRadarFragment.onEventListener, DriverEditInfoFragment.onEventListener, DriverEditCarFragment.onEventListener {
    private final static String CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    private final DriverOrderFragment driverOrderFragment = new DriverOrderFragment();
    private final DriverRadarFragment driverRadarFragment = new DriverRadarFragment();
    private final DriverProfilFragment driverProfilFragment = new DriverProfilFragment();
    private final DriverHistoryFragment driverHistoryFragment = new DriverHistoryFragment();
    private final DriverEditInfoFragment driverEditInfoFragment = new DriverEditInfoFragment();
    private final DriverEditCarFragment driverEditCarFragment = new DriverEditCarFragment();
    private final DriverPaymentFragment driverPaymentFragment = new DriverPaymentFragment();
    ImageButton btnSwitchActiveDriver;
    private IntentFilter intentFilter;
    private MyReceiver receiver;
    private Snackbar snackbar;
    private TextView tvStatusDriver;
    private boolean mainScreen = true;
    private FragmentTransaction fTrans;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private MenuItem itemPayment;
    private RequestQueue rQueue;

    public static void onGetDriverInfo(JSONObject jMainObject) {
        try {
            JSONArray jArrayCar = jMainObject.getJSONArray("typeCar");
            Model.getInstance().getCarCard().clear();
            for (int i = 0; i < jArrayCar.length(); i++) {
                JSONObject jObjectCar = jArrayCar.getJSONObject(i);
                Model.getInstance().getCarCard().add(new CarCard(Integer.parseInt(jObjectCar.getString("id")), jObjectCar.getString("name"), jObjectCar.getString("info"), Integer.parseInt(jObjectCar.getString("price")), Integer.parseInt(jObjectCar.getString("koef"))));
            }
            Model.getInstance().getParamCard().clear();
            JSONArray jArrayText = jMainObject.getJSONArray("text");
            for (int i = 0; i < jArrayText.length(); i++) {
                JSONObject jObjectParam = jArrayText.getJSONObject(i);
                Model.getInstance().getParamCard().add(new ParamCard(jObjectParam.getString("type"), jObjectParam.getString("value")));
            }
            JSONArray jArrayParam = jMainObject.getJSONArray("param");
            for (int i = 0; i < jArrayParam.length(); i++) {
                JSONObject jObjectParam = jArrayParam.getJSONObject(i);
                Model.getInstance().getParamCard().add(new ParamCard(jObjectParam.getString("type"), jObjectParam.getString("value")));
            }
            Model.getInstance().setBalanceDriver(jMainObject.getDouble("balance"));
            if (jMainObject.getInt("statusDriver") == 1)
                Model.getInstance().setStatusDriver(true);
            else
                Model.getInstance().setStatusDriver(false);
            JSONArray jArray = jMainObject.getJSONArray("order");
            Model.getInstance().getOrderCard().clear();
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject jObject = jArray.getJSONObject(i);
                try {
                    OrderCard orderCard = new OrderCard(jObject.getInt("idorder"), jObject.getString("latStart"), jObject.getString("lngStart"),
                        jObject.getString("latFinish"), jObject.getString("lngFinish"), jObject.getString("addressStart"), jObject.getString("cityStart"), jObject.getString("addressFinish"), jObject.getString("cityFinish"),
                        jObject.getInt("period"), jObject.getInt("countPorter"), jObject.getString("toCenter"),
                        jObject.getString("toSuburb"), jObject.getInt("priceCar"), jObject.getInt("pricePorter"), jObject.getInt("priceDopCar"), jObject.getInt("priceDopPorter"), jObject.getString("comment"),
                        jObject.getString("nameCustomer"), jObject.getString("phoneCustomer"), jObject.getInt("typeCar"), jObject.getInt("status"), jObject.getInt("idCustomer"), jObject.getInt("idDriver"), jObject.getInt("isTake"),
                        jObject.getString("nameDriver"), jObject.getString("modelCar"), jObject.getString("numberCar"), jObject.getString("date"),
                        jObject.getInt("regionCar"), jObject.getString("phoneDriver"), jObject.getString("latDriver"), jObject.getString("lngDriver"),
                        jObject.getString("time"), jObject.getInt("rating"));
                    Model.getInstance().setLastIdOrder(jObject.getInt("idorder"));
                    Model.getInstance().getOrderCard().add(0, orderCard);
                    if ((orderCard.getStatus() >= 3) && (orderCard.getStatus() < 6)) {
                        Model.getInstance().setCurrentOrder(orderCard);
                    }
                } catch (Exception ignored) {
                }
            }
        } catch (JSONException e) {
            Timber.e(e);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void toPrevFragment() {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, driverOrderFragment);
        fTrans.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver);
        intentFilter = new IntentFilter();
        intentFilter.addAction(CONNECTIVITY_ACTION);
        receiver = new MyReceiver();
        if (checkForInternet()) {
        } else {
        }
        tvStatusDriver = findViewById(R.id.tvStatusDriver);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(Color.parseColor("#1E1E1E"));
        toolbar.setNavigationOnClickListener(v -> finish());
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        drawer.openDrawer(GravityCompat.START);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.add(R.id.frgmCont, driverOrderFragment);
        fTrans.commit();
        btnSwitchActiveDriver = findViewById(R.id.btnSwitchActiveDriver);
        btnSwitchActiveDriver.setBackgroundResource(R.drawable.switch_driver_on);
        btnSwitchActiveDriver.setVisibility(View.GONE);
        btnSwitchActiveDriver.setOnClickListener(v -> {
            Model.getInstance().setWorkDriver(!Model.getInstance().isWorkDriver());
            if (Model.getInstance().isWorkDriver())
                btnSwitchActiveDriver.setBackgroundResource(R.drawable.switch_driver_on);
            else
                btnSwitchActiveDriver.setBackgroundResource(R.drawable.switch_driver_off);
            driverOrderFragment.reload();
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        //noinspection unchecked
        ForegroundService.start(getApplicationContext(), new Pair("location", true), new Pair("interval", 1000L));
    }

    @Override
    protected void onStop() {
        //noinspection unchecked
        ForegroundService.start(getApplicationContext(), new Pair("location", true), new Pair("interval", 5000L));
        super.onStop();
    }

    private void setOrder() {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, driverRadarFragment);
        fTrans.commit();
    }

    public void toOrderList() {
        Model.getInstance().getCurrentOrder().setId(0);
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, driverOrderFragment);
        fTrans.commit();
    }

    public void toChangeStatus(int status) {
        changeStatusOrder(status);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_order) {
            fTrans = getSupportFragmentManager().beginTransaction();
            fTrans.replace(R.id.frgmCont, driverOrderFragment);
            fTrans.commit();
        } else if (id == R.id.nav_history) {
            fTrans = getSupportFragmentManager().beginTransaction();
            fTrans.replace(R.id.frgmCont, driverHistoryFragment);
            fTrans.commit();
            setBackArrow(true);
        } else if (id == R.id.nav_support) {
            fTrans = getSupportFragmentManager().beginTransaction();
            SupportFragment supportFragment = new SupportFragment();
            fTrans.replace(R.id.frgmCont, supportFragment);
            fTrans.commit();
            setBackArrow(true);
        } else if (id == R.id.nav_profile) {
            fTrans = getSupportFragmentManager().beginTransaction();
            fTrans.replace(R.id.frgmCont, driverProfilFragment);
            fTrans.commit();
            setBackArrow(true);
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_driver, menu);
        itemPayment = menu.findItem(R.id.action_payment);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_payment) {
            setBackArrow(true);
            fTrans = getSupportFragmentManager().beginTransaction();
            fTrans.replace(R.id.frgmCont, driverPaymentFragment);
            fTrans.commit();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setBackArrow(boolean rt) {
        if (rt) {
            mainScreen = false;
            btnSwitchActiveDriver.setVisibility(View.GONE);
            itemPayment.setVisible(false);
            toggle.setDrawerIndicatorEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toggle.setToolbarNavigationClickListener(v -> {
                fTrans = getSupportFragmentManager().beginTransaction();
                fTrans.replace(R.id.frgmCont, driverOrderFragment);
                setBackArrow(false);
                fTrans.commit();
            });
        } else {
            mainScreen = true;
            itemPayment.setVisible(true);
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            toggle.setDrawerIndicatorEnabled(true);
            toggle.setToolbarNavigationClickListener(v -> drawer.openDrawer(Gravity.LEFT));
        }
    }

    private void changeStatusOrder(int status) {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/change_status_order.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
                try {
                    JSONObject jObject = new JSONObject(response);
                    if (jObject.getBoolean("status")) {
                        if (status == 4)
                            driverRadarFragment.changeStatus(4);
                        if (status == 5) {
                            Model.getInstance().getCurrentOrder().setDate(jObject.getString("ts"));
                            driverRadarFragment.changeStatus(5);
                        }
                        if (status == 6)
                            driverRadarFragment.changeStatus(6);
                        Model.getInstance().getCurrentOrder().setStatus(status);
                        Model.getInstance().getCurrentOrder().setPriceCarFinal(Model.getInstance().getCurrentOrder().getPriceCarReal());
                        Model.getInstance().getCurrentOrder().setPricePorterFinal(Model.getInstance().getCurrentOrder().getPricePorterReal());
                    }
                } catch (Exception e) {
                    Timber.e(e);
                }
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idOrder", String.valueOf(Model.getInstance().getCurrentOrder().getId()));
                params.put("idUserDriver", String.valueOf(Model.getInstance().getIdUser()));
                params.put("status", String.valueOf(status));
                if (status == 6) {
                    params.put("priceCar", String.valueOf(Model.getInstance().getCurrentOrder().getPriceCarReal()));
                    params.put("pricePorter", String.valueOf(Model.getInstance().getCurrentOrder().getPricePorterReal()));
                }
                return params;
            }
        };
        queue.add(postRequest);
        queue.addRequestFinishedListener(request -> queue.getCache().clear());
    }

    public void onDestroy() {
        try {
            driverRadarFragment.mTimer.cancel();
        } catch (Exception ignored) {
        }
        try {
            driverRadarFragment.mMyTimerTask.cancel();
        } catch (Exception ignored) {
        }
        super.onDestroy();
    }

    public void onbtnEditInfoClick(View view) {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, driverEditInfoFragment);
        fTrans.commit();
        setBackArrow(true);
    }

    public void onbtnEditCarClick(View view) {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, driverEditCarFragment);
        fTrans.commit();
        setBackArrow(true);
    }

    public void toProfilEvent() {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, driverProfilFragment);
        fTrans.commit();
    }

    public void onBtnExitClick(View view) {
        ForegroundService.stop(getApplicationContext());
        Model.getInstance().setTypeUser(0);
        DAO.getInstance().deleteProfil();
        Intent intent = new Intent(getApplicationContext(), RegActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    private boolean checkForInternet() {
        ConnectivityManager cm =
            (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
            activeNetwork.isConnectedOrConnecting();
    }

    private void checkNet(boolean rez) {
        if (rez) {
            if (snackbar != null && snackbar.isShown())
                snackbar.dismiss();
        } else {
            snackbar = Snackbar.make(btnSwitchActiveDriver, "Проверьте наличие Интернета", Snackbar.LENGTH_INDEFINITE);
            snackbar.show();
        }
    }

    public void toPay() {
        Intent intent = new Intent(this, SuccessTokenizeActivity.class);
        startActivity(intent);
    }

    private void getRequest(String str) {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://cargogruztaxi.ru/payment.php?count=10&key=" + str;
        StringRequest request = new StringRequest(Request.Method.GET, url, response -> {
            response = response.substring(response.indexOf("<br />") + 7);
            try {
                JSONObject jObject = new JSONObject(response);
                if (jObject.getBoolean("status")) {
                }
            } catch (Exception e) {
                Timber.e(e);
            }
        }, error -> {
        });
        queue.add(request);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        {
            switch (resultCode) {
                case RESULT_OK:
                    TokenizationResult result = Checkout.createTokenizationResult(data);
                    getRequest(result.getPaymentToken());
                    break;
                case RESULT_CANCELED:
                    Toast.makeText(this, R.string.tokenization_canceled, Toast.LENGTH_SHORT).show();
                    break;
            }
        }
        if (resultCode == 500) {
            payment();
            return;
        }
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (data != null) {
            Uri contentURI = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                switch (requestCode) {
                    case 1:
                        uploadImage(bitmap, "driver_photo");
                        driverEditInfoFragment.btnAddPhoto.setImageURI(data.getData());
                        break;
                    case 2:
                        uploadImage(bitmap, "car_photo");
                        driverEditCarFragment.btnAddPhotoCar.setImageURI(data.getData());
                        break;
                    case 3:
                        uploadImage(bitmap, "regts1_photo");
                        driverEditCarFragment.btnAddRegTS1.setImageURI(data.getData());
                        break;
                    case 4:
                        uploadImage(bitmap, "regts2_photo");
                        driverEditCarFragment.btnAddRegTS2.setImageURI(data.getData());
                        break;
                    case 5:
                        uploadImage(bitmap, "license_photo");
                        driverEditCarFragment.btnAddLicense.setImageURI(data.getData());
                        break;
                }
            } catch (Exception e) {
                Timber.e(e);
                Toast.makeText(DriverActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void payment() {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/payment_driver.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
                try {
                    JSONObject jObject = new JSONObject(response);
                    if (jObject.getBoolean("status")) {
                        double balance = jObject.getLong("balance");
                        Model.getInstance().setBalanceDriver(balance);
                        driverPaymentFragment.tvBalance.setText(Model.getInstance().getStrCurrency(Model.getInstance().getBalanceDriver()));
                    }
                } catch (Exception e) {
                    Timber.e(e);
                }
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("price", String.valueOf(Model.getInstance().getCurPaymentDriver()));
                params.put("idUserDriver", String.valueOf(Model.getInstance().getIdUser()));
                return params;
            }
        };
        queue.add(postRequest);
        queue.addRequestFinishedListener(request -> queue.getCache().clear());
    }

    public void onBtnAddDriverPhotoClick(View view) {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, 1);
    }

    public void onBtnAddPhotoCarClick(View view) {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, 2);
    }

    public void onBtnAddPhotoDoc1Click(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(intent, 3);
    }

    public void onBtnAddPhotoDoc2Click(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(intent, 4);
    }

    public void onBtnAddPhotoDoc3Click(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(intent, 5);
    }

    private void uploadImage(final Bitmap bitmap, String type) {
        String upload_URL;
        upload_URL = Model.getInstance().getUrlHost() + "script/upload_" + type + ".php?";
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, upload_URL,
            response -> {
                rQueue.getCache().clear();
                try {
                    JSONObject jsonObject = new JSONObject(new String(response.data));
                    Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    Timber.e(e);
                }
            },
            error -> Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show()) {
            @Override
            protected Map<String, String> getParams() {
                return new HashMap<>();
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("filename", new DataPart(Model.getInstance().getIdUser() + ".png", Model.getInstance().getFileDataFromDrawable(bitmap)));
                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rQueue = Volley.newRequestQueue(DriverActivity.this);
        rQueue.add(volleyMultipartRequest);
        rQueue.addRequestFinishedListener(request -> rQueue.getCache().clear());
    }

    @Override
    public void getDriverInfo(@NotNull String response) {
        try {
            JSONObject jMainObject = new JSONObject(response);
            if (mainScreen) {
                if (Model.getInstance().isStatusDriver()) {
                    btnSwitchActiveDriver.setVisibility(View.VISIBLE);
                    tvStatusDriver.setVisibility(View.GONE);
                } else {
                    btnSwitchActiveDriver.setVisibility(View.GONE);
                    tvStatusDriver.setVisibility(View.VISIBLE);
                }
            }
            JSONArray jArray = jMainObject.getJSONArray("order");
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject jObject = jArray.getJSONObject(i);
                try {
                    OrderCard orderCard = new OrderCard(jObject.getInt("idorder"), jObject.getString("latStart"), jObject.getString("lngStart"),
                        jObject.getString("latFinish"), jObject.getString("lngFinish"), jObject.getString("addressStart"), jObject.getString("cityStart"), jObject.getString("addressFinish"), jObject.getString("cityFinish"),
                        jObject.getInt("period"), jObject.getInt("countPorter"), jObject.getString("toCenter"),
                        jObject.getString("toSuburb"), jObject.getInt("priceCar"), jObject.getInt("pricePorter"), jObject.getInt("priceDopCar"), jObject.getInt("priceDopPorter"), jObject.getString("comment"),
                        jObject.getString("nameCustomer"), jObject.getString("phoneCustomer"), jObject.getInt("typeCar"), jObject.getInt("status"), jObject.getInt("idCustomer"), jObject.getInt("idDriver"), jObject.getInt("isTake"),
                        jObject.getString("nameDriver"), jObject.getString("modelCar"), jObject.getString("numberCar"), jObject.getString("date"),
                        jObject.getInt("regionCar"), jObject.getString("phoneDriver"), jObject.getString("latDriver"), jObject.getString("lngDriver"),
                        jObject.getString("time"), jObject.getInt("rating"));
                    if ((jObject.getInt("status") == 2) && (orderCard.getId() == Model.getInstance().getCurrentOrder().getId())) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(DriverActivity.this);
                        builder.setTitle("Клиент отменил заказ")
                            .setCancelable(false)
                            .setNegativeButton("Ок",
                                (dialog, id) -> cancelOrder());
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                    if ((orderCard.getStatus() >= 3) && (orderCard.getStatus() < 6)) {
                        setOrder();
                    }
                } catch (Exception ignored) {
                }
            }
            try {
                driverOrderFragment.changeNewOrder();
            } catch (Exception ignored) {
            }
            try {
                driverPaymentFragment.changePrice();
            } catch (Exception rt) {
                rt.printStackTrace();
            }
        } catch (JSONException e) {
            Timber.e(e);
        }
    }

    private void cancelOrder() {
        RequestQueue queue = Volley.newRequestQueue(Model.getInstance().getContext());
        String url = Model.getInstance().getUrlHost() + "script/delete_order.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
            response -> {
                try {
                    JSONObject jObject = new JSONObject(response);
                    if (jObject.getBoolean("status"))
                        toOrderList();
                } catch (Exception e) {
                    Timber.e(e);
                }
            },
            error -> {
            }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idOrder", String.valueOf(Model.getInstance().getCurrentOrder().getId()));
                params.put("idUserDriver", String.valueOf(Model.getInstance().getIdUser()));
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void toHistory() {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, driverHistoryFragment);
        fTrans.commit();
        setBackArrow(true);
    }

    public void onBtnRequisitesClick(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Model.getInstance().getUrlRequisites()));
        startActivity(browserIntent);
    }

    public void onBtnTermsClick(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Model.getInstance().getUrlTerms()));
        startActivity(browserIntent);
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String actionOfIntent = intent.getAction();
            boolean isConnected = checkForInternet();
            if (actionOfIntent.equals(CONNECTIVITY_ACTION)) {
                checkNet(isConnected);
                if (isConnected) {
                } else {
                }
            }
        }
    }
}
