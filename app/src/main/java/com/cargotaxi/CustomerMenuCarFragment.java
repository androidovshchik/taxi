package com.cargotaxi;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class CustomerMenuCarFragment extends Fragment {
    CustomerMap2Fragment.onEventListener eventListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_menu_car, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Тип автомобиля");
        RecyclerView rv = view.findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        RecyclerView.LayoutManager llm = new GridLayoutManager(getActivity().getApplicationContext(), 1);
        rv.setLayoutManager(llm);
        rv.setVisibility(View.VISIBLE);
        String[] customer_menu_array = getResources().getStringArray(R.array.customer_menu);
        CustomerMenuCarAdapter formulaAdapter = new CustomerMenuCarAdapter(this, getActivity().getApplicationContext());
        rv.setAdapter(formulaAdapter);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            eventListener = (CustomerMap2Fragment.onEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }

    public interface onEventListener {
        void toMenuEvent();
    }
}